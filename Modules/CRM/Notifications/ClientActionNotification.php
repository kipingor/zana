<?php

namespace Modules\CRM\Notifications;

use Auth;
use Lang;
use Modules\CRM\Entities\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ClientActionNotification extends Notification
{
    use Queueable;

    private $client;
    private $action;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($client, $action)
    {
        $this->middleware('auth');
        $this->client = $client;
        $this->action = $action;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        // return (new MailMessage)
        //             ->line('The introduction to the notification.')
        //             ->action('Notification Action', 'https://laravel.com')
        //             ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        switch ($this->action) {
            case 'created':
                $text = __('Client :company was assigned to you', [
                    'company' => $this->client->company_name,
                ]);
                break;
            case 'updated_assign':
                $text = __(':username assigned :company to you', [
                    'company' => $this->client->company_name,
                    'username' => Auth()->user()->name
                ]);
                break;
            default:
                break;
        }

        return [
            'assigned_user' => $notifiable->id, //Assigned user ID
            'created_user' => auth()->user()->id,
            'creator' => Auth()->user()->name,
            'title' => $this->client->company_name,
            'message' => $text,
            'type' => Client::class,
            'type_id' =>  $this->client->id,
            'url' =>  url('crm/clients/' . $this->client->id),
            'action' => $this->action
        ];
    }
}
