<?php
namespace Modules\CRM\Repositories\Setting;

interface SettingRepositoryContract
{
    public function getCompanyName();

    public function updateOverall($requestData);

    public function getSetting();
}
