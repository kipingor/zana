<ul class="breadcrumb">
    @php($isCurrent = true)
    @foreach($allSteps as $id => $step)
        @php($cssClass = ($isCurrent ? 'sw-current' : ''))

        <li class="{{ $cssClass }}"></li>
        <li class="{{ $cssClass }}">{!! trans('setup_wizard::steps.' . $id . '.breadcrumb') !!}</li>

        @if(\SetupWizard::isCurrent($id))
            @php($isCurrent = false)
        @endif
    @endforeach
</ul>
