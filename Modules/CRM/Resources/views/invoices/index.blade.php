@extends('crm::layouts.master')

@section('content')
<div class="page-content-wrapper content-builder full-height" id="columns-3-9">
  <!-- START PAGE CONTENT -->
  <div class="content full-height">
    <div class="container-fluid full-height no-padding">
      <div class="row full-height no-margin">
        <div class="col-md-3 no-padding b-r b-grey sm-b-b full-height">
          <div class="bg-white full-height">
            <!-- YOU CAN REMOVE FULL-HEIGHT IN ALL PARENT ELEMENTS TO EXPEND TO CONTENT HEIGHT
                     YOU CAN ALSO CHANGE THE BACKGROUN COLOR BY ADDING THE BG CLASSES
                     EXAMPLE : bg-success
                   -->
          </div>
        </div>
        <div class="col-md-9 no-padding full-height">
          <div class="placeholder full-height">
            <!-- YOU CAN REMOVE FULL-HEIGHT IN ALL PARENT ELEMENTS TO EXPEND TO CONTENT HEIGHT
                     YOU CAN ALSO CHANGE THE BACKGROUN COLOR BY ADDING THE BG CLASSES
                     EXAMPLE : bg-success
                   -->
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- END PAGE CONTENT -->
  <!-- START COPYRIGHT -->
  <!-- START CONTAINER FLUID -->
  <div class="container-fluid container-fixed-lg footer">
    <div class="copyright sm-text-center">
      <p class="small no-margin pull-left sm-pull-reset">
        <span class="hint-text">Copyright © 2014 </span>
        <span class="font-montserrat">REVOX</span>.
        <span class="hint-text">All rights reserved. </span>
        <span class="sm-block"><a href="#" class="m-l-10 m-r-10">Terms of use</a> | <a href="#" class="m-l-10">Privacy Policy</a></span>
      </p>
      <p class="small no-margin pull-right sm-pull-reset">
        <a href="#">Hand-crafted</a> <span class="hint-text">&amp; Made with Love ®</span>
      </p>
      <div class="clearfix"></div>
    </div>
  </div>
  <!-- END COPYRIGHT -->
</div>

@endsection