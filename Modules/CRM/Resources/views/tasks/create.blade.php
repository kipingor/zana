@extends('crm::layouts.master')
@section('heading')
    <h1>Create task</h1>
@endsection

@section('content')
<div class="container-fluid container-fixed-lg">
    {!! Form::open([
            'route' => 'tasks.store'
            ]) !!}
        <div class="row clearfix">
            <div class="col-sm-7">
                <div class="form-group form-group-default required">
                    {!! Form::label('title', __('Title') , ['class' => 'control-label']) !!}
                    {!! Form::text('title', null, ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
    <div class="row">
        <div class="col-sm-7">
            <div class="form-group form-group-default">
                {!! Form::label('description', __('Description'), ['class' => 'control-label']) !!}
                {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-7">

            <div class="form-inline">
            
            <div class="form-group form-group-default input-group col-md-6">
            <div class="form-input-group">
            {!! Form::label('deadline', __('Deadline')) !!}
            <input type="email" class="form-control" placeholder="Pick a date" id="datepicker-component2">
            </div>
            <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
            </div>
            </div>


                <div class="form-group col-sm-6 removeleft ">
                    {!! Form::label('deadline', __('Deadline'), ['class' => 'control-label']) !!}
                    {!! Form::date('deadline', \Carbon\Carbon::now()->addDays(3), ['class' => 'form-control']) !!}
                </div>

                <div class="form-group col-sm-6 removeleft removeright">
                    {!! Form::label('status', __('Status'), ['class' => 'control-label']) !!}
                    {!! Form::select('status', array(
                    '1' => 'Open', '2' => 'Completed'), null, ['class' => 'cs-select cs-skin-slide', 'data-init-plugin' => 'cs-select'] )
                 !!}
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-7">
    
            <div class="form-group form-inline">
                {!! Form::label('user_assigned_id', __('Assign user'), ['class' => 'control-label']) !!}
                {!! Form::select('user_assigned_id', $users, null, ['class' => 'form-control']) !!}
                @if(Request::get('client') != "")
                    {!! Form::hidden('client_id', Request::get('client')) !!}
                @else
                    {!! Form::label('client_id', __('Assign client'), ['class' => 'control-label']) !!}
                    {!! Form::select('client_id', $clients, null, ['class' => 'form-control']) !!}
                @endif
            </div>
        </div>
    </div>

    {!! Form::submit(__('Create task'), ['class' => 'btn btn-primary']) !!}

    {!! Form::close() !!}

</div>



@endsection