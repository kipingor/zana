@extends('crm::layouts.master')
@section('heading')
    <h1>All tasks</h1>
@endsection

@section('content')
<div class="container-fluid container-fixed-lg">
<table class="table table-hover" id="tasks-table">
        <thead>
        <tr>
            
            <th style="width:1%">
                <button class="btn"><i class="fa fa-trash-o"></i>
                </button>
            </th>
            <th style="width:49%">{{ __('Title') }}</th>
            <th style="width:15%">{{ __('Created at') }}</th>
            <th style="width:15%">{{ __('Deadline') }}</th>
            <th style="width:20%">{{ __('Assigned') }}</th>

        </tr>
        </thead>
        <tbody>
            @foreach($tasks as $task)
            <tr>
                <td class="v-align-middle">
                    <div class="checkbox check-success checkbox-circle">
                      <input type="checkbox" value="{{ $task->id }}" id="checkbox1">
                      <label for="checkbox1"></label>
                    </div>
                </td>
                <td class="v-align-middle">
                    <a href="/crm/tasks/{{$task->id}}">{{ $task->title }}</a>
                </td>
                <td class="v-align-middle">{{ $task->created_at->toFormattedDateString() }}</td>
                <td class="v-align-middle">{{ $task->deadline->toFormattedDateString() }}</td>
                <td class="v-align-middle">{{ $task->user->name }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection

@include('layouts.admin.partials.datatables')