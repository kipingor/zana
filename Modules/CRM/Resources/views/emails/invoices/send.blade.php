<!DOCTYPE html>
<html>
<head>
	<title>Invoice</title>
</head>
<body>
<p>Dear {{ $client->name}},</p>
<p>Thanks for your business. </p>
<p>
The invoice {{ $invoice->invoice_no}} is attached with this email.
</p>
<p>
<br>Invoice Overview: 
<br>Invoice # : {{ $invoice->invoice_no}} 
<br>Date : {{ $invoice->sent_at}} 
<br>Amount : KES83,730.00 
</p>
<p>
It was great working with you. Looking forward to working with you again.
</p>
<p>
Regards
<br>{{$user->name}}</p>
</body>
</html>