@extends('crm::layouts.master')
@section('page-content-wrapper-class')
content-builder full-height active
@endsection
@section('content-class')
full-height
@endsection
@section('jumbotron')
	@include('crm::partials.clientheader')
  @include('crm::partials.userheader')
@endsection
@section('content')
<div class="container-fluid container-fixed-lg">
	<div class="col-sm-8">
    <div class="row">
      <div class="panel">
        <ul class="nav nav-tabs nav-tabs-simple" role="tablist" data-init-reponsive-tabs="collapse">
          <li class="active">
          	<a href="#tasks" data-toggle="tab" role="tab">Tasks</a>
          </li>
          <li>
          	<a href="#leads" data-toggle="tab" role="tab">Leads</a>
          </li>
          <li>
          	<a href="#invoices" data-toggle="tab" role="tab">Invoices</a>
          </li>
          <li>
          	<a href="#documents" data-toggle="tab" role="tab">Documents</a>
          </li>
        </ul>
        <div class="tab-content">
            @include('crm::clients.views.tasks')
            @include('crm::clients.views.leads')
            @include('crm::clients.views.invoice')
            @include('crm::clients.views.document')
        </div>
      </div>
    </div>
	</div>
	
	<div class="col-md-4">
		{!! Form::model($client, [
               'method' => 'PATCH',
                'url' => ['clients/updateassign', $client->id],
                ]) !!}
            {!! Form::select('user_assigned_id', $users, $client->user->id, ['class' => 'form-control ui search selection top right pointing search-select', 'id' => 'search-select']) !!}
            {!! Form::submit(__('Assign new user'), ['class' => 'btn btn-primary form-control closebtn']) !!}
            {!! Form::close() !!}
	</div>
<!-- End of Page -->
</div>
</div>
@endsection

@section('modal')

@endsection

@push('styles')
    <link href="{{asset('assets/plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/datatables-responsive/css/datatables.responsive.css')}}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{asset('assets/plugins/dropzone/css/dropzone.css')}}" rel="stylesheet" type="text/css" />
    {{-- <link href="{{asset('assets/css/dropzone.css')}}" rel="stylesheet" type="text/css" /> --}}
@endpush

@push('scripts')
    <script src="{{asset('assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/datatables-responsive/js/datatables.responsive.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/datatables-responsive/js/lodash.min.js')}}"></script>
    <script src="{{asset('assets/js/datatables.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/bootstrap-collapse/bootstrap-tabcollapse.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/dropzone/dropzone.js')}}"></script>
    {{-- <script src="{{asset('assets/js/form_elements.js')}}" type="text/javascript"></script> --}}
    {{-- <script type="text/javascript" scr="{{asset('assets/js/dropzone.js')}}"></script> --}}
@endpush