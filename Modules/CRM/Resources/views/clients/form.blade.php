<div class="row clearfix">
    <div class="col-sm-12">
      <div class="form-group form-group-default required">
        {!! Form::label('name', 'Name:', ['class' => 'control-label']) !!}
        {!! 
        Form::text('name',  
        isset($data['owners']) ? $data['owners'][0]['name'] : null, 
        ['class' => 'form-control']) 
        !!}
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <div class="form-group form-group-default">
        {!! Form::label('vat', 'Vat:', ['class' => 'control-label']) !!}
        {!! 
            Form::text('vat',
            isset($data['vat']) ?$data['vat'] : null,
            ['class' => 'form-control']) 
        !!}
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <div class="form-group form-group-default">
        {!! Form::label('company_name', 'Company name:', ['class' => 'control-label']) !!}
        {!! 
            Form::text('company_name',
            isset($data['name']) ? $data['name'] : null, 
            ['class' => 'form-control']) 
        !!}
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <div class="form-group form-group-default">
        {!! Form::label('email', 'Email:', ['class' => 'control-label']) !!}
        {!! 
            Form::email('email',
            isset($data['email']) ? $data['email'] : null, 
            ['class' => 'form-control']) 
        !!}
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <div class="form-group form-group-default">
        {!! Form::label('address', 'Address:', ['class' => 'control-label']) !!}
        {!! 
            Form::text('address',
            isset($data['address']) ? $data['address'] : null, 
            ['class' => 'form-control'])
        !!}
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-4">
      <div class="form-group form-group-default">
        {!! Form::label('zipcode', 'Zipcode:', ['class' => 'control-label']) !!}
        {!! 
            Form::text('zipcode',
             isset($data['zipcode']) ? $data['zipcode'] : null, 
             ['class' => 'form-control']) 
        !!}
      </div>
    </div>
    <div class="col-sm-8">
      <div class="form-group form-group-default">
        {!! Form::label('city', 'City:', ['class' => 'control-label']) !!}
        {!! 
            Form::text('city',
            isset($data['city']) ? $data['city'] : null,
            ['class' => 'form-control']) 
        !!}
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-6">
      <div class="form-group form-group-default">
        {!! Form::label('primary_number', 'Primary Number:', ['class' => 'control-label']) !!}
        {!! 
            Form::text('primary_number',  
            isset($data['phone']) ? $data['phone'] : null, 
            ['class' => 'form-control']) 
        !!}
      </div>
    </div>
    <div class="col-sm-6">
      <div class="form-group form-group-default">
        {!! Form::label('secondary_number', 'Secondary Number:', ['class' => 'control-label']) !!}
        {!! 
            Form::text('secondary_number',  
            null, 
            ['class' => 'form-control']) 
        !!}
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <div class="form-group form-group-default form-group-default-select2">
        {!! Form::label('industry', 'Industry:', ['class' => 'control-label ']) !!}
        {!!
            Form::select('industry_id',
            $industries,
            null,
            ['class' => 'form-control full-width ui search selection top right pointing search-select',
            'id' => 'search-select', 'data-init-plugin' => 'select2'])
        !!}
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <div class="form-group form-group-default form-group-default-select2">
        {!! Form::label('user_id', 'Assign user:', ['class' => 'control-label']) !!}
        {!! Form::select('user_id', $users, null, ['class' => 'form-control full-width ui search selection top right pointing search-select', 'id' => 'search-select', 'data-init-plugin' => 'select2']) !!}
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
  {!! Form::submit($submitButtonText, ['class' => 'btn btn-primary']) !!}