<div class="tab-pane" id="leads">
  <div class="row">
    <div class="panel panel-transparent">
      <div class="panel-heading">
        <div class="panel-title">{{ __('All Leads') }}</div>
        <div class="pull-right">
          <div class="col-xs-12">
            <a href="{{ route('leads.create', ['client' => $client->id])}}" class="btn btn-primary btn-cons"><i class="fa fa-plus"></i> {{ __('New Lead') }}
            </a> 
          </div>
        </div>
        <div class="clearfix"></div>
      </div>
      <div class="panel-body">
          <div class="table-responsive">
            <div id="stripedTable_wrapper" class="dataTables_wrapper form-inline no-footer">
              <table class="table table-striped dataTable no-footer" id="stripedTable" role="grid">
                <thead>
                  <tr role="row">
                    <th style="width:50%" class="sorting_asc" tabindex="0" aria-controls="stripedTable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Title: activate to sort column descending">
                        {{ __('Title') }}
                    </th>
                    <th style="width:20%;" class="sorting" tabindex="0" aria-controls="stripedTable" rowspan="1" colspan="1" aria-label="Places: activate to sort column ascending">
                        {{ __('Assigned') }}
                    </th>
                    <th style="width:15%;" class="sorting" tabindex="0" aria-controls="stripedTable" rowspan="1" colspan="1" aria-label="Places: activate to sort column ascending">
                        {{ __('Created at') }}
                    </th>
                    <th style="width:15%;" class="sorting" tabindex="0" aria-controls="stripedTable" rowspan="1" colspan="1" aria-label="Activities: activate to sort column ascending">
                        {{ __('Deadline') }}
                    </th>
                </tr>
                </thead>
                <tbody>
                    <?php  $tr = ""; ?>
                    @foreach($client->leads as $lead)
                        @if($lead->status == 1)
                            <?php  $tr = '#adebad'; ?>
                        @elseif($lead->status == 2)
                            <?php $tr = '#ff6666'; ?>
                        @endif
                        <tr role="row" class="odd" style="background-color:<?php echo $tr;?>">

                            <td class="v-align-middle semi-bold sorting_1">
                                <a href="{{ route('leads.show', $lead->id) }}">{{$lead->title}} </a>
                            </td>
                            <td class="v-align-middle">
                                <div class="popoverOption"
                                     rel="popover"
                                     data-placement="right"
                                     data-html="true"
                                     data-original-title="<span class='glyphicon glyphicon-user' aria-hidden='true'> </span> {{$lead->user->name}}">
                                    <div id="popover_content_wrapper" style="display:none; width:250px;">
                                        <img src='http://placehold.it/350x150' height='80px' width='80px'
                                             style="float:left; margin-bottom:5px;"/>
                                        <p class="popovertext">
                                            <span class="glyphicon glyphicon-envelope" aria-hidden="true"> </span>
                                            <a href="mailto:{{$lead->user->email}}">
                                                {{$lead->user->email}}<br/>
                                            </a>
                                            <span class="glyphicon glyphicon-headphones" aria-hidden="true"> </span>
                                            <a href="mailto:{{$lead->user->work_number}}">
                                            {{$lead->user->work_number}}
                                        </a>
                                        </p>
                                    </div>
                                    <a href="{{route('users.show', $lead->user->id)}}"> {{$lead->user->name}}</a>

                                </div> <!--Shows users assigned to lead -->
                            </td>
                            <td class="v-align-middle">{{date('d, M Y, H:i', strTotime($lead->contact_date))}}  </td>
                            <td class="v-align-middle">{{date('d, M Y', strTotime($lead->contact_date))}}
                        @if($lead->status == 1)({{ $lead->days_until_contact }})@endif </td>
                        </tr>

                    @endforeach                  
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    <!-- END PANEL -->
    </div>
  </div>