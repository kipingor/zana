<div class="tab-pane" id="documents">
  <div class="row">
    
        {{-- <form method="post" action="{{ url('/crm/clients/upload', $client->id)}}" class="dropzone no-margin" enctype="multipart/form-data" id="dropzone"
                files="true" data-dz-removea>
          <meta name="csrf-token" content="{{ csrf_token() }}">
          <div class="fallback">
            <input name="file" type="file" multiple/>
          </div>         
        </form> --}}
    <div class="panel panel-transparent">
      <div class="panel-heading">
        <div class="panel-title">{{ __('All Documents') }}</div>
        {{-- <div class="pull-right">
          <div class="col-xs-12">
            <a href="{{ route('leads.create', ['client' => $client->id])}}" class="btn btn-primary btn-cons"><i class="fa fa-plus"></i> {{ __('New Lead') }}
            </a> 
          </div>
        </div> --}}
        <div class="clearfix"></div>
      </div>
      <div class="panel-body">
        <table class="table">
          <div class="col-xs-10">
            {{-- <form method="post" action="{{ url('/clients/upload', $client->id)}}" class="dropzone no-margin" enctype="multipart/form-data" data-dz-removea>
              <div class="fallback">
                <input name="file" type="file" multiple/>
              </div>
              <meta name="csrf-token" content="{{ csrf_token() }}">
            </form> --}}
              <form method="POST" action="{{ url('/crm/clients/upload', $client->id)}}" 
                class="dropzone no-margin" id="dropzone"
                files="true" data-dz-removea
                enctype="multipart/form-data">
                @csrf
                {{-- <meta name="csrf-token" content="{{ csrf_token() }}"> --}}
              </form>
              <p><b>{{ __('Max size') }}</b></p>
            </div>
          </div>
          <thead>
            <tr>
              <th>{{ __('File') }}</th>
              <th>{{ __('Size') }}</th>
              <th>{{ __('Created at') }}</th>
            </tr>
          </thead>
          <tbody>
          @foreach($client->documents as $document)
            <tr>
              <td><a href="/assets/files/{{$companyname}}/{{$document->path}}"
                         target="_blank">{{$document->file_display}}</a></td>
              <td>{{$document->size}} <span class="moveright"> MB</span></td>
              <td>{{$document->created_at}}</td>
      
              <td>
                <form method="POST" action="{{action('DocumentsController@destroy', $document->id)}}">
                  <input type="hidden" name="_method" value="delete"/>
                  @csrf
                  <input type="submit" class="btn btn-danger" value="Delete"/>
                </form>
              </td>
            </tr>
          @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

