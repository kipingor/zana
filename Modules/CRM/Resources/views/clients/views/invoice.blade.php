<div class="tab-pane" id="invoices">
  <div class="row">
    <div class="panel panel-transparent">
      <div class="panel-heading">
        <div class="panel-title">{{ __('All Invoices') }}</div>
        <div class="pull-right">
          <div class="col-xs-12">
            <a href="{{ route('leads.create', ['client' => $client->id])}}" class="btn btn-primary btn-cons"><i class="fa fa-plus"></i> {{ __('New Lead') }}
            </a> 
          </div>
        </div>
        <div class="clearfix"></div>
      </div>
      <div class="panel-body">

      </div>
    </div>
  </div>
</div>