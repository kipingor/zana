@extends('crm::layouts.master')
@section('heading')

@endsection
@section('bg-color')
@endsection

@section('content')
<div class="container-fluid container-fixed-lg">
<div class="row">
  <div class="col-lg-7 col-md-6 ">
    <!-- START PANEL -->
    <div class="panel panel-transparent">
      <div class="panel-body">
        {!! Form::model($client, [
            'method' => 'PATCH',
            'route' => ['clients.update', $client->id],
            ]) !!}
          @include('crm::clients/form', ['submitButtonText' => __('Update client')])
        {!! Form::close() !!}
      </div>
    </div>
    <!-- END PANEL -->
  </div>
  <div class="col-lg-5 col-md-6">
    <!-- START PANEL -->
    <div class="panel panel-transparent">
      <div class="panel-heading">
        <div class="panel-title">Create a New Client
        </div>
      </div>
      <div class="panel-body">
        <h3>Mandatory Field are marked</h3>
        <p>Forms are one of the most important components
          <br> when it comes to a dashboard. Recognizing that fact, users are
          <br>able work in a maximum content width.</p>
        <br>
        <p class="small hint-text m-t-5">A lead usually is the contact information and in some cases, demographic information of a customer who is interested in a specific product or service. There are two types of leads in the lead generation market: sales leads and marketing leads.</p>
        <p class="small hint-text m-t-5"><strong>Sales leads</strong> are generated on the basis of demographic criteria such as FICO score, income, age, household income, psychographic, etc. These leads are resold to multiple advertisers. Sales leads are typically followed up through phone calls by the sales force. Sales leads are commonly found in the mortgage, insurance and finance industries.</p>
        <p class="small hint-text m-t-5"></p>
        <button class="btn btn-primary btn-cons">More</button>
      </div>
    </div>
    <!-- END PANEL -->
  </div>
</div>
</div>
@endsection