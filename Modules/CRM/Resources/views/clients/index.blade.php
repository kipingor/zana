@extends('crm::layouts.master')
@section('content')
<div class="container-fluid container-fixed-lg">
  <table class="table table-hover " id="tableWithSearch">
        <thead>
        <tr>
            <th style="width:1%">
                <button class="btn"><i class="fa fa-trash-o"></i>
                </button>
            </th>
            <th style="width:29%">{{ __('Account Name') }}</th>
            <th style="width:15%">{{ __('Phone') }}</th>
            <th style="width:15%">{{ __('Email') }}</th>
            <th style="width:20%">{{ __('Account Ownwer') }}</th>
            <th style="width:10%"></th>
            <th style="width:10%"></th>
        </tr>
        </thead>
        <tbody>
            @foreach($clients as $client)
            <tr>
                <td class="v-align-middle">
                    <div class="checkbox ">
                      <input type="checkbox" value="{{ $client->id }}" id="checkbox1">
                      <label for="checkbox1"></label>
                    </div>
                </td>
                <td><a href="/crm/clients/{{$client->id}}">{{$client->name}}</a></td>
                <td>{{$client->primary_number}}</td>
                <td>{{$client->email}}</td>
                <td>{{$client->user->name}}</td>
                <td>
                     @if(Auth::user()->hasPermission('edit.client'))
                     <a href="{{ route('clients.edit', $client->id) }}" class="btn btn-success" >Edit</a>
                    @endif
                </td>
                <td>
                    @if(Auth::user()->hasPermission('delete.client'))
                    <form action="{{ route('clients.destroy', $client->id) }}" method="POST">
                        <input type="hidden" name="_method" value="DELETE">
                        <input type="submit" name="submit" value="Delete" class="btn btn-danger" onclick="return confirm('Are you sure?')">
                        @csrf
                    </form>
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection

@section('navlink')
<nav class="navbar navbar-default bg-master-lighter sm-padding-10" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sub-nav">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="sub-nav">
          <div class="row">
            <div class="col-sm-4">
              <ul class="nav navbar-nav navbar-center">
                <li><a href="{{url('/crm/clients/create')}}" data-toggle="tooltip" data-placement="bottom" title="Add Cliient"><i class="fas fa-user-plus fa-2x" style="color: orange"></i></a></li>
              </ul>
            </div>        
          </div>
    </div>
</nav>
@endsection

@include('layouts.admin.partials.datatables')

