<div class="bg-white full-height">
    <div class="panel panel-transparent">
        <div class="panel-heading">
            <div class="panel-title">{{ __('Lead information') }}</div>
        </div>
        <div class="panel-body">
            <div class="row">
                <h5 class="font-arial no-margin">{{ __('Assigned to') }}</h5>
                <p class="hint-text">
                    <a href="{{route('leads.show', $lead->user->id)}}">
                        {{$lead->user->name}}</a>
                    </p>
            </div>
            <div class="row">
                <h5 class="font-arial no-margin">{{ __('Created at') }}</h5>
                <p class="hint-text">{{ date('d F, Y, H:i', strtotime($lead->created_at))}}</p>
            </div>
            @if($lead->days_until_contact < 2)
            <div class="row">
                <h5 class="font-arial no-margin">{{ __('Follow up') }}</h5>
                <p class="hint-text">
                    <span style="color:red;">{{date('d, F Y, H:i', strTotime($lead->contact_date))}}

                            @if($lead->status == 1) ({!! $lead->days_until_contact !!}) @endif</span> <i
                                class="glyphicon glyphicon-calendar" data-toggle="modal"
                                data-target="#ModalFollowUp"></i>
                </p>
                <!--Remove days left if lead is completed-->
            </div>
             @else
            <div class="row">
                <h5 class="font-arial no-margin">{{ __('Follow up') }}</h5>
                <p class="hint-text">
                    <span style="color:green;">{{date('d, F Y, H:i', strTotime($lead->contact_date))}}

                            @if($lead->status == 1) ({!! $lead->days_until_contact !!})<i
                                    class="glyphicon glyphicon-calendar" data-toggle="modal"
                                    data-target="#ModalFollowUp"></i>@endif</span>
                </p>
                <!--Remove days left if lead is completed-->
            </div>
            @endif            
            <div class="row">
                <h5 class="font-arial no-margin">{{ __('Status') }}</h5>
                @if($lead->status == 1)
                <p class="hint-text">{{ __('Contact') }}</p>
                @elseif($lead->status == 2)
                <p class="hint-text">{{ __('Completed') }}</p>
                @elseif($lead->status == 3)
                <p class="hint-text">{{ __('Not interested') }}</p>
                @endif
            </div>

        </div>
    </div>
            
            @if($lead->status == 1)
                {!! Form::model($lead, [
               'method' => 'PATCH',
                'url' => ['leads/updateassign', $lead->id],
                ]) !!}
                {!! Form::select('user_assigned_id', $users, null, ['class' => 'form-control ui search selection top right pointing search-select', 'id' => 'search-select']) !!}
                {!! Form::submit(__('Assign new user'), ['class' => 'btn btn-primary form-control closebtn']) !!}
                {!! Form::close() !!}
                {!! Form::model($lead, [
               'method' => 'PATCH',
               'url' => ['leads/updatestatus', $lead->id],
               ]) !!}

                {!! Form::submit(__('Complete Lead'), ['class' => 'btn btn-success form-control closebtn movedown']) !!}
                {!! Form::close() !!}
            @endif

            <div class="activity-feed movedown">
                @foreach($lead->activity as $activity)

                    <div class="feed-item">
                        <div class="activity-date">{{date('d, F Y H:i', strTotime($activity->created_at))}}</div>
                        <div class="activity-text">{{$activity->text}}</div>

                    </div>
                @endforeach
            </div>
        </div>