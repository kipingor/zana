<div class="row clearfix">
    <div class="col-sm-12">
        <div class="form-group form-group-default required">
            {!! Form::label('title', __('Title'), ['class' => 'control-label']) !!}
            {!! Form::text('title', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="form-group form-group-default">
            {!! Form::label('description', __('Description'), ['class' => 'control-label']) !!}
            {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <div class="form-group col-lg-3 removeleft">
            {!! Form::label('status', __('Status'), ['class' => 'control-label']) !!}
            {!! Form::select('status', array(
            '1' => 'Contact Client', '2' => 'Completed'), null, ['class' => 'cs-select cs-skin-slide', 'data-init-plugin' => 'cs-select'] )
         !!}
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group form-group-default input-group col-sm-10">
            {!! Form::label('contact_date', __('Deadline'), ['class' => 'control-label']) !!}
            {!! Form::date('contact_date', Carbon\Carbon::now()->addDays(7), ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group form-group-default input-group col-sm-10">
            {!! Form::label('contact_time', __('Time'), ['class' => 'control-label']) !!}
            {!! Form::time('contact_time', '11:00', ['class' => 'form-control', 'id' => 'timepicker']) !!}
            <span class="input-group-addon"><i class="far fa-clock"></i></span>
        </div>
    </div>
</div>
<div class="row">
<div class="col-sm-6">
    <div class="form-group form-group-default form-group-default-select2">
        {!! Form::label('user_assigned_id', __('Assign user'), ['class' => 'control-label']) !!}
        {!! Form::select('user_assigned_id', $users, null, ['class' => 'full-width', 'data-init-plugin' => 'select2']) !!}
    </div>
</div>
<div class="col-sm-6">
    <div class="form-group form-group-default form-group-default-select2">
        @if(Request::get('client') != "")
            {!! Form::hidden('client_id', Request::get('client')) !!}
        @else
            {!! Form::label('client_id', __('Assign client'), ['class' => 'control-label']) !!}
            {!! Form::select('client_id', $clients, null, ['class' => 'full-width', 'data-init-plugin' => 'select2']) !!}
        @endif
    </div>
</div>
</div>
<div class="clearfix"></div>
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-primary']) !!}
	   
	