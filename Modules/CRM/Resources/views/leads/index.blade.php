@extends('crm::layouts.master')

@section('heading')
    <h1>{{__('All leads')}}</h1>
@stop

@section('content')
    <table class="table table-hover" id="leads-table">
        <thead>
        <tr>

            <th>{{ __('Lead Name') }}</th>
            <th>{{ __('Company')}}</th>
            <th>{{ __('Created by') }}</th>
            <th>{{ __('Contact Data') }}</th>
            <th>{{ __('Assigned') }}</th>

        </tr>
        </thead>
        <tbody>
            
                @foreach($leads as $lead)
                <tr>
                <td><a href="leads/{{$lead->id}}">{{$lead->title}}</a></td>
                <td><a href="/crm/clients/{{$lead->client_id}}"> {{$lead->client->name}} </a></td>
                <td>{{$lead->creator->name}}</td>
                <td>{{$lead->contact_date->toFormattedDateString()}}</td>
                <td>{{$lead->user->name}}</td>
                </tr>
                @endforeach
            
        </tbody>
    </table>
@stop

