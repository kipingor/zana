<br/><br/>
<div class="col-sm-6">

    <div class="box box-primary">
        <div class="box-header with-border">
            <h4 class="box-title"
            >
                {{ __('Tasks each month') }}
            </h4>
            <div class="box-tools pull-right">
                <button type="button" id="collapse1" class="btn btn-box-tool" data-toggle="collapse"
                        data-target="#collapseOne"><i id="toggler1" class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div id="collapseOne" class="panel-collapse">
            <div class="box-body">
                <div>
                    <graphline class="chart" :labels="{{json_encode($createdTaskEachMonths)}}"
                               :values="{{json_encode($taskCreated)}}"
                               :valuesextra="{{json_encode($taskCompleted)}}"></graphline>
                </div>
            </div>
        </div>
    </div>
    <div class="box box-primary">
        <div class="box-header with-border">
            <h4 class="box-title"
            >
               {{ __('Lead each month') }}
            </h4>
            <div class="box-tools pull-right">
                <button type="button" id="collapse2" class="btn btn-box-tool" data-toggle="collapse"
                        data-target="#collapseTwo"><i id="toggler2" class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div id="collapseTwo" class="panel-collapse">
            <div class="box-body">
                <div>
                    <graphline class="chart" :labels="{{json_encode($createdLeadEachMonths)}}"
                               :values="{{json_encode($leadCreated)}}"
                               :valuesextra="{{json_encode($leadsCompleted)}}"></graphline>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-sm-6">    
    
    <div class="col-sm-12">

        <div class="box box-primary">
            <div class="box-header with-border">
                <h4 class="box-title"
                >
                    {{ __('Users') }}
                </h4>
                <div class="box-tools pull-right">

                </div>
            </div>
            <div id="collapseOne" class="panel-collapse">

                @foreach($users as $user)
                    <div class="col-lg-1">
                        @if($user->isOnline())
                            <i class="glyphicon glyphicon-ok-circle text-success" data-toggle="tooltip" title="Online" data-placement="left"></i>
                        @else
                            <i class="glyphicon glyphicon-remove-circle text-danger" data-toggle="tooltip" title="Offline" data-placement="left"></i>
                        @endif
                        <span class="thumbnail-wrapper d32 circular b-white m-r-5 b-a b-white">
                        <a href="{{route('users.show', $user->id)}}">
                            <img width="35" height="35"
                        @if($user->image_path != "")
                        data-src-retina="/assets/img/profiles/{{$companyname}}/{{$user->image_path}}" data-src="/assets/img/profiles/{{$companyname}}/{{$user->image_path}}" title="{{$user->name}}" src="/assets/img/profiles/{{$companyname}}/{{$user->image_path}}"
                        @else
                        data-src-retina="assets/img/profiles/avatar.jpg" data-src="assets/img/profiles/avatar.jpg" alt="Profile Image" src="assets/img/profiles/avatar.jpg"
                        @endif
                        >
                        </a>
                        </span>

                    </div>

                @endforeach

            </div>
        </div>
    </div>


</div>
</div>


<!-- Info boxes -->
<div class="row movedown">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="ion ion-ios-book-outline"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">{{ __('Tasks completed today') }}</span>
                <span class="info-box-number">{{$completedTasksToday}}
                    <small></small></span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-red"><i class="ion ion-ios-book-outline"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">{{ __('Tasks created today') }}</span>
                <span class="info-box-number">{{$createdTasksToday}}</span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->

    <!-- fix for small devices only -->
    <div class="clearfix visible-sm-block"></div>

    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-green"><i class="ion ion-stats-bars"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">{{ __('Leads completed today') }}</span>
                <span class="info-box-number">{{$completedLeadsToday}}</span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="ion ion-stats-bars"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">{{ __('Leads created today') }}</span>
                <span class="info-box-number">{{$createdLeadsToday}}</span>
            </div>
            <!-- /.info-box-content -->
        </div>
    </div>
</div>
<!-- /.info-box -->
    