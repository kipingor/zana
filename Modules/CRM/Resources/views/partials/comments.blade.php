<?php $subject instanceof \Modules\CRM\Entities\Task ? $instance = 'task' : $instance = 'lead' ?>

<div class="panel panel-default">
  <div class="panel-heading">
    <div class="panel-title">{{$subject->title}}
    </div>
    <div class="panel-controls">
      <ul>
        <li><a data-toggle="close" class="portlet-close" href="#"><i class="portlet-icon portlet-icon-close"></i></a>
        </li>
      </ul>
    </div>
  </div>
  <div class="panel-body">
    <p>{{$subject->description }}</p>
    <p class="small hint-text">{{ __('Created at') }}:
        {{ date('d F, Y, H:i:s', strtotime($subject->created_at))}}
        @if($subject->updated_at != $subject->created_at)
            <br/>{{ __('Modified') }}: {{date('d F, Y, H:i:s', strtotime($subject->updated_at))}}
        @endif
    </p>
  </div>
<!-- Post a comment -->
<div class="panel-heading">
    <div class="panel-title">Add Comments
    </div>    
  </div>
  <div class="panel-body">
    @if($instance == 'task')
        {!! Form::open(array('url' => array('/crm/comments/task',$subject->id, ))) !!}
        <div class="form-group form-group-default">
            {!! Form::label('description', __('Add Comment'), ['class' => 'control-label']) !!}
            {!! Form::textarea('description', null, ['class' => 'form-control', 'id' => 'comment-field']) !!}
        </div>
        <div class="row"> 

            {!! Form::submit( __('Add Comment') , ['class' => 'btn btn-primary']) !!}
        </div>
        {!! Form::close() !!}
    @else
        {!! Form::open(array('url' => array('/crm/comments/lead',$lead->id, ))) !!}
        <div class="form-group form-group-default">
            {!! Form::label('description', __('Add Comment'), ['class' => 'control-label']) !!}
            {!! Form::textarea('description', null, ['class' => 'form-control', 'id' => 'comment-field']) !!}
        </div>
        <div class="row">
            {!! Form::submit( __('Add Comment') , ['class' => 'btn btn-primary']) !!}
        </div>
        {!! Form::close() !!}
    @endif
  </div>
<!-- View the comments -->
<div class="panel-heading">
    <div class="panel-title">Comments
    </div>
  </div>
  <div class="panel-body">
<?php $count = 0;?>
<?php $i = 1 ?>
@foreach($subject->comments as $comment)
    <p>
    <span class="small hint-text">#{{$i++}} </span>{{ $comment->description }}</p>
    <p class="small hint-text">
        {{ __('Comment by') }}: <a
                        href="{{route('users.show', $comment->user->id)}}"> {{$comment->user->name}} </a>
    </p>
    <p class="small hint-text">
        {{ __('Created on') }}:
        {{ date('d F, Y, H:i:s', strtotime($comment->created_at))}}
        @if($comment->updated_at != $comment->created_at)
                <br/>{{ __('Modified') }} : {{date('d F, Y, H:i:s', strtotime($comment->updated_at))}}
        @endif
    </p>

@endforeach
    </div>
</div>

@push('scripts')
    <script>
        $('#comment-field').atwho({
            at: "@",
            limit: 5, 
            delay: 400,
            callbacks: {
                remoteFilter: function (t, e) {
                    t.length <= 2 || $.getJSON("/users/users", {q: t}, function (t) {
                        e(t)
                    })
                }
            }
        })
    </script>
@endpush