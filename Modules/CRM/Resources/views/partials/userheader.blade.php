<div class="col-md-6">
    <div class="container-xs-height">
      <div class="row-xs-height">
        <div class="social-user-profile col-xs-height text-center col-top">
          <div class="thumbnail-wrapper d48 circular bordered b-white">
            <img alt="Avatar" width="55" height="auto" data-src-retina="{{ asset($contact->avatar) }}" data-src="{{ asset($contact->avatar) }}" src="{{ asset($contact->avatar) }}">
          </div>
          <br>
          @if($contact->isOnline())
          <i class="fa fa-check-circle text-success fs-16 m-t-10"></i>
          @else
          <i class="fa fa-times-circle-o text-default fs-16 m-t-10"></i>
          @endif
        </div>
        <div class="col-xs-height p-l-20">
          <h3 class="no-margin">{{ $contact->nameAndDepartment }}</h3>
          <p class="no-margin fs-16">{{ $contact->address }}
          </p>
          <p class="hint-text m-t-5 small"><a href="tel:{{ $contact->personal_number }}">{{ $contact->personal_number }}</a> | <a href="tel:{{ $contact->work_number }}">{{ $contact->work_number }}</a>
          </p>
          <p class="hint-text m-t-5 small"><a href="mailto:{{ $contact->email }}">{{ $contact->email }}</a> | 
          </p>
        </div>
      </div>
    </div>
</div>