<!-- BEGIN SIDEBPANEL-->
    <nav class="page-sidebar" data-pages="sidebar">
      <!-- BEGIN SIDEBAR MENU TOP TRAY CONTENT-->
      @include('layouts.admin.partials.sidebartop')
      <!-- START SIDEBAR MENU -->
      <div class="sidebar-menu">
        <!-- BEGIN SIDEBAR MENU ITEMS-->
        <ul class="menu-items">
          <li class="m-t-30 ">
            <a href="{{ url('/home') }}" class="detailed">
              <span class="title">Back Home</span>
              <span class="details">12 New Updates</span>
            </a>
            <span class="icon-thumbnail"><i class="fa fa-home"></i></span>
          </li>
          <li class="open active">
            <a href="javascript:;"><span class="title">CRM</span>
            <span class=" arrow open active"></span></a>
            <span class="bg-success icon-thumbnail"><i class="fa fa-group"></i></span>
            <ul class="sub-menu">
              <li class="">
                <a href="{{ url('/crm') }}">Dashboard</a>
                <span class="icon-thumbnail">D</span>
              </li>
              <li class="">
                <a href="{{ url('/crm/clients') }}">Accounts</a>
                <span class="icon-thumbnail"><a href="{{ url('/crm/clients/create') }}">A<i class="fa fa-plus"></i></a></span>
              </li>
              <li class="">
                <a href="{{ url('/crm/tasks') }}">Tasks</a>
                <span class="icon-thumbnail"><a href="{{ url('/crm/tasks/add') }}">T<i class="fa fa-plus"></i></a></span>
              </li>
              <li class="">
                <a href="{{ url('/crm/leads') }}">Leads</a>
                <span class="icon-thumbnail"><a href="{{ url('/crm/leads/add') }}"><i class="fa fa-plus"></i></a></span>
              </li>
              <li class="">
                <a href="{{ url('/crm/invoices') }}">Invoices</a>
                <span class="icon-thumbnail">n</span>
              </li>
            </ul>
          </li>
          <li class="">
            <a href="javascript:;"><span class="title">Menu Levels</span>
            <span class="arrow"></span></a>
            <span class="icon-thumbnail"><i class="pg-menu_lv"></i></span>
            <ul class="sub-menu">
              <li>
                <a href="javascript:;">Level 1</a>
                <span class="icon-thumbnail">L1</span>
              </li>
              <li>
                <a href="javascript:;"><span class="title">Level 2</span>
                <span class="arrow"></span></a>
                <span class="icon-thumbnail">L2</span>
                <ul class="sub-menu">
                  <li>
                    <a href="javascript:;">Sub Menu</a>
                    <span class="icon-thumbnail">Sm</span>
                  </li>
                  <li>
                    <a href="ujavascript:;">Sub Menu</a>
                    <span class="icon-thumbnail">Sm</span>
                  </li>
                </ul>
              </li>
            </ul>
          </li>
          <li class="">
            <a href="http://pages.revox.io/dashboard/2.2.0/docs/" target="_blank"><span class="title">Docs</span></a>
            <span class="icon-thumbnail"><i class="pg-note"></i></span>
          </li>
          <li class="">
            <a href="http://changelog.pages.revox.io/" target="_blank"><span class="title">Changelog</span></a>
            <span class="icon-thumbnail">Cl</span>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <!-- END SIDEBAR MENU -->
    </nav>
    <!-- END SIDEBAR -->