@extends('crm::layouts.master')
@include('crm::panels')


@section('content')
<div class="container-fluid container-fixed-lg bg-transparent">
    <div class="row">
        <div class="col-lg-4 col-md-6">
            @yield('tasks')
            @yield('leads')
        </div>
        <div class="col-lg-4 col-md-6">
            @yield('clients')
            @yield('hours')
        </div>
        <div class="col-lg-4 col-md-6">
            @yield('task-lead-stats')
        </div>
    </div>
</div>
<div class="container-fluid container-fixed-lg padding-25 sm-padding-10">
    

        <!-- Small boxes (Stat box) -->
        
        <!-- /.row -->

        <?php $createdTaskEachMonths = array(); $taskCreated = array();?>
        @foreach($createdTasksMonthly as $task)
            <?php $createdTaskEachMonths[] = $task->month; ?>
            <?php $taskCreated[] = $task->taskmonth;?>
        @endforeach

        <?php $completedTaskEachMonths = array(); $taskCompleted = array();?>

        @foreach($completedTasksMonthly as $tasks)
            <?php $completedTaskEachMonths[] = date('F', strTotime($tasks->month)); ?>
            <?php $taskCompleted[] = $tasks->taskmonth;?>
        @endforeach

        <?php $completedLeadEachMonths = array(); $leadsCompleted = array();?>
        @foreach($completedLeadsMonthly as $leads)
            <?php $completedLeadEachMonths[] = $leads->month.' '.$leads->year; ?>
            <?php $leadsCompleted[] = $leads->leadmonth;?>
        @endforeach

        <?php $createdLeadEachMonths = array(); $leadCreated = array();?>
        @foreach($createdLeadsMonthly as $lead)
            <?php $createdLeadEachMonths[] = $lead->month.' '.$leads->year; ?>
            <?php $leadCreated[] = $lead->leadmonth;?>
        @endforeach
        <div class="row">

            @include('crm::partials.dashboardone')


</div>
@endsection

@push('styles')
<link href="{{ \asset('assets/plugins/nvd3/nv.d3.min.css') }}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{ \asset('assets/plugins/mapplic/css/mapplic.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ \asset('assets/plugins/rickshaw/rickshaw.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ \asset('assets/plugins/bootstrap-datepicker/css/datepicker3.css') }}" rel="stylesheet" type="text/css" media="screen">
    <link href="{{ \asset('assets/plugins/jquery-metrojs/MetroJs.css') }}" rel="stylesheet" type="text/css" media="screen" />
@endpush
 @push('pagescripts')
 <script src="{{ \asset('assets/js/dashboard.js') }}" type="text/javascript"></script>
    <script src="{{ \asset('assets/plugins/nvd3/lib/d3.v3.js') }}" type="text/javascript"></script>
    <script src="{{ \asset('assets/plugins/nvd3/nv.d3.min.js') }}" type="text/javascript"></script>
    <script src="{{ \asset('assets/plugins/nvd3/src/utils.js') }}" type="text/javascript"></script>
    <script src="{{ \asset('assets/plugins/nvd3/src/tooltip.js') }}" type="text/javascript"></script>
    <script src="{{ \asset('assets/plugins/nvd3/src/interactiveLayer.js') }}" type="text/javascript"></script>
    <script src="{{ \asset('assets/plugins/nvd3/src/models/axis.js') }}" type="text/javascript"></script>
    <script src="{{ \asset('assets/plugins/nvd3/src/models/line.js') }}" type="text/javascript"></script>
    <script src="{{ \asset('assets/plugins/nvd3/src/models/lineWithFocusChart.js') }}" type="text/javascript"></script>
    <script src="{{ \asset('assets/plugins/mapplic/js/hammer.js') }}"></script>
    <script src="{{ \asset('assets/plugins/mapplic/js/jquery.mousewheel.js') }}"></script>
    <script src="{{ \asset('assets/plugins/mapplic/js/mapplic.js') }}"></script>
    <script src="{{ \asset('assets/plugins/rickshaw/rickshaw.min.js') }}"></script>
    <script src="{{ \asset('assets/plugins/jquery-metrojs/MetroJs.min.js') }}" type="text/javascript"></script>
    <script src="{{ \asset('assets/plugins/jquery-sparkline/jquery.sparkline.min.js') }}" type="text/javascript"></script>
    <script src="{{ \asset('assets/plugins/skycons/skycons.js') }}" type="text/javascript"></script>
    <script src="{{ \asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}" type="text/javascript"></script> 
    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script> 

    <script>
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip(); //Tooltip on icons top

            $('.popoverOption').each(function () {
                var $this = $(this);
                $this.popover({
                    trigger: 'hover',
                    placement: 'left',
                    container: $this,
                    html: true,

                });
            });
        });
    </script> 
@endpush