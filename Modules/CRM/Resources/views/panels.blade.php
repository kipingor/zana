@section('tasks')
<div class="col-md-12 m-b-10">
<div class="panel no-border no-margin widget-loader-bar">
  <div class="container-xs-height full-height">
    <div class="row-xs-height">
      <div class="col-xs-height col-top">
        <div class="panel-heading  top-left top-right">
          <div class="panel-title text-black">
            <span class="font-montserrat fs-11 all-caps">Tasks <i class="fa fa-chevron-right"></i>
        </span>
          </div>
          <div class="panel-controls">
            <ul>
              <li><a href="#" class="portlet-refresh text-black" data-toggle="refresh"><i class="portlet-icon portlet-icon-refresh"></i></a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="row-xs-height">
      <div class="col-xs-height col-top">
        <div class="p-l-20 p-t-60 p-b-40 p-r-20">
          <h1 class="no-margin p-b-5">
            @foreach($taskCompletedThisMonth as $thisMonth)
                {{$thisMonth->total}}
            @endforeach            
          </h1>
          <span class="small hint-text pull-left">{{ __('Tasks completed this month') }}</span>
          <span class="pull-right small text-primary">
            <a href="{{route('tasks.index')}}" class="small-box-footer">
                {{ __('All Tasks') }} <i class="fa fa-arrow-circle-right"></i>
            </a>
          </span>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection
@section('leads')
<div class="col-md-12 m-b-10">
<div class="panel no-border no-margin widget-loader-bar">
  <div class="container-xs-height full-height">
    <div class="row-xs-height">
      <div class="col-xs-height col-top">
        <div class="panel-heading  top-left top-right">
          <div class="panel-title text-black">
            <span class="font-montserrat fs-11 all-caps">Leads <i class="fa fa-chevron-right"></i>
        </span>
          </div>
          <div class="panel-controls">
            <ul>
              <li><a href="#" class="portlet-refresh text-black" data-toggle="refresh"><i class="portlet-icon portlet-icon-refresh"></i></a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="row-xs-height">
      <div class="col-xs-height col-top">
        <div class="p-l-20 p-t-60 p-b-40 p-r-20">
          <h1 class="no-margin p-b-5">
            @foreach($leadCompletedThisMonth as $thisMonth)
                {{$thisMonth->total}}
            @endforeach
          </h1>
          <span class="small hint-text pull-left">{{ __('Leads completed this month') }}</span>
          <span class="pull-right small text-primary">
            <a href="{{route('leads.index')}}" class="small-box-footer">
                {{ __('All Leads') }} <i class="fa fa-arrow-circle-right"></i>
            </a>
          </span>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection

@section('clients')
<div class="col-md-12 m-b-10">
<div class="panel no-border no-margin widget-loader-bar">
  <div class="container-xs-height full-height">
    <div class="row-xs-height">
      <div class="col-xs-height col-top">
        <div class="panel-heading  top-left top-right">
          <div class="panel-title text-black">
            <span class="font-montserrat fs-11 all-caps">Clients <i class="fa fa-chevron-right"></i>
        </span>
          </div>
          <div class="panel-controls">
            <ul>
              <li><a href="#" class="portlet-refresh text-black" data-toggle="refresh"><i class="portlet-icon portlet-icon-refresh"></i></a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="row-xs-height">
      <div class="col-xs-height col-top">
        <div class="p-l-20 p-t-60 p-b-40 p-r-20">
          <h1 class="no-margin p-b-5">
            {{$totalClients}}
          </h1>
          <span class="small hint-text pull-left">{{ __('Total Clients') }}</span>
          <span class="pull-right small text-primary">
            <a href="{{route('leads.index')}}" class="small-box-footer">
                {{ __('All Leads') }} <i class="fa fa-arrow-circle-right"></i>
            </a>
          </span>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection

@section('hours')
<div class="col-md-12 m-b-10">
<div class="panel no-border no-margin widget-loader-bar">
  <div class="container-xs-height full-height">
    <div class="row-xs-height">
      <div class="col-xs-height col-top">
        <div class="panel-heading  top-left top-right">
          <div class="panel-title text-black">
            <span class="font-montserrat fs-11 all-caps">Hours <i class="fa fa-chevron-right"></i>
            </span>
          </div>
          <div class="panel-controls">
            <ul>
              <li><a href="#" class="portlet-refresh text-black" data-toggle="refresh"><i class="portlet-icon portlet-icon-refresh"></i></a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="row-xs-height">
      <div class="col-xs-height col-top">
        <div class="p-l-20 p-t-60 p-b-40 p-r-20">
          <h1 class="no-margin p-b-5">
            @foreach($totalTimeSpent[0] as $sum => $value)
                {{$value}}
            @endforeach
            @if($value == "")
                0
            @endif
          </h1>
          <span class="small hint-text pull-left">{{ __('Total hours registered') }}</span>
          <span class="pull-right small text-primary">
            <a href="#" class="small-box-footer"> {{ __('More info') }} <i
                class="fa fa-arrow-circle-right"></i>
            </a>
          </span>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection

@section('task-lead-stats')
<div class="col-md-12 m-b-10">
  <div class=" panel no-border  no-margin widget-loader-circle todolist-widget">    
    <div class="panel-body">
      <h5 class="">Ongoing at <span class="semi-bold">Zana</span></h5>
      
        
            <div class="p-t-25">
              <div class="">
                <span class="icon-thumbnail bg-master-light pull-left text-master">at</span>
                <div class="pull-left">
                  <p class="hint-text all-caps font-montserrat  small no-margin overflow-ellipsis ">{{ __('All Tasks') }}
                  </p>
                  <h5 class="no-margin overflow-ellipsis ">{{$allCompletedTasks}} / {{$alltasks}} {{ __('of all tasks are compleated') }}</h5>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="m-t-25">
                <p class="hint-text fade small pull-left">{{number_format($totalPercentageTasks, 0)}}% {{ __('Completed') }}</p>
                <a href="#" class="pull-right text-master"><i class="pg-more"></i></a>
                <div class="clearfix"></div>
              </div>
              <div class="progress progress-small m-b-20 m-t-10">
                <!-- START BOOTSTRAP PROGRESS (http://getbootstrap.com/components/#progress) -->
                <div class="progress-bar progress-bar-info" style="width:{{$totalPercentageTasks}}%"></div>
                <!-- END BOOTSTRAP PROGRESS -->
              </div>
            </div>

            <div class="p-t-25">
              <div class="">
                <span class="icon-thumbnail bg-warning-light pull-left text-master">al</span>
                <div class="pull-left">
                  <p class="hint-text all-caps font-montserrat  small no-margin overflow-ellipsis ">{{ __('All Leads') }}
                  </p>
                  <h5 class="no-margin overflow-ellipsis ">{{$allCompletedLeads}} / {{$allleads}} {{ __('of all leads are compleated') }}</h5>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="m-t-25">
                <p class="hint-text fade small pull-left">{{number_format($totalPercentageLeads, 0)}}% {{ __('Completed') }}</p>
                <a href="#" class="pull-right text-master"><i class="pg-more"></i></a>
                <div class="clearfix"></div>
              </div>
              <div class="progress progress-small m-b-20 m-t-10">
                <!-- START BOOTSTRAP PROGRESS (http://getbootstrap.com/components/#progress) -->
                <div class="progress-bar progress-bar-warning" style="width:{{$totalPercentageLeads}}%"></div>
                <!-- END BOOTSTRAP PROGRESS -->
              </div>
            </div>

            
          </div>
        </div>
      </div>
@endsection