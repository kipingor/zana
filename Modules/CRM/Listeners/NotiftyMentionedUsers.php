<?php

namespace Modules\CRM\Listeners;

use Zana\User;
use Modules\CRM\Events\NewComment;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\CRM\Notifications\YouWereMentionedNotification;

class NotiftyMentionedUsers
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(NewComment $event)
    {
        collect($event->comment->mentionedUsers())
        ->map(function ($name) {
            return User::where('name', $name)->first();
        })
        ->filter()
        ->each(function ($user) use ($event) {
            $user->notify(new YouWereMentionedNotification($event->comment));
        });
    }
}
