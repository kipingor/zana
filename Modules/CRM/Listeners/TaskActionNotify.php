<?php

namespace Modules\CRM\Listeners;

use Modules\CRM\Events\TaskAction;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\CRM\Notifications\TaskActionNotification;

class TaskActionNotify
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(TaskAction $event)
    {
        $task = $event->getTask();
        $action = $event->getAction();
        $task->assignedUser->notify(new TaskActionNotification(
            $task,
            $action
        ));
    }
}
