<?php

namespace Modules\CRM\Listeners;

use Modules\CRM\Events\ClientAction;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\CRM\Notifications\ClinetActionNotifications;

class ClientActionNotify
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(clientAction $event)
    {
        $client = $event->getClient();
        $action = $event->getAction();
        
        $client->assignedUser->notify(new clientActionNotification(
            $client,
            $action
        ));
    }
}
