<?php

Route::group(['middleware' => 'web', 'prefix' => 'crm', 'namespace' => 'Modules\CRM\Http\Controllers'], function()
{
    Route::get('/', 'CRMController@index');

    /**
     * Main
     */
        Route::get('/', 'CRMController@index');
        Route::get('index', 'CRMController@index')->name('dashboard');
        

	
    /**
     * Clients
     */
    Route::group(['prefix' => 'clients'], function () {
        Route::get('/', 'ClientsController@index')->name('clients.index');
        Route::get('/data', 'ClientsController@anyData')->name('clients.data');
        Route::get('/create', 'ClientsController@create')->name('clients.create');
        Route::post('/create/cvrapi', 'ClientsController@cvrapiStart');
        Route::post('/upload/{id}', 'DocumentsController@upload');
        Route::patch('/updateassign/{id}', 'ClientsController@updateAssign');
        Route::get('/{client}', 'ClientsController@show');
    });
        Route::resource('clients', 'ClientsController');
	    Route::resource('documents', 'DocumentsController');
	
      
    /**
     * Tasks
     */
    Route::group(['prefix' => 'tasks'], function () {
        Route::get('/data', 'TasksController@anyData')->name('tasks.data');
        Route::get('/add', 'TasksController@create');
        Route::get('/{id}', 'TasksController@show')->name
        ('tasks.show');
        Route::patch('/updatestatus/{id}', 'TasksController@updateStatus');
        Route::patch('/updateassign/{id}', 'TasksController@updateAssign');
        Route::post('/updatetime/{id}', 'TasksController@updateTime');
    });
        Route::resource('tasks', 'TasksController');

    /**
     * Leads
     */
    Route::group(['prefix' => 'leads'], function () {
        Route::get('/data', 'LeadsController@anyData')->name('leads.data');
        Route::get('/add', 'LeadsController@create');
        Route::get('/{id}', 'LeadsController@show')->name('leads.show');
        Route::patch('/updateassign/{id}', 'LeadsController@updateAssign');
        Route::patch('/updatestatus/{id}', 'LeadsController@updateStatus');
        Route::patch('/updatefollowup/{id}', 'LeadsController@updateFollowup')->name('leads.followup');
    });
        Route::resource('leads', 'LeadsController');
        Route::post('/comments/{type}/{id}', 'CommentController@store');
     

    /**
     * Integrations
     */
    Route::group(['prefix' => 'integrations'], function () {
        Route::get('Integration/slack', 'IntegrationsController@slack');
    });
        Route::resource('integrations', 'IntegrationsController');

    

    /**
     * Invoices
     */
    Route::group(['prefix' => 'invoices'], function () {
        Route::patch('/updatepayment/{id}', 'InvoicesController@updatePayment')->name('invoice.payment.date');
        Route::patch('/reopenpayment/{id}', 'InvoicesController@reopenPayment')->name('invoice.payment.reopen');
        Route::post('/sentinvoice/{id}', 'InvoicesController@updateSentStatus')->name('invoice.sent');
        Route::post('/newitem/{id}', 'InvoicesController@newItem')->name('invoice.new.item');
    });
        Route::resource('invoices', 'InvoicesController');
});
