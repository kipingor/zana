<?php

namespace Modules\CRM\Http\Middleware;

use Zana\Setting;
use Modules\CRM\Entities\Lead;
use Closure;
use Illuminate\Http\Request;

class IsLeadAssigned
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $lead = Lead::findOrFail($request->id);
        $settings = Setting::all();
        $isAdmin = Auth()->user()->hasRole('admin');
        $settingscomplete = $settings[0]['lead_assign_allowed'];
        if ($isAdmin) {
            return $next($request);
        }
        if ($settingscomplete == 1  && Auth()->user()->id == $lead->fk_user_id_assign) {
            Session()->flash('flash_message_warning', 'Not allowed to create lead');
            return redirect()->back();
        }
        return $next($request);
    }
}
