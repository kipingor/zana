<?php

namespace Modules\CRM\Http\Controllers;

use Auth;
use Session;
use Modules\CRM\Entities\Task;
use Modules\CRM\Entities\Lead;
use Zana\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Modules\CRM\Entities\Comment;

class CommentController extends Controller
{
    /**
     * Create a comment for tasks and leads
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function store(Request $request)
    {   
        // $this->validate($request, [
        //     'description' => 'required'
        // ]);

        $source = $request->type == "task" ? Task::find($request->id) : Lead::find($request->id); 
        $comment = $source->addComment(['description' => $request->description, 'user_id' => auth()->user()->id]);
        event(new \Modules\CRM\Events\NewComment($comment));
        Session::flash('flash_message', 'Comment successfully added!'); //Snippet in Master.blade.php
        return redirect()->back();
    }
}
