<?php

namespace Modules\CRM\Http\Controllers;

use DB;
use Auth;
use Carbon\Carbon;
use Session;
use Datatables;
use Modules\CRM\Entities\Lead;
use Illuminate\Http\Request;
use Modules\CRM\Requests;
use Modules\CRM\Http\Requests\StoreLeadRequest;
use Modules\CRM\Repositories\Lead\LeadRepositoryContract;
use Modules\CRM\Repositories\User\UserRepositoryContract;
use Modules\CRM\Http\Requests\UpdateLeadFollowUpRequest;
use Modules\CRM\Repositories\Client\ClientRepositoryContract;
use Modules\CRM\Repositories\Setting\SettingRepositoryContract;
use Illuminate\Routing\Controller;
use Zana\User;

class LeadsController extends Controller
{
    protected $leads;
    protected $clients;
    protected $settings;
    protected $users;

    public function __construct(
        LeadRepositoryContract $leads,
        UserRepositoryContract $users,
        ClientRepositoryContract $clients,
        SettingRepositoryContract $settings
    )
    {
        $this->users = $users;
        $this->settings = $settings;
        $this->clients = $clients;
        $this->leads = $leads;
        $this->middleware('auth');
        $this->middleware('lead.create', ['only' => ['create']]);
        $this->middleware('lead.assigned', ['only' => ['updateAssign']]);
        $this->middleware('lead.update.status', ['only' => ['updateStatus']]);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $leads = Lead::all();
        return view('crm::leads.index', compact('leads'));
    }

    /**
     * Data for Data tables
     * @return mixed
     */
    public function anyData()
    {
        $leads = Lead::select(
            ['id', 'title', 'user_created_id', 'client_id', 'user_assigned_id', 'contact_date']
        )->where('status', 1)->get();
        return Datatables::of($leads)
            ->addColumn('titlelink', function ($leads) {
                return '<a href="crm/leads/' . $leads->id . '" ">' . $leads->title . '</a>';
            })
            ->editColumn('user_created_id', function ($leads) {
                return $leads->creator->name;
            })
            ->editColumn('contact_date', function ($leads) {
                return $leads->contact_date ? with(new Carbon($leads->contact_date))
                    ->format('d/m/Y') : '';
            })
            ->editColumn('user_assigned_id', function ($leads) {
                return $leads->user->name;
            })->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::pluck('name','id');
        return view('crm::leads.create', compact('users'))            
            ->withClients($this->clients->listAllClients());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreLeadRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreLeadRequest $request)
    {
        $getInsertedId = $this->leads->create($request);
        Session()->flash('flash_message', 'Lead is created');
        return redirect()->route('leads.show', $getInsertedId);
    }

    public function updateAssign($id, Request $request)
    {
        $this->leads->updateAssign($id, $request);
        Session()->flash('flash_message', 'New user is assigned');
        return redirect()->back();
    }

    /**
     * Update the follow up date (Deadline)
     * @param UpdateLeadFollowUpRequest $request
     * @param $id
     * @return mixed
     */
    public function updateFollowup(UpdateLeadFollowUpRequest $request, $id)
    {
        $this->leads->updateFollowup($id, $request);
        Session()->flash('flash_message', 'New follow up date is set');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $users = User::pluck('name','id');
        return view('crm::leads.show', compact('users'))
            ->withLead($this->leads->find($id))
            ->withCompanyname($this->settings->getCompanyName());
    }

    /**
     * Complete lead
     * @param $id
     * @param Request $request
     * @return mixed
     */
    public function updateStatus($id, Request $request)
    {
        $this->leads->updateStatus($id, $request);
        Session()->flash('flash_message', 'Lead is completed');
        return redirect()->back();
    }
}
