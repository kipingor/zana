<?php

namespace Modules\CRM\Http\Controllers;

use Config;
use Datatables;
use Illuminate\Http\Request;
use Modules\CRM\Repositories\User\UserRepositoryContract;
use Modules\CRM\Repositories\Client\ClientRepositoryContract;
use Modules\CRM\Repositories\Setting\SettingRepositoryContract;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\CRM\Entities\Client;
use Modules\CRM\Entities\Industry;
use Zana\Setting;
use Modules\CRM\Http\Requests;
use Modules\CRM\Http\Requests\StoreClientRequest;
use Modules\CRM\Http\Requests\UpdateClientRequest;
use Zana\User;


class ClientsController extends Controller
{
    protected $users;
    protected $clients;
    protected $settings;

    public function __construct(
        UserRepositoryContract $users,
        ClientRepositoryContract $clients,
        SettingRepositoryContract $settings
    )
    {
        $this->middleware('auth');
        $this->users = $users;
        $this->clients = $clients;
        $this->settings = $settings;
        $this->middleware('client.create', ['only' => ['create']]);
        $this->middleware('client.update', ['only' => ['edit']]);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $clients = Client::all();
        $settings = Setting::all();
        return view('crm::clients.index', compact(
            'clients'
        ));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $users = User::pluck('name','id');
       return view('crm::clients.create', compact('users'))            
            ->withIndustries($this->clients->listAllIndustries());
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(StoreClientRequest $request)
    {

       $this->clients->create($request->all());
        return redirect()->route('clients.index');
    }

    /**
     * @param Request $vatRequest
     * @return mixed
     */
    public function cvrapiStart(Request $vatRequest)
    {
        return redirect()->back()
            ->with('data', $this->clients->vat($vatRequest));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return mixed
     */
    public function show($id)
    {
        return view('crm::clients.show')
            ->withClient($this->clients->find($id))
            ->withCompanyname($this->settings->getCompanyName())
            ->withInvoices($this->clients->getInvoices($id))
            ->withUsers($this->users->getAllUsersWithDepartments());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return mixed
     */
    public function edit($id)
    {
        $users = User::pluck('name','id');
        return view('crm::clients.edit', compact('users'))
            ->withClient($this->clients->find($id))
            ->withIndustries($this->clients->listAllIndustries());
    }

    /**
     * @param $id
     * @param UpdateClientRequest $request
     * @return mixed
     */
    public function update($id, UpdateClientRequest $request)
    {
        $this->clients->update($id, $request);
        Session()->flash('flash_message', 'Client successfully updated');
        return redirect()->route('clients.index');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $this->clients->destroy($id);

        return redirect()->route('clients.index');
    }

    /**
     * @param $id
     * @param Request $request
     * @return mixed
     */
    public function updateAssign($id, Request $request)
    {
        $this->clients->updateAssign($id, $request);
        Session()->flash('flash_message', 'New user is assigned');
        return redirect()->back();
    }
}
