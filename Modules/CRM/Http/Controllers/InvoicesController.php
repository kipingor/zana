<?php

namespace Modules\CRM\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\CRM\Repositories\Invoice\InvoiceRepositoryContract;
use Modules\CRM\Repositories\Client\ClientRepositoryContract;
use Modules\CRM\Entities\Integration;
use Modules\CRM\Http\Requests;
use Modules\CRM\Emails\Invoice\Send;
use Modules\CRM\Entities\Invoice;

class InvoicesController extends Controller
{
    protected $clients;
    protected $invoices;
    public function __construct(
        InvoiceRepositoryContract $invoices,
        ClientRepositoryContract $clients
    )
    {
        $this->middleware('auth');
        $this->invoices = $invoices;
        $this->clients = $clients;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $invoice = Invoice::all();
        return view('crm::invoices.index', compact('invoice'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $this->invoices->create('test');
        // return view('crm::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Invoice $invoice)
    {
        
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        $integrationCheck = Integration::first();
        $invoice = $this->invoices->find($id);
        if ($integrationCheck) {
            $api = Integration::getApi('billing');
            $apiConnected = true;
            $invoiceContacts = $api->getContacts($invoice->client->email);
            // If we can't find a client in the integration, show all
            if (!$invoiceContacts) {
                $invoiceContacts = $api->getContacts();
            }
            
        } else {
            $apiConnected = false;
            $invoiceContacts = [];
        }
        return view('crm::invoices.show')
            ->withInvoice($invoice)
            ->withApiconnected($apiConnected)
            ->withContacts($invoiceContacts);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('crm::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    /**
     * Closed open payment
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function updatePayment(Request $request, $id)
    {
        $this->invoices->updatePayment($id, $request);
        return redirect()->back();
    }

    /**
     * Reopen closed payment
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function reopenPayment(Request $request, $id)
    {
        $this->invoices->reopenPayment($id, $request);
        return redirect()->back();
    }

    /**
     * Update the sent status
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function updateSentStatus($id, Request $request)
    {
        $invoice = $this->invoices->find($id);
        $client = $invoice->client;
        $user = auth()->user();
        $this->invoices->updateSentStatus($id, $request);
        \Mail::to($client)->send(new Send($client, $invoice, $user));
        Session()->flash('flash_message', 'Invoice Has been sent');
        return redirect()->back();
    }

    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function updateSentReopen($id, Request $request)
    {
        Invoice::updateSentReopen($id, $request);
        return redirect()->back();
    }

    /**
     * Add new invoice line
     * @param $id
     * @param Request $request
     * @return mixed
     */
    public function newItem($id, Request $request)
    {
        $this->invoices->newItem($id, $request);
        return redirect()->back();
    }
}
