<?php

namespace Modules\CRM\Entities;

use Illuminate\Database\Eloquent\Model;
use Zana\User;

class Department extends Model
{
    protected $fillable =
        [
            'name',
            'description'
        ];
}
