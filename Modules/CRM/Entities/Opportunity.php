<?php

namespace Modules\CRM\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Zana\User;

class Opportunity extends Model
{
	use SoftDeletes;
	
	// protected $table = 'opportunities';
	
	protected $hidden = [];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

    protected $fillable = [];

    /**
     * Get the Employee assigned to this Opportunity
     */
    public function assigned_to_emp()
    {
        return $this->belongsTo('Modules\CRM\Entities\Employee', 'assigned_to', 'id');
    }

	/**
     * Get the Organization assigned to this Opportunity
     */
    public function organization_info()
    {
        return $this->belongsTo('Modules\CRM\Entities\Organization', 'organization', 'id');
    }

	/**
     * Get the Contact assigned to this Opportunity
     */
    public function contact_info()
    {
        return $this->belongsTo('Modules\CRM\Entities\Contact', 'contact', 'id');
    }
}
