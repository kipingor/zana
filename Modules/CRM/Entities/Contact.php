<?php

namespace Modules\CRM\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Zana\User;

class Contact extends Model
{
	use SoftDeletes;

	// protected $table = 'contacts';

	protected $hidden = [];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

    protected $fillable = [];

    /**
     * Get the Employee assigned to this Contact
     */
    public function assigned_to_emp()
    {
        return $this->belongsTo('Modules\CRM\Entities\Employee', 'assigned_to', 'id');
    }

    /**
     * Get the Organization belongs to this Contact
     */
    public function organization_info()
    {
        return $this->belongsTo('Modules\CRM\Entities\Organization', 'organization', 'id');
    }

    /**
     * Get the Opportunities hasMany with Contact
     */
	public function opportunities()
	{
		return $this->hasMany('Modules\CRM\Entities\Opportunity', 'contact', 'id');
	}
}
