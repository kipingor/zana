<?php

namespace Modules\CRM\Entities;

use Illuminate\Database\Eloquent\Model;
use Zana\User;

class Invoice extends Model
{
    protected $fillable = [
	    'invoiced_at',
	    'invoice_status_code',
	    'amount',
	    'due_at',
	    'customer_id',
    ];

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function invoiceLines()
    {
        return $this->hasMany(InvoiceLine::class);
    }

    public function canUpdateInvoice()
    {
        if ($this->sent_at != null) {
            return false;
        }
        return true;
    }
}
