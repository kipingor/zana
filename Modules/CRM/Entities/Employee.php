<?php

namespace Modules\CRM\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Zana\User;

class Employee extends Model
{
    use SoftDeletes;
	
	// protected $table = 'employees';
	
	protected $hidden = [];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

    protected $fillable = [];

    /**
     * Get the Lead associated with Employee
     */
	public function leads()
	{
		return $this->hasMany('Modules\CRM\Entities\Lead', 'assigned_to', 'id');
	}
	/**
	 * Get the Opportunities associated with Employee
     */
	public function opportunities()
	{
		return $this->hasMany('Modules\CRM\Entities\Opportunity', 'assigned_to', 'id');
	}

	/**
     * Get the Projects associated with Employee
     */
	public function projects()
	{
		return $this->hasMany('Modules\CRM\Entities\Project', 'assigned_to', 'id');
	}

	/**
     * Get the Contacts associated with Employee
     */
	public function contacts()
	{
		return $this->hasMany('Modules\CRM\Entities\Contact', 'assigned_to', 'id');
	}

	/**
     * Get the Organizations associated with Employee
     */
	public function organizations()
	{
		return $this->hasMany('Modules\CRM\Entities\Organization', 'assigned_to', 'id');
	}
}
