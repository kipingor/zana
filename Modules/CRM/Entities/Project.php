<?php

namespace Modules\CRM\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Zana\User;

class Project extends Model
{
	 use SoftDeletes;
	
	// protected $table = 'projects';
	
	protected $hidden = [];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

    protected $fillable = [];

    /**
     * Get the Employee assigned to this Project
     */
    public function assigned_to_emp()
    {
        return $this->belongsTo('Modules\CRM\Entities\Employee', 'assigned_to', 'id');
    }

	/**
     * Get the Organization belongs to this Project
     */
    public function organization_info()
    {
        return $this->belongsTo('Modules\CRM\Entities\Organization', 'organization', 'id');
    }
}
