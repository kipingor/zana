<?php

namespace Modules\CRM\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class CRMDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

            // $this->call("OthersTableSeeder");
            // $this->call('UsersTableSeeder');
            // $this->call('Modules\CRM\Database\Seeders\IndustriesTableSeeder');
            // $this->call('Modules\CRM\Database\Seeders\DepartmentsTableSeeder');
            // $this->call('Modules\CRM\Database\Seeders\SettingsTableSeeder');
            // $this->call('Modules\CRM\Database\Seeders\PermissionsTableSeeder');
            // $this->call('Modules\CRM\Database\Seeders\RolesTablesSeeder');
            // $this->call('Modules\CRM\Database\Seeders\RolePermissionTableSeeder');
            // $this->call('Modules\CRM\Database\Seeders\UserRoleTableSeeder');
            $this->call('Modules\CRM\Database\Seeders\LeadsTableSeeder');
            // $this->call('Modules\CRM\Database\Seeders\TasksDummyTableSeeder');

        Model::reguard();
    }
}
