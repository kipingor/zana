<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->string('designation');
            $table->string('gender', 10);
            $table->string('phone_primary', 50);
            $table->string('phone_secondary', 50)->nullable();
            $table->string('email', 200);
            $table->integer('dept');
            $table->string('city', 50)->nullable();
            $table->string('address', 255)->nullable();
            $table->text('about')->nullable();
            $table->date('date_birth')->nullable();
            $table->date('date_hire')->nullable();
            $table->date('date_left')->nullable();
            $table->integer('salary_cur')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
