<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpportunitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opportunities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->integer('organization')->nullable();
            $table->integer('contact')->nullable();
            $table->integer('amount')->nullable();
            $table->date('expected_close_date')->nullable();
            $table->text('next_step')->nullable();
            $table->integer('assigned_to')->nullable();
            $table->string('type')->nullable();
            $table->string('lead_source')->nullable();
            $table->string('sales_stage')->nullable();
            $table->integer('probability')->nullable();
            $table->integer('forecast_amount')->nullable();
            $table->text('description')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('opportunities');
    }
}
