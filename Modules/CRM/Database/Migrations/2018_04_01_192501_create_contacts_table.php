<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('designation', 10);
            $table->string('first_name', 100)->nullable();
            $table->string('last_name', 100);
            $table->string('title', 255)->nullable();
            $table->integer('organization')->nullable;
            $table->string('phone_primary', 50);
            $table->string('email', 255);
            $table->integer('lead_source');
            $table->string('department', 255)->nullable();
            $table->string('phone_secondary', 50)->nullable();
            $table->string('home_phone', 50)->nullable();
            $table->string('email2', 50)->nullable();
            $table->date('dob')->nullable();
            $table->string('assistant', 100)->nullable();
            $table->string('assistant_phone', 100)->nullable();
            $table->integer('assigned_to');
            $table->string('address', 255)->nullable();
            $table->string('city', 50)->nullable();
            $table->string('country', 50)->nullable();
            $table->text('description')->nullable();
            $table->string('profile_picture')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
