<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->string('email_primary', 200)->nullable();
            $table->string('email_secondary', 200)->nullable();
            $table->string('phone_primary', 50)->nullable();
            $table->string('phone_secondary', 50)->nullable();
            $table->string('website', 50)->nullable();
            $table->string('type', 100)->nullable();
            $table->integer('assigned_to')->nullable();
            $table->date('connected_since')->nullable();
            $table->string('address', 255)->nullable();
            $table->string('city', 100);
            $table->string('country', 100)->nullable();
            $table->string('postal_code', 50)->nullable();
            $table->text('description')->nullable();
            $table->string('profile_image', 100)->nullable();
            $table->string('profile', 200)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organizations');
    }
}
