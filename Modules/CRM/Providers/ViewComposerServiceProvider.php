<?php

namespace Modules\CRM\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    // protected $defer = false;

    public function boot()
    {
        view()->composer(
            ['crm::clients.show'],
            'Modules\CRM\Http\ViewComposers\ClientHeaderComposer'
        );
        view()->composer(
            ['crm::tasks.show'],
            'Modules\CRM\Http\ViewComposers\TaskHeaderComposer'
        );
        view()->composer(
            ['crm::leads.show'],
            'Modules\CRM\Http\ViewComposers\LeadHeaderComposer'
        );
        view()->composer(
            ['crm::invoices.show'],
            'Modules\CRM\Http\ViewComposers\InvoiceHeaderComposer'
        );
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
