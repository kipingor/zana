<?php

namespace Modules\CRM\Providers;

use Illuminate\Support\ServiceProvider;

class AccessServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            \Modules\CRM\Repositories\User\UserRepositoryContract::class,
            \Modules\CRM\Repositories\User\UserRepository::class
        );
        $this->app->bind(
            \Modules\CRM\Repositories\Role\RoleRepositoryContract::class,
            \Modules\CRM\Repositories\Role\RoleRepository::class
        );
        $this->app->bind(
            \Modules\CRM\Repositories\Department\DepartmentRepositoryContract::class,
            \Modules\CRM\Repositories\Department\DepartmentRepository::class
        );
        $this->app->bind(
            \Modules\CRM\Repositories\Setting\SettingRepositoryContract::class,
            \Modules\CRM\Repositories\Setting\SettingRepository::class
        );
        $this->app->bind(
            \Modules\CRM\Repositories\Task\TaskRepositoryContract::class,
            \Modules\CRM\Repositories\Task\TaskRepository::class
        );
        $this->app->bind(
            \Modules\CRM\Repositories\Client\ClientRepositoryContract::class,
            \Modules\CRM\Repositories\Client\ClientRepository::class
        );
        $this->app->bind(
            \Modules\CRM\Repositories\Lead\LeadRepositoryContract::class,
            \Modules\CRM\Repositories\Lead\LeadRepository::class
        );
        $this->app->bind(
            \Modules\CRM\Repositories\Invoice\InvoiceRepositoryContract::class,
            \Modules\CRM\Repositories\Invoice\InvoiceRepository::class
        );
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
