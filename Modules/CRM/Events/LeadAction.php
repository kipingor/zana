<?php

namespace Modules\CRM\Events;

use Modules\CRM\Entities\Lead;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;


class LeadAction
{
    private $lead;
    private $action;

    use InteractsWithSockets, SerializesModels;

    public function getLead()
    {
        return $this->lead;
    }
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Lead $lead, $action)
    {
        $this->lead = $lead;
        $this->action = $action;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
