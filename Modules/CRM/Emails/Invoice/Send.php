<?php

namespace Modules\CRM\Emails\Invoice;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\CRM\Entities\Client;
use Modules\CRM\Entities\Invoice;
use Zana\User;

class Send extends Mailable
{
    use Queueable, SerializesModels;

    public $client;
    public $invoice;
    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(
        Client $client,
        Invoice $invoice,
        User $user
    )
    {
        $this->client = $client;
        $this->invoice = $invoice;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('crm::emails.invoices.send');
    }
}
