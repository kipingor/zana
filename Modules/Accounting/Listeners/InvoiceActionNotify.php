<?php

namespace Modules\Accounting\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Accounting\Events\InvoiceAction;
use Modules\Accounting\Notifications\InvoiceActionNotification;
use Modules\Accounting\Notifications\NewCustomerInvoiceNotification;

class InvoiceActionNotify
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(InvoiceAction $event)
    {
        $invoice = $event->getInvoice();
        $action = $event->getAction();
        auth()->user()->notify(new InvoiceActionNotification(
            $invoice,
            $action
        ));
    }
}
