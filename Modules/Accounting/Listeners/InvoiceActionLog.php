<?php

namespace Modules\Accounting\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Accounting\Events\InvoiceAction;
use Lang;
use Modules\CRM\Entities\Activity;
use Modules\Accounting\Entities\Invoice;

class InvoiceActionLog
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(InvoiceAction $event)
    {
        $text = __(':invoice was created for :customer', [
            'invoice' => $event->getInvoice()->invoice_number,
            'customer' => $event->getInvoice()->customer_name
            ]);
        $activityinput = array_merge(
            [
                'text' => $text,
                'user_id' => Auth()->id(),
                'source_type' =>  Invoice::class,
                'source_id' =>  $event->getInvoice()->id,
                'action' => $event->getAction()
            ]
        );
        
        Activity::create($activityinput);
    }
}
