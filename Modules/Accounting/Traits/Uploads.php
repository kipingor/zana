<?php

namespace Modules\Accounting\Traits;

use MediaUploader;
// use Zana\Setting;

trait Uploads
{

    public function getUploadedFilePath($file, $folder = 'settings')
    {
        $path = '';

        if (!$file || !$file->isValid()) {
            return $path;
        }

        
        $file_name = $file->getClientOriginalName();

        // Upload file
        $file->storeAs('/' . $folder, $file_name);

        // Prepare db path
        $path = $folder . '/' . $file_name;

        return $path;
    }

    public function getMedia($file, $folder = 'settings', $company_id = null)
    {
        $path = '';

        if (!$file || !$file->isValid()) {
            return $path;
        }


        $path = '/' . $folder;

        return MediaUploader::fromSource($file)->toDirectory($path)->upload();
    }
}
