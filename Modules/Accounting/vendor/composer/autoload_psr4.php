<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Symfony\\Polyfill\\Mbstring\\' => array($vendorDir . '/symfony/polyfill-mbstring'),
    'Symfony\\Component\\Translation\\' => array($vendorDir . '/symfony/translation'),
    'Symfony\\Component\\Finder\\' => array($vendorDir . '/symfony/finder'),
    'Symfony\\Component\\Debug\\' => array($vendorDir . '/symfony/debug'),
    'Symfony\\Component\\Console\\' => array($vendorDir . '/symfony/console'),
    'Sofa\\Hookable\\' => array($vendorDir . '/sofa/hookable/src'),
    'Sofa\\Eloquence\\' => array($vendorDir . '/sofa/eloquence-base/src', $vendorDir . '/sofa/eloquence-mappable/src', $vendorDir . '/sofa/eloquence-metable/src', $vendorDir . '/sofa/eloquence-mutable/src', $vendorDir . '/sofa/eloquence-validable/src'),
    'Psr\\SimpleCache\\' => array($vendorDir . '/psr/simple-cache/src'),
    'Psr\\Log\\' => array($vendorDir . '/psr/log/Psr/Log'),
    'Psr\\Container\\' => array($vendorDir . '/psr/container/src'),
    'Modules\\Accounting\\' => array($baseDir . '/'),
    'Kyslik\\ColumnSortable\\' => array($vendorDir . '/kyslik/column-sortable/src/ColumnSortable'),
    'Illuminate\\View\\' => array($vendorDir . '/illuminate/view'),
    'Illuminate\\Support\\' => array($vendorDir . '/illuminate/support'),
    'Illuminate\\Filesystem\\' => array($vendorDir . '/illuminate/filesystem'),
    'Illuminate\\Events\\' => array($vendorDir . '/illuminate/events'),
    'Illuminate\\Database\\' => array($vendorDir . '/illuminate/database'),
    'Illuminate\\Contracts\\' => array($vendorDir . '/illuminate/contracts'),
    'Illuminate\\Container\\' => array($vendorDir . '/illuminate/container'),
    'Illuminate\\Console\\' => array($vendorDir . '/illuminate/console'),
    'Illuminate\\Config\\' => array($vendorDir . '/illuminate/config'),
    'EloquentFilter\\' => array($vendorDir . '/tucker-eric/eloquentfilter/src'),
    'Doctrine\\Common\\Inflector\\' => array($vendorDir . '/doctrine/inflector/lib/Doctrine/Common/Inflector'),
    'Bkwld\\Cloner\\' => array($vendorDir . '/bkwld/cloner/src'),
    'Akaunting\\Money\\' => array($vendorDir . '/akaunting/money/src'),
    '' => array($vendorDir . '/nesbot/carbon/src'),
);
