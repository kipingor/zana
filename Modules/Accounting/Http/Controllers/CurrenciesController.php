<?php

namespace Modules\Accounting\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Akaunting\Money\Currency as MoneyCurrency;
use Modules\Accounting\Http\Requests\Currency as CurrencyRequest;
use Modules\Accounting\Entities\Account;
use Modules\Accounting\Entities\Currency;


class CurrenciesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $currencies = Currency::all();
        return view('accounting::currencies.index', compact('currencies'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        // Get current currencies
        $current = Currency::pluck('code')->toArray();

        // Prepare codes
        $codes = array();
        $currencies = MoneyCurrency::getCurrencies();
        foreach ($currencies as $key => $item) {
            // Don't show if already available
            if (in_array($key, $current)) {
                continue;
            }

            $codes[$key] = $key;
        }
        return view('accounting::currencies.create', compact('codes'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(CurrencyRequest $request)
    {
        // Force the rate to be 1 for default currency
        if ($request['default_currency']) {
            $request['rate'] = '1';
        }

        Currency::create($request->all());

        // Update default currency setting
        if ($request['default_currency']) {
            setting()->set('general.default_currency', $request['code']);
            setting()->save();
        }

        $message = trans('messages.success.added', ['type' => trans_choice('general.currencies', 1)]);

        Session()->flash('flash_message', $message);

        return redirect('accounting/currencies');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('accounting::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('accounting::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(CurrencyRequest $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
