<?php

namespace Modules\Accounting\Http\Controllers;

// use Illuminate\Http\Request;
use Modules\Accounting\Http\Requests\Item as Request;;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Accounting\Entities\Item;
use Modules\Accounting\Entities\Category;
use Modules\Accounting\Entities\Currency;
use Modules\Accounting\Entities\Tax;
use MediaUploader;

class ItemsController extends Controller
{
    protected $items;

    public function __construct()
    {
        $this->middleware('auth');
        $this->items = Item::all();

    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $items = Item::all();
        $categories = Category::all();
        return view('accounting::items.index', compact('items','categories'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $categories = Category::all()->pluck('name', 'id');
        $taxes = Tax::all()->pluck('title', 'id');
        return view('accounting::items.create', compact('categories','taxes'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $item = Item::create($request->input());

        // Upload picture
        if ($request->file('picture')) {
            $media = MediaUploader::fromSource($request->file('picture'))
                ->toDestination('public', 'img/Zana/items')
                ->upload();            

            $item->attachMedia($media, 'picture');
        }

        $message = trans('messages.success.added', ['type' => trans_choice('general.items', 1)]);

        Session()->flash('flash_message', $message);
        return redirect()->route('items.index');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('accounting::items.show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Item $item)
    {
        $categories = Category::all()->pluck('name', 'id');
        $taxes = Tax::all()->pluck('title', 'id');
        return view('accounting::items.edit', compact('item','categories','taxes'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Item $item, Request $request)
    {
        $item->update($request->input());

        // Upload picture
        if ($request->file('picture')) {
            $media = MediaUploader::fromSource($request->file('picture'))
                ->toDestination('public', 'img/Zana/items')
                ->upload();            

            $item->attachMedia($media, 'picture');
        }

        $message = trans('messages.success.updated', ['type' => trans_choice('general.items', 1)]);

        Session()->flash('flash_message', $message);
        return redirect()->route('items.index');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function autocomplete()
    {
        $type = request('type');
        $query = request('query');
        $currency_code = request('currency_code');

        if (empty($currency_code) || (strtolower($currency_code)  == 'null')) {
            $currency_code = setting('general.default_currency');
        }

        $currency = Currency::where('code', $currency_code)->first();

        $autocomplete = Item::autocomplete([
            'name' => $query,
            'sku' => $query,
        ]);

        if ($type == 'invoice') {
            $autocomplete->quantity();
        }

        $items = $autocomplete->get();

        if ($items) {
            foreach ($items as $item) {
                $tax = Tax::find($item->tax_id);

                $item_tax_price = 0;

                if (!empty($tax)) {
                    $item_tax_price = ($item->sale_price / 100) * $tax->rate;
                }

                $item->sale_price = $this->convertPrice($item->sale_price, $currency_code, $currency->rate);
                $item->purchase_price = $this->convertPrice($item->purchase_price, $currency_code, $currency->rate);

                switch ($type) {
                    case 'bill':
                        $total = $item->purchase_price + $item_tax_price;
                        break;
                    case 'invoice':
                    default:
                        $total = $item->sale_price + $item_tax_price;
                        break;
                }

                $item->total = money($total, $currency_code, true)->format();
            }
        }

        return response()->json($items);
    }

    public function totalItem()
    {
        $input_items = request('item');
        $currency_code = request('currency_code');
        $discount = request('discount');

        if (empty($currency_code)) {
            $currency_code = setting('general.default_currency');
        }

        $json = new \stdClass;

        $sub_total = 0;
        $tax_total = 0;

        $items = array();

        if ($input_items) {
            foreach ($input_items as $key => $item) {
                $price = (double) $item['price'];
                $quantity = (double) $item['quantity'];

                $item_tax_total= 0;
                $item_sub_total = ($price * $quantity);

                if (!empty($item['tax_id'])) {
                    $tax = Tax::find($item['tax_id']);

                    $item_tax_total = (($price * $quantity) / 100) * $tax->rate;
                }

                $sub_total += $item_sub_total;

                // Apply discount to tax
                if ($discount) {
                    $item_tax_total = $item_tax_total - ($item_tax_total * ($discount / 100));
                }

                $tax_total += $item_tax_total;

                $items[$key] = money($item_sub_total, $currency_code, true)->format();
            }
        }

        $json->items = $items;

        $json->sub_total = money($sub_total, $currency_code, true)->format();

        $json->discount_text= trans('invoices.add_discount');
        $json->discount_total = '';

        $json->tax_total = money($tax_total, $currency_code, true)->format();

        // Apply discount to total
        if ($discount) {
            $json->discount_text= trans('invoices.show_discount', ['discount' => $discount]);
            $json->discount_total = money($sub_total * ($discount / 100), $currency_code, true)->format();

            $sub_total = $sub_total - ($sub_total * ($discount / 100));
        }

        $grand_total = $sub_total + $tax_total;

        $json->grand_total = money($grand_total, $currency_code, true)->format();

        return response()->json($json);
    }

    protected function convertPrice($amount, $currency_code, $currency_rate, $format = false, $reverse = false)
    {
        $item = new Item();

        $item->amount = $amount;
        $item->currency_code = $currency_code;
        $item->currency_rate = $currency_rate;

        if ($reverse) {
            return $item->getReverseConvertedAmount($format);
        }

        return $item->getConvertedAmount($format);
    }
}
