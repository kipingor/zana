<?php

namespace Modules\Accounting\Http\Controllers\Banking;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Accounting\Http\Requests\Banking\TransferRequest;
use Modules\Accounting\Entities\Banking\Account;
use Modules\Accounting\Entities\Banking\Transfer;
use Modules\Accounting\Entities\Expense\Payment;
use Modules\Accounting\Entities\Income\Revenue;
use Modules\Accounting\Entities\Setting\Category;
use Modules\Accounting\Entities\Setting\Currency;

class TransfersController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $request = request();

        $items = Transfer::with(['payment', 'payment.account', 'revenue', 'revenue.account'])->collect('payment.paid_at');

        $accounts = collect(Account::enabled()->pluck('name', 'id'))
            ->prepend(trans('general.all_type', ['type' => trans_choice('general.accounts', 2)]), '');

        $transfers = array();

        foreach ($items as $item) {
            $payment = $item->payment;
            $revenue = $item->revenue;

            $transfers[] = (object)[
                'from_account' => $payment->account->name,
                'to_account' => $revenue->account->name,
                'amount' => $payment->amount,
                'currency_code' => $payment->currency_code,
                'paid_at' => $payment->paid_at,
            ];
        }

        $special_key = array(
            'payment.name' => 'from_account',
            'revenue.name' => 'to_account',
        );

        if (isset($request['sort']) && array_key_exists($request['sort'], $special_key)) {
            $sort_order = array();

            foreach ($transfers as $key => $value) {
                $sort = $request['sort'];

                if (array_key_exists($request['sort'], $special_key)) {
                    $sort = $special_key[$request['sort']];
                }

                $sort_order[$key] = $value->{$sort};
            }

            $sort_type = (isset($request['order']) && $request['order'] == 'asc') ? SORT_ASC : SORT_DESC;

            array_multisort($sort_order, $sort_type, $transfers);
        }
        
        return view('accounting::banking.tranfers.index', compact('transfers', 'items', 'accounts'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('accounting::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('accounting::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('accounting::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
