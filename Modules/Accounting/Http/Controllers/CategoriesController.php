<?php

namespace Modules\Accounting\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Accounting\Entities\Category;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $categories = Category::all();

        $transfer_id = Category::transfer();

        $types = collect([
            'expense' => trans_choice('general.expenses', 1),
            'income' => trans_choice('general.incomes', 1),
            'item' => trans_choice('general.items', 1),
            'other' => trans_choice('general.others', 1),
        ])->prepend(trans('general.all_type', ['type' => trans_choice('general.types', 2)]), '');
        return view('accounting::categories.index', compact('categories', 'types', 'transfer_id'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $types = [
            'expense' => trans_choice('general.expenses', 1),
            'income' => trans_choice('general.incomes', 1),
            'item' => trans_choice('general.items', 1),
            'other' => trans_choice('general.others', 1),
        ];
        return view('accounting::categories.create', compact('types'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        Category::create($request->all());

        $message = trans('messages.success.added', ['type' => trans_choice('general.categories', 1)]);

        Session()->flash('flash_message', $message);
        return redirect('accounting/categories');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return redirect('accounting/categories');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        $types = [
            'expense' => trans_choice('general.expenses', 1),
            'income' => trans_choice('general.incomes', 1),
            'item' => trans_choice('general.items', 1),
            'other' => trans_choice('general.others', 1),
        ];

        $type_disabled = (Category::where('type', $category->type)->count() == 1) ?: false;

        return view('accounting::categories.edit', compact('category', 'types', 'type_disabled'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Category $category, Request $request)
    {
        $relationships = $this->countRelationships($category, [
            'items' => 'items',
            'invoices' => 'invoices',
            'revenues' => 'revenues',
            'bills' => 'bills',
            'payments' => 'payments',
        ]);

        if (empty($relationships) || $request['enabled']) {
            $category->update($request->all());

            $message = trans('messages.success.updated', ['type' => trans_choice('general.categories', 1)]);

            Session()->flash('flash_message_success', $message);

            return redirect('settings/categories');
        } else {
            $message = trans('messages.warning.disabled', ['name' => $category->name, 'text' => implode(', ', $relationships)]);

            Session()->flash('flash_message_warning', $message);

            return redirect('accounting/categories/' . $category->id . '/edit');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Category $category)
    {
        // Can not delete the last category by type
        if (Category::where('type', $category->type)->count() == 1) {
            $message = trans('messages.error.last_category', ['type' => strtolower(trans_choice('general.' . $category->type . 's', 1))]);

            Session()->flash('flash_message_warning', $message);

            return redirect('settings/categories');
        }

        $relationships = $this->countRelationships($category, [
            'items' => 'items',
            'invoices' => 'invoices',
            'revenues' => 'revenues',
            'bills' => 'bills',
            'payments' => 'payments',
        ]);

        if (empty($relationships)) {
            $category->delete();

            $message = trans('messages.success.deleted', ['type' => trans_choice('general.categories', 1)]);

            Session()->flash('flash_message_success', $message);
        } else {
            $message = trans('messages.warning.deleted', ['name' => $category->name, 'text' => implode(', ', $relationships)]);

            Session()->flash('flash_message_warning', $message);
        }

        return redirect('accounting/categories');
    }

    public function category(Request $request)
    {
        $category = Category::create($request->all());

        return response()->json($category);
    }
}
