<?php

namespace Modules\Accounting\Http\Controllers\Expenses;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Accounting\Http\Requests\Expense\VendorRequest;
use Modules\Accounting\Entities\Bill;
use Modules\Accounting\Entities\Payment;
use Modules\Accounting\Entities\Vendor;
use Modules\Accounting\Entities\Currency;
use App\Traits\Uploads;
use App\Utilities\ImportFile;
use Carbon\Carbon;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class VendorsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $vendors = Vendor::all();
        return view('accounting::expenses.vendors.index', compact('vendors'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $currencies = Currency::all()->pluck('name', 'code');

        return view('accounting::expenses.vendors.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(VendorRequest $request)
    {
        if (empty($request['email'])) {
            $request['email'] = '';
        }

        $vendor = Vendor::create($request->all());

        // Upload logo
        if ($request->file('logo')) {
            $media = $this->getMedia($request->file('logo'), 'vendors');

            $vendor->attachMedia($media, 'logo');
        }

        $message = trans('messages.success.added', ['type' => trans_choice('general.vendors', 1)]);

        flash($message)->success();

        return redirect('accounting/expenses/vendors');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show(Vendor $vendor)
    {
        $amounts = [
            'paid' => 0,
            'open' => 0,
            'overdue' => 0,
        ];

        $counts = [
            'bills' => 0,
            'payments' => 0,
        ];

        // Handle bills
        $bills = Bill::with(['status', 'payments'])->where('vendor_id', $vendor->id)->get();

        $counts['bills'] = $bills->count();

        $bill_payments = [];

        $today = Date::today()->toDateString();

        foreach ($bills as $item) {
            $payments = 0;

            foreach ($item->payments as $payment) {
                $payment->category = $item->category;

                $bill_payments[] = $payment;

                $amount = $payment->getConvertedAmount();

                $amounts['paid'] += $amount;

                $payments += $amount;
            }

            if ($item->bill_status_code == 'paid') {
                continue;
            }

            // Check if it's open or overdue invoice
            if ($item->due_at > $today) {
                $amounts['open'] += $item->getConvertedAmount() - $payments;
            } else {
                $amounts['overdue'] += $item->getConvertedAmount() - $payments;
            }
        }

        // Handle payments
        $payments = Payment::with(['account', 'category'])->where('vendor_id', $vendor->id)->get();

        $counts['payments'] = $payments->count();

        // Prepare data
        $items = collect($payments)->each(function ($item) use (&$amounts) {
            $amounts['paid'] += $item->getConvertedAmount();
        });

        $limit = request('limit', setting('general.list_limit', '25'));
        $transactions = $this->paginate($items->merge($bill_payments)->sortByDesc('paid_at'), $limit);

        return view('accounting::expenses.vendors.show', compact('vendor', 'counts', 'amounts', 'transactions'));
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('accounting::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function currency()
    {
        $vendor_id = request('vendor_id');

        $vendor = Vendor::find($vendor_id);

        return response()->json($vendor);
    }

    public function vendor(VendorRequest $request)
    {
        if (empty($request['email'])) {
            $request['email'] = '';
        }

        $vendor = Vendor::create($request->all());

        return response()->json($vendor);
    }
}
