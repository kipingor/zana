<?php

namespace Modules\Accounting\Http\Controllers\Expenses;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Accounting\Events\BillCreated;
use Modules\Accounting\Events\BillUpdated;
use Modules\Accounting\Http\Requests\Expense\BillRequest;
use Modules\Accounting\Http\Requests\Expense\BillPaymentRequest;
use Modules\Accounting\Entities\Account;
use Modules\Accounting\Entities\BillStatus;
use Modules\Accounting\Entities\Vendor;
use Modules\Accounting\Entities\Bill;
use Modules\Accounting\Entities\BillItem;
use Modules\Accounting\Entities\BillTotal;
use Modules\Accounting\Entities\BillHistory;
use Modules\Accounting\Entities\BillPayment;
use Modules\Accounting\Entities\Item;
use Modules\Accounting\Entities\Category;
use Modules\Accounting\Entities\Currency;
use Modules\Accounting\Entities\Tax;
use Modules\Accounting\Entities\Media;
use Modules\Accounting\Traits\Currencies;
use Modules\Accounting\Traits\DateTime;
use Modules\Accounting\Traits\Uploads;
use Modules\Accounting\Utilities\ImportFile;
use Carbon\Carbon;
use File;
use Image;
use Storage;

class BillsController extends Controller
{
    use DateTime, Currencies, Uploads;

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    
    public function index()
    {
        
        $bills = Bill::all();

        $vendors = Vendor::all()->pluck('name', 'id')
            ->prepend(trans('general.all_type', ['type' => trans_choice('general.vendors', 2)]), '');

        $statuses = BillStatus::all()->pluck('name', 'code')
            ->prepend(trans('general.all_type', ['type' => trans_choice('general.statuses', 2)]), '');

        return view('accounting::expenses.bills.index', compact('bills', 'vendors', 'statuses'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $vendors = Vendor::all()->pluck('name', 'id');

        $currencies = Currency::all()->pluck('name', 'code');

        $items = Item::all()->pluck('name', 'id');

        $taxes = Tax::all()->pluck('title', 'id');

        $categories = Category::all()->where('type', 'expense')->pluck('name', 'id');

        return view('accounting::expenses.bills.create', compact('vendors', 'currencies', 'items', 'taxes', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(BillRequest $request)
    {
        // Get vendor object
        $vendor = Vendor::findOrFail($request['vendor_id']);

        $request['vendor_name'] = $vendor->name;
        $request['vendor_email'] = $vendor->email;
        $request['vendor_tax_number'] = $vendor->tax_number;
        $request['vendor_phone'] = $vendor->phone;
        $request['vendor_address'] = $vendor->address;

        // Get currency object
        $currency = Currency::where('code', $request['currency_code'])->first();

        $request['currency_code'] = $currency->code;
        $request['currency_rate'] = $currency->rate;

        $request['bill_status_code'] = 'draft';

        $request['amount'] = 0;

        $bill = Bill::create($request->input());

        // Upload attachment
        if ($request->file('attachment')) {
            $media = $this->getMedia($request->file('attachment'), 'bills');

            $bill->attachMedia($media, 'attachment');
        }

        $taxes = [];

        $tax_total = 0;
        $sub_total = 0;
        $discount_total = 0;
        $discount = $request['discount'];

        $bill_item = [];
        $bill_item['bill_id'] = $bill->id;

        if ($request['item']) {
            foreach ($request['item'] as $item) {
                unset($tax_object);
                $item_sku = '';

                if (!empty($item['item_id'])) {
                    $item_object = Item::find($item['item_id']);

                    $item['name'] = $item_object->name;
                    $item_sku = $item_object->sku;

                    // Increase stock (item bought)
                    $item_object->quantity += $item['quantity'];
                    $item_object->save();
                }

                $tax = $tax_id = 0;

                if (!empty($item['tax_id'])) {
                    $tax_object = Tax::find($item['tax_id']);

                    $tax_id = $item['tax_id'];

                    $tax = (((double) $item['price'] * (double) $item['quantity']) / 100) * $tax_object->rate;

                    // Apply discount to tax
                    if ($discount) {
                        $tax = $tax - ($tax * ($discount / 100));
                    }
                }

                $bill_item['item_id'] = $item['item_id'];
                $bill_item['name'] = str_limit($item['name'], 180, '');
                $bill_item['sku'] = $item_sku;
                $bill_item['quantity'] = (double) $item['quantity'];
                $bill_item['price'] = (double) $item['price'];
                $bill_item['tax'] = $tax;
                $bill_item['tax_id'] = $tax_id;
                $bill_item['total'] = (double) $item['price'] * (double) $item['quantity'];

                BillItem::create($bill_item);

                // Set taxes
                if (isset($tax_object)) {
                    if (array_key_exists($tax_object->id, $taxes)) {
                        $taxes[$tax_object->id]['amount'] += $tax;
                    } else {
                        $taxes[$tax_object->id] = [
                            'name' => $tax_object->name,
                            'amount' => $tax
                        ];
                    }
                }

                // Calculate totals
                $tax_total += $tax;
                $sub_total += $bill_item['total'];

                unset($tax_object);
            }
        }

        $s_total = $sub_total;

        // Apply discount to total
        if ($discount) {
            $s_discount = $s_total * ($discount / 100);
            $discount_total += $s_discount;
            $s_total = $s_total - $s_discount;
        }

        $request['amount'] = $s_total + $tax_total;

        $bill->update($request->input());

        // Add bill totals
        $this->addTotals($bill, $request, $taxes, $sub_total, $discount_total, $tax_total);

        // Add bill history
        BillHistory::create([
            'bill_id' => $bill->id,
            'status_code' => 'draft',
            'notify' => 0,
            'description' => trans('messages.success.added', ['type' => $bill->bill_number]),
        ]);

        // Recurring
        $bill->createRecurring();

        // Fire the event to make it extendible
        event(new BillCreated($bill));

        $message = trans('messages.success.added', ['type' => trans_choice('general.bills', 1)]);

        Session()->flash('flash_message_success', $message);

        return redirect('accounting/expenses/bills/' . $bill->id);
        // return redirect()->route('bills.show');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('accounting::expenses.bills.show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('accounting::expenses.bills.edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    protected function addTotals($bill, $request, $taxes, $sub_total, $discount_total, $tax_total)
    {
        $sort_order = 1;

        // Added bill sub total
        BillTotal::create([
            'company_id' => $request['company_id'],
            'bill_id' => $bill->id,
            'code' => 'sub_total',
            'name' => 'bills.sub_total',
            'amount' => $sub_total,
            'sort_order' => $sort_order,
        ]);

        $sort_order++;

        // Added bill discount
        if ($discount_total) {
            BillTotal::create([
                'company_id' => $request['company_id'],
                'bill_id' => $bill->id,
                'code' => 'discount',
                'name' => 'bills.discount',
                'amount' => $discount_total,
                'sort_order' => $sort_order,
            ]);

            // This is for total
            $sub_total = $sub_total - $discount_total;
        }

        $sort_order++;

        // Added bill taxes
        if ($taxes) {
            foreach ($taxes as $tax) {
                BillTotal::create([
                    'company_id' => $request['company_id'],
                    'bill_id' => $bill->id,
                    'code' => 'tax',
                    'name' => $tax['name'],
                    'amount' => $tax['amount'],
                    'sort_order' => $sort_order,
                ]);

                $sort_order++;
            }
        }

        // Added bill total
        BillTotal::create([
            'company_id' => $request['company_id'],
            'bill_id' => $bill->id,
            'code' => 'total',
            'name' => 'bills.total',
            'amount' => $sub_total + $tax_total,
            'sort_order' => $sort_order,
        ]);
    }
}
