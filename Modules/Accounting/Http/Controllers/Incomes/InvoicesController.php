<?php

namespace Modules\Accounting\Http\Controllers\Incomes;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Accounting\Events\InvoiceAction;
use Modules\Accounting\Events\InvoicePrinting;
use Modules\Accounting\Events\InvoiceUpdated;
use Modules\Accounting\Http\Requests\Income\StoreInvoiceRequest;
use Modules\Accounting\Http\Requests\Income\StorePaymentRequest;
use Modules\Accounting\Entities\Account;
use Modules\Accounting\Entities\Customer;
use Modules\Accounting\Entities\Invoice;
use Modules\Accounting\Entities\InvoiceHistory;
use Modules\Accounting\Entities\InvoiceItem;
use Modules\Accounting\Entities\InvoiceTotal;
use Modules\Accounting\Entities\InvoicePayment;
use Modules\Accounting\Entities\InvoiceStatus;
use Modules\Accounting\Entities\Item;
use Modules\Accounting\Entities\Category;
use Modules\Accounting\Entities\Currency;
use Modules\Accounting\Entities\Tax;
use Modules\Accounting\Entities\Media;
use Modules\Accounting\Notifications\InvoiceActionNotification;
use Modules\Accounting\Notifications\ItemActionNotification;
use Modules\Accounting\Traits\Currencies;
use Modules\Accounting\Traits\DateTime;
use Modules\Accounting\Traits\Incomes;
use Modules\Accounting\Traits\Uploads;
use Modules\Accounting\Utilities\ImportFile;
use Modules\Accounting\Utilities\Modules;
use Carbon\Carbon;
use File;
use Image;
use Storage;

class InvoicesController extends Controller
{
  use DateTime, Currencies, Incomes, Uploads;
  
  public function __construct() {
    $this->middleware('auth');
  }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
      $invoices = Invoice::latest('created_at')->get();
      $customers = Customer::all()->pluck('name', 'id');
      $status = InvoiceStatus::all()->pluck('name','code');
        return view('accounting::incomes.invoices.index', compact(
            'invoices',
            'customers',
            'status'
            ));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
      $customers = Customer::all()->pluck('name','id');
      $currencies = currency::all()->pluck('name','code');
      $items = Item::all()->pluck('name','id');
      $taxes = Tax::all()->pluck('title','id');
      $categories = Category::all()
          ->where('type','income')
          ->pluck('name','id');
      $number = $this->getNextInvoiceNumber();
        return view('accounting::incomes.invoices.create', compact('customers', 'currencies', 'items', 'taxes', 'categories', 'number'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function createUtility()
    {
      $customers = Customer::all()->pluck('name','id');
      $currencies = currency::all()->pluck('name','code');
      $items = Item::all()->pluck('name','id');
      $taxes = Tax::all()->pluck('title','id');
      $categories = Category::all()
          ->where('type','income')
          ->pluck('name','id');
      $number = $this->getNextInvoiceNumber();
        return view('accounting::incomes.invoices.createUtility', compact('customers', 'currencies', 'items', 'taxes', 'categories', 'number'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(StoreInvoiceRequest $request)
    {
      // Get customer object
        $customer = Customer::findOrFail($request['customer_id']);

        $request['customer_name'] = $customer->name;
        $request['customer_email'] = $customer->email;
        $request['customer_tax_number'] = $customer->vat;
        $request['customer_phone'] = $customer->primary_number;
        $request['customer_address'] = $customer->address;

        // Get currency object
        $currency = Currency::where('code', $request['currency_code'])->first();

        $request['currency_code'] = $currency->code;
        $request['currency_rate'] = $currency->rate;

        $request['invoice_status_code'] = 'draft';

        $request['amount'] = 0;

        $invoice = Invoice::create($request->input());

        // Upload attachment
        if ($request->file('attachment')) {
            $media = $this->getMedia($request->file('attachment'), 'invoices');

            $invoice->attachMedia($media, 'attachment');
        }

        $taxes = [];

        $tax_total = 0;
        $sub_total = 0;
        $discount_total = 0;
        $discount = $request['discount'];

        $invoice_item = [];
        $invoice_item['invoice_id'] = $invoice->id;

        if ($request['item']) {
            foreach ($request['item'] as $item) {
                $item_sku = '';

                if (!empty($item['item_id'])) {
                    $item_object = Item::find($item['item_id']);

                    $item['name'] = $item_object->name;
                    $item_sku = $item_object->sku;

                    // Decrease stock (item sold)
                    $item_object->quantity -= $item['quantity'];
                    $item_object->save();

                    // Notify users if out of stock
                    if ($item_object->quantity == 0) {
                        foreach ($item_object->users as $user) {
                            if (!$user->has('read.item.notifications')) {
                                continue;
                            }

                            $user->notify(new ItemActionNotification($item_object));
                        }
                    }
                }

                $tax = $tax_id = 0;

                if (!empty($item['tax_id'])) {
                    $tax_object = Tax::find($item['tax_id']);

                    $tax_id = $item['tax_id'];

                    $tax = (((double) $item['price'] * (double) $item['quantity']) / 100) * $tax_object->rate;

                    // Apply discount to tax
                    if ($discount) {
                        $tax = $tax - ($tax * ($discount / 100));
                    }
                }

                $invoice_item['item_id'] = $item['item_id'];
                $invoice_item['name'] = str_limit($item['name'], 180, '');
                $invoice_item['sku'] = $item_sku;
                $invoice_item['quantity'] = (double) $item['quantity'];
                $invoice_item['price'] = (double) $item['price'];
                $invoice_item['tax'] = $tax;
                $invoice_item['tax_id'] = $tax_id;
                $invoice_item['total'] = (double) $item['price'] * (double) $item['quantity'];

                InvoiceItem::create($invoice_item);

                // Set taxes
                if (isset($tax_object)) {
                    if (array_key_exists($tax_object->id, $taxes)) {
                        $taxes[$tax_object->id]['amount'] += $tax;
                    } else {
                        $taxes[$tax_object->id] = [
                            'name' => $tax_object->name,
                            'amount' => $tax
                        ];
                    }
                }

                // Calculate totals
                $tax_total += $tax;
                $sub_total += $invoice_item['total'];

                unset($tax_object);
            }
        }

        $s_total = $sub_total;

        // Apply discount to total
        if ($discount) {
            $s_discount = $s_total * ($discount / 100);
            $discount_total += $s_discount;
            $s_total = $s_total - $s_discount;
        }

        $request['amount'] = $s_total + $tax_total;

        $invoice->update($request->input());

        // Add invoice totals
        $this->addTotals($invoice, $request, $taxes, $sub_total, $discount_total, $tax_total);

        // Add invoice history
        InvoiceHistory::create([
            'invoice_id' => $invoice->id,
            'status_code' => 'draft',
            'notify' => 0,
            'description' => trans('messages.success.added', ['type' => $invoice->invoice_number]),
        ]);

        // Update next invoice number
        $this->increaseNextInvoiceNumber();

        // Recurring
        $invoice->createRecurring();

        // Fire the event to make it extendible
        event(new InvoiceAction($invoice, 'New Invoice Created'));

        $message = trans('messages.success.added', ['type' => trans_choice('general.invoices', 1)]);

        Session()->flash('flash_message_success', $message);

        return redirect('accounting/incomes/invoices/' . $invoice->id);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show(Invoice $invoice)
    {
      $paid = 0;

      foreach ($invoice->payments as $item) {
          $item->default_currency_code = $invoice->currency_code;

          $paid += $item->getDynamicConvertedAmount();
      }

      $invoice->paid = $paid;

      $accounts = Account::all()->pluck('name', 'id');

      $currencies = Currency::all()->pluck('name', 'code')->toArray();

      $account_currency_code = Account::where('id', setting('general.default_account'))->pluck('currency_code')->first();

      $customers = Customer::all()->pluck('name', 'id');

      $categories = Category::all()
      ->where('type','income')
      ->pluck('name', 'id');
      $payment_methods = ['1'=>'Mpesa','2'=>'Cash', '3'=>'Other'];
      // $payment_methods = Modules::getPaymentMethods();

        return view('accounting::incomes.invoices.show', compact('invoice', 'accounts', 'currencies', 'account_currency_code', 'customers', 'categories', 'payment_methods'));
    }

    /**
     * Water the specified resource.
     * @return Response
     */
    public function water(Invoice $invoice)
    {
      $paid = 0;

      foreach ($invoice->payments as $item) {
          $item->default_currency_code = $invoice->currency_code;

          $paid += $item->getDynamicConvertedAmount();
      }

      $invoice->paid = $paid;

      $accounts = Account::all()->pluck('name', 'id');

      $currencies = Currency::all()->pluck('name', 'code')->toArray();

      $account_currency_code = Account::where('id', setting('general.default_account'))->pluck('currency_code')->first();

      $customers = Customer::all()->pluck('name', 'id');

      $categories = Category::all()
      ->where('type','income')
      ->pluck('name', 'id');

      $payment_methods = ['1'=>'Mpesa','2'=>'Cash', '3'=>'Other'];//Modules::getPaymentMethods();

        return view('accounting::incomes.water.show', compact('invoice', 'accounts', 'currencies', 'account_currency_code', 'customers', 'categories', 'payment_methods'));
    }

    /**
     * Duplicate the specified resource.
     *
     * @param  Invoice  $invoice
     *
     * @return Response
     */
    public function duplicate(Invoice $invoice)
    {
        $clone = $invoice->duplicate();

        // Add invoice history
        InvoiceHistory::create([
            'company_id' => session('company_id'),
            'invoice_id' => $clone->id,
            'status_code' => 'draft',
            'notify' => 0,
            'description' => trans('messages.success.added', ['type' => $clone->invoice_number]),
        ]);

        // Update next invoice number
        $this->increaseNextInvoiceNumber();

        $message = trans('messages.success.duplicated', ['type' => trans_choice('general.invoices', 1)]);

        flash($message)->success();

        return redirect('accounting/incomes/invoices/' . $clone->id . '/edit');
    }

    /**
     * Import the specified resource.
     *
     * @param  ImportFile  $import
     *
     * @return Response
     */
    public function import(ImportFile $import)
    {
        $rows = $import->all();

        foreach ($rows as $row) {
            $data = $row->toArray();
            $data['company_id'] = session('company_id');

            Invoice::create($data);
        }

        $message = trans('messages.success.imported', ['type' => trans_choice('general.invoices', 2)]);

        flash($message)->success();

        return redirect('accounting/incomes/invoices');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Invoice $invoice)
    {
      $customers = Customer::all()->pluck('name', 'id');

      $currencies = Currency::all()->pluck('name', 'code');

      $items = Item::all()->pluck('name', 'id');

      $taxes = Tax::all()->pluck('title', 'id');

      $categories = Category::all()
          ->where('type','income')
          ->pluck('name','id');

      return view('accounting::incomes.invoices.edit', compact('invoice', 'customers', 'currencies', 'items', 'taxes', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
    
    /**
     * Mark the invoice as sent.
     *
     * @param  Invoice $invoice
     *
     * @return Response
     */
    public function markSent(Invoice $invoice)
    {
        $invoice->invoice_status_code = 'sent';

        $invoice->save();

        // Add invoice history
        InvoiceHistory::create([
            'invoice_id' => $invoice->id,
            'status_code' => 'sent',
            'notify' => 0,
            'description' => trans('invoices.mark_sent'),
        ]);
        
        Session()->flash('flash_message_success', trans('invoices.messages.marked_sent'));

        return redirect()->back();
    }

    /**
     * Download the PDF file of invoice.
     *
     * @param  Invoice $invoice
     *
     * @return Response
     */
    public function emailInvoice(Invoice $invoice)
    {
        if (empty($invoice->customer_email)) {
          Session()->flash('flash_message_warning', trans('invoices.messages.email_required'));
            return redirect()->back();
        }

        $invoice = $this->prepareInvoice($invoice);

        $html = view($invoice->template_path, compact('invoice'))->render();

        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($html);

        $file = storage_path('app/temp/invoice_'.time().'.pdf');

        $invoice->pdf_path = $file;

        // Save the PDF file into temp folder
        $pdf->save($file);

        // Notify the customer
        $invoice->customer->notify(new InvoiceActionNotification($invoice));

        // Delete temp file
        File::delete($file);

        unset($invoice->paid);
        unset($invoice->template_path);
        unset($invoice->pdf_path);

        // Mark invoice as sent
        if ($invoice->invoice_status_code != 'partial') {
            $invoice->invoice_status_code = 'sent';

            $invoice->save();
        }

        // Add invoice history
        InvoiceHistory::create([
            'invoice_id' => $invoice->id,
            'status_code' => 'sent',
            'notify' => 1,
            'description' => trans('invoices.send_mail'),
        ]);
        
        Session()->flash('flash_message_success', trans('invoices.messages.email_sent'));

        return redirect()->back();
    }

    /**
     * Print the invoice.
     *
     * @param  Invoice $invoice
     *
     * @return Response
     */
    public function printInvoice(Invoice $invoice)
    {
        $invoice = $this->prepareInvoice($invoice);

        return view($invoice->template_path, compact('invoice'));
    }

    /**
     * Download the PDF file of invoice.
     *
     * @param  Invoice $invoice
     *
     * @return Response
     */
    public function pdfInvoice(Invoice $invoice)
    {
        $invoice = $this->prepareInvoice($invoice);

        $html = view($invoice->template_path, compact('invoice'))->render();

        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($html);

        //$pdf->setPaper('A4', 'portrait');

        $file_name = 'invoice_'.time().'.pdf';

        return $pdf->download($file_name);
    }

    /**
     * Mark the invoice as paid.
     *
     * @param  Invoice $invoice
     *
     * @return Response
     */
    public function markPaid(Invoice $invoice)
    {
        $paid = 0;

        foreach ($invoice->payments as $item) {
            $item->default_currency_code = $invoice->currency_code;

            $paid += $item->getDynamicConvertedAmount();
        }

        $amount = $invoice->amount - $paid;

        if (!empty($amount)) {
            $request = new StorePaymentRequest();

            $request['invoice_id'] = $invoice->id;
            $request['account_id'] = setting('general.default_account');
            $request['payment_method'] = setting('general.default_payment_method', 'offlinepayment.cash.1');
            $request['currency_code'] = $invoice->currency_code;
            $request['amount'] = $amount;
            $request['paid_at'] = Carbon::now();
            $request['_token'] = csrf_token();

            $this->payment($request);
        } else {
            $invoice->invoice_status_code = 'paid';
            $invoice->save();
        }

        return redirect()->back();
    }

    /**
     * Add payment to the invoice.
     *
     * @param  PaymentRequest  $request
     *
     * @return Response
     */
    public function payment(StorePaymentRequest $request)
    {
        // Get currency object
        $currency = Currency::where('code', $request['currency_code'])->first();

        $request['currency_code'] = $currency->code;
        $request['currency_rate'] = $currency->rate;

        $invoice = Invoice::find($request['invoice_id']);

        $total_amount = $invoice->amount;

        $amount = (double) $request['amount'];

        if ($request['currency_code'] != $invoice->currency_code) {
            $request_invoice = new Invoice();

            $request_invoice->amount = (float) $request['amount'];
            $request_invoice->currency_code = $currency->code;
            $request_invoice->currency_rate = $currency->rate;

            $amount = $request_invoice->getConvertedAmount();
        }

        if ($invoice->payments()->count()) {
            $total_amount -= $invoice->payments()->paid();
        }

        if ($amount > $total_amount) {
            $message = trans('messages.error.over_payment');

            return response()->json([
                'success' => false,
                'error' => true,
                'message' => $message,
            ]);
        } elseif ($amount == $total_amount) {
            $invoice->invoice_status_code = 'paid';
        } else {
            $invoice->invoice_status_code = 'partial';
        }

        $invoice->save();

        $invoice_payment = InvoicePayment::create($request->input());

        // Upload attachment
        if ($request->file('attachment')) {
            $media = $this->getMedia($request->file('attachment'), 'invoices');

            $invoice_payment->attachMedia($media, 'attachment');
        }

        $request['status_code'] = $invoice->invoice_status_code;
        $request['notify'] = 0;

        $desc_amount = money((float) $request['amount'], (string) $request['currency_code'], true)->format();

        $request['description'] = $desc_amount . ' ' . trans_choice('general.payments', 1);

        InvoiceHistory::create($request->input());

        $message = trans('messages.success.added', ['type' => trans_choice('general.payments', 1)]);

        return response()->json([
            'success' => true,
            'error' => false,
            'message' => $message,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  InvoicePayment  $payment
     *
     * @return Response
     */
    public function paymentDestroy(InvoicePayment $payment)
    {
        $invoice = Invoice::find($payment->invoice_id);

        if ($invoice->payments()->count() > 1) {
            $invoice->invoice_status_code = 'partial';
        } else {
            $invoice->invoice_status_code = 'sent';
        }

        $invoice->save();

        $desc_amount = money((float) $payment->amount, (string) $payment->currency_code, true)->format();

        $description = $desc_amount . ' ' . trans_choice('general.payments', 1);

        // Add invoice history
        InvoiceHistory::create([
            'invoice_id' => $invoice->id,
            'status_code' => $invoice->invoice_status_code,
            'notify' => 0,
            'description' => trans('messages.success.deleted', ['type' => $description]),
        ]);

        $payment->delete();

        $message = trans('messages.success.deleted', ['type' => trans_choice('general.invoices', 1)]);

        Session()->flash('flash_message_success', $message);

        return redirect()->back();
    }

    protected function prepareInvoice(Invoice $invoice)
    {
        $paid = 0;

        foreach ($invoice->payments as $item) {
            $item->default_currency_code = $invoice->currency_code;

            $paid += $item->getDynamicConvertedAmount();
        }

        $invoice->paid = $paid;

        $invoice->template_path = 'accounting::incomes.invoices.invoice';

        event(new InvoicePrinting($invoice));

        return $invoice;
    }

    protected function addTotals($invoice, $request, $taxes, $sub_total, $discount_total, $tax_total)
    {
        $sort_order = 1;

        // Added invoice sub total
        InvoiceTotal::create([
            'invoice_id' => $invoice->id,
            'code' => 'sub_total',
            'name' => 'invoices.sub_total',
            'amount' => $sub_total,
            'sort_order' => $sort_order,
        ]);

        $sort_order++;

        // Added invoice discount
        if ($discount_total) {
            InvoiceTotal::create([
                'invoice_id' => $invoice->id,
                'code' => 'discount',
                'name' => 'invoices.discount',
                'amount' => $discount_total,
                'sort_order' => $sort_order,
            ]);

            // This is for total
            $sub_total = $sub_total - $discount_total;
        }

        $sort_order++;

        // Added invoice taxes
        if ($taxes) {
            foreach ($taxes as $tax) {
                InvoiceTotal::create([
                    'invoice_id' => $invoice->id,
                    'code' => 'tax',
                    'name' => $tax['name'],
                    'amount' => $tax['amount'],
                    'sort_order' => $sort_order,
                ]);

                $sort_order++;
            }
        }

        // Added invoice total
        InvoiceTotal::create([
            'invoice_id' => $invoice->id,
            'code' => 'total',
            'name' => 'invoices.total',
            'amount' => $sub_total + $tax_total,
            'sort_order' => $sort_order,
        ]);
    }
}
