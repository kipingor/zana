<?php

namespace Modules\Accounting\Http\Controllers\Incomes;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Accounting\Http\Requests\Income\StoreRevenueRequest;
use Modules\Accounting\Entities\Account;
use Modules\Accounting\Entities\Customer;
use Modules\Accounting\Entities\Revenue;
use Modules\Accounting\Entities\Category;
use Modules\Accounting\Entities\Currency;
use Modules\Accounting\Traits\Currencies;
use Modules\Accounting\Traits\DateTime;
use Modules\Accounting\Traits\Uploads;
use Modules\Accounting\Utilities\ImportFile;
// use Modules\Accounting\Utilities\Modules;

class RevenuesController extends Controller
{
     use DateTime, Currencies, Uploads;
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        // $revenues = Revenue::all();
        $revenues = Revenue::orderBy('paid_at', 'desc')->get();

        $customers = collect(Customer::all()->pluck('name', 'id'))
            ->prepend(trans('general.all_type', ['type' => trans_choice('general.customers', 2)]), '');

        $categories = collect(Category::all()->pluck('name', 'id'))
            ->prepend(trans('general.all_type', ['type' => trans_choice('general.categories', 2)]), '');

        $accounts = collect(Account::all()->pluck('name', 'id'))
            ->prepend(trans('general.all_type', ['type' => trans_choice('general.accounts', 2)]), '');

        $transfer_cat_id = Category::transfer();

        return view('accounting::incomes.revenues.index', compact('revenues', 'customers', 'categories', 'accounts', 'transfer_cat_id'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $accounts = Account::all()->pluck('name', 'id');

        $currencies = Currency::all()->pluck('name', 'code')->toArray();

        $account_currency_code = Account::where('id', setting('general.default_account'))->pluck('currency_code')->first();

        $customers = Customer::all()->pluck('name', 'id');

        $categories = Category::all()->where('type','income')->pluck('name', 'id');

        $payment_methods = ['1'=>'Mpesa','2'=>'Cash', '3'=>'Other'];//Modules::getPaymentMethods();

        return view('accounting::incomes.revenues.create', compact('accounts', 'currencies', 'account_currency_code', 'customers', 'categories','payment_methods'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(StoreRevenueRequest $request)
    {
        // Get currency object
        $currency = Currency::where('code', $request['currency_code'])->first();

        $request['currency_code'] = $currency->code;
        $request['currency_rate'] = $currency->rate;

        $revenue = Revenue::create($request->input());

        // Upload attachment
        if ($request->file('attachment')) {
            $media = $this->getMedia($request->file('attachment'), 'revenues');

            $revenue->attachMedia($media, 'attachment');
        }

        // Recurring
        $revenue->createRecurring();

        $message = trans('messages.success.added', ['type' => trans_choice('general.revenues', 1)]);

        Session()->flash('flash_message_success', $message);

        return redirect('/accounting/incomes/revenues');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return redirect('/accounting/incomes/revenues');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('accounting::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
