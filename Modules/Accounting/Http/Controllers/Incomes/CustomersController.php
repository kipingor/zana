<?php

namespace Modules\Accounting\Http\Controllers\Incomes;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Accounting\Http\Requests\Income\CustomerRequest;
use Zana\User;
use Modules\Accounting\Entities\Customer;
use Modules\Accounting\Entities\Invoice;
use Modules\Accounting\Entities\WaterBillItem;
use Modules\Accounting\Entities\Currency;
use Modules\Accounting\Utilities\ImportFile;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class CustomersController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
      $customers = Customer::all();
        return view('accounting::incomes.customers.index', compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
      $currencies = Currency::all()->pluck('name','code');
        return view('accounting::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        // return view('accounting::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('accounting::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function currency(Customer $customer)
    {
        return view('crm::clients.index.php');
    }
    public function reading($customer)    
    {
        $invoice = Invoice::latest()->where('customer_id', $customer)->first();
        $waterBill = WaterBillItem::latest()->where('invoice_id', $invoice->id)->first();
        return $waterBill->current_reading;
    }
}
