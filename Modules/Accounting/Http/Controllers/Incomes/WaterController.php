<?php

namespace Modules\Accounting\Http\Controllers\Incomes;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Accounting\Events\InvoiceAction;
use Modules\Accounting\Events\InvoicePrinting;
use Modules\Accounting\Events\InvoiceUpdated;
use Modules\Accounting\Http\Requests\Income\StoreWaterRequest;
use Modules\Accounting\Http\Requests\Income\InvoicePayment as PaymentRequest;
use Modules\Accounting\Entities\Account;
use Modules\Accounting\Entities\Customer;
use Modules\Accounting\Entities\Invoice;
use Modules\Accounting\Entities\InvoiceHistory;
use Modules\Accounting\Entities\WaterBillItem;
use Modules\Accounting\Entities\InvoiceTotal;
use Modules\Accounting\Entities\InvoicePayment;
use Modules\Accounting\Entities\InvoiceStatus;
use Modules\Accounting\Entities\Item;
use Modules\Accounting\Entities\Category;
use Modules\Accounting\Entities\Currency;
use Modules\Accounting\Entities\Tax;
use Modules\Accounting\Entities\Media;
use Modules\Accounting\Notifications\InvoiceActionNotification;
use Modules\Accounting\Notifications\ItemActionNotification;
use Modules\Accounting\Traits\Currencies;
use Modules\Accounting\Traits\DateTime;
use Modules\Accounting\Traits\Incomes;
use Modules\Accounting\Traits\Uploads;
use Modules\Accounting\Utilities\ImportFile;
use Modules\Accounting\Utilities\Modules;
use Carbon\Carbon;
use File;
use Image;
use Storage;

class WaterController extends Controller
{
    use DateTime, Currencies, Incomes, Uploads;
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $invoices = Invoice::latest('created_at')->get();
      $customers = Customer::all()->pluck('name', 'id');
      $status = InvoiceStatus::all()->pluck('name','code');
        return view('accounting::incomes.invoices.index', compact(
            'invoices',
            'customers',
            'status'
            ));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $customers = Customer::all()->pluck('name','id');
      $currencies = currency::all()->pluck('name','code');
      $items = Item::all()->pluck('name','id');
      $taxes = Tax::all()->pluck('title','id');
      $categories = Category::all()
          ->where('type','income')
          ->pluck('name','id');
      $number = $this->getNextInvoiceNumber();
        return view('accounting::incomes.invoices.createUtility', compact('customers', 'currencies', 'items', 'taxes', 'categories', 'number'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(StoreWaterRequest $request)
    {
        // Get customer object
        $customer = Customer::findOrFail($request['customer_id']);

        $request['customer_name'] = $customer->name;
        $request['customer_email'] = $customer->email;
        $request['customer_tax_number'] = $customer->vat;
        $request['customer_phone'] = $customer->primary_number;
        $request['customer_address'] = $customer->address;

        // Get currency object
        $currency = Currency::where('code', $request['currency_code'])->first();

        $request['currency_code'] = $currency->code;
        $request['currency_rate'] = $currency->rate;

        $request['invoice_status_code'] = 'draft';

        $request['amount'] = 0;

        $invoice = Invoice::create($request->input());

        // Upload attachment
        if ($request->file('attachment')) {
            $media = $this->getMedia($request->file('attachment'), 'invoices');

            $invoice->attachMedia($media, 'attachment');
        }

        $taxes = [];

        $tax_total = 0;
        $sub_total = 0;
        $discount_total = 0;
        $discount = $request['discount'];

        $invoice_item = [];
        $invoice_item['invoice_id'] = $invoice->id;

        if ($request['item']) {
            foreach ($request['item'] as $item) {
                $item_sku = '';

                if (!empty($item['item_id'])) {
                    $item_object = Item::find($item['item_id']);

                    $item_sku = $item_object->sku;

                    // Decrease stock (item sold)
                    $item_object->quantity -= $item['quantity'];
                    $item_object->save();

                    // Notify users if out of stock
                    if ($item_object->quantity == 0) {
                        foreach ($item_object->users as $user) {
                            if (!$user->has('read.item.notifications')) {
                                continue;
                            }

                            $user->notify(new ItemActionNotification($item_object));
                        }
                    }
                }

                $tax = $tax_id = 0;

                if (!empty($item['tax_id'])) {
                    $tax_object = Tax::find($item['tax_id']);

                    $tax_id = $item['tax_id'];

                    $tax = (((double) $item['price'] * (double) $item['quantity']) / 100) * $tax_object->rate;

                    // Apply discount to tax
                    if ($discount) {
                        $tax = $tax - ($tax * ($discount / 100));
                    }
                }

                $invoice_item['item_id'] = 1;
                $invoice_item['previous_reading'] = $request['previous'];
                $invoice_item['current_reading'] = $request['current'];
                $invoice_item['sku'] = 'SS-001PW001';
                $invoice_item['quantity'] = (double) $request['quantity'];
                $invoice_item['price'] = (double) $request['price'];
                $invoice_item['tax'] = $tax;
                $invoice_item['tax_id'] = $tax_id;
                $invoice_item['total'] = (double) $request['price'] * (double) $request['quantity'];

                WaterBillItem::create($invoice_item);

                // Set taxes
                if (isset($tax_object)) {
                    if (array_key_exists($tax_object->id, $taxes)) {
                        $taxes[$tax_object->id]['amount'] += $tax;
                    } else {
                        $taxes[$tax_object->id] = [
                            'name' => $tax_object->name,
                            'amount' => $tax
                        ];
                    }
                }

                // Calculate totals
                $tax_total += $tax;
                $sub_total += $invoice_item['total'];

                unset($tax_object);
            }
        }

        $s_total = $sub_total;

        // Apply discount to total
        if ($discount) {
            $s_discount = $s_total * ($discount / 100);
            $discount_total += $s_discount;
            $s_total = $s_total - $s_discount;
        }

        $request['amount'] = $s_total + $tax_total;

        $invoice->update($request->input());

        // Add invoice totals
        $this->addTotals($invoice, $request, $taxes, $sub_total, $discount_total, $tax_total);

        // Add invoice history
        InvoiceHistory::create([
            'invoice_id' => $invoice->id,
            'status_code' => 'draft',
            'notify' => 0,
            'description' => trans('messages.success.added', ['type' => $invoice->invoice_number]),
        ]);

        // Update next invoice number
        $this->increaseNextInvoiceNumber();

        // Recurring
        $invoice->createRecurring();

        // Fire the event to make it extendible
        event(new InvoiceAction($invoice, 'New Invoice Created'));

        $message = trans('messages.success.added', ['type' => trans_choice('general.invoices', 1)]);

        Session()->flash('flash_message_success', $message);

        return redirect('accounting/incomes/invoices/water/' . $invoice->id);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show(Invoice $invoice)
    {
        $paid = 0;

      foreach ($invoice->payments as $item) {
          $item->default_currency_code = $invoice->currency_code;

          $paid += $item->getDynamicConvertedAmount();
      }

      $invoice->paid = $paid;

      $accounts = Account::all()->pluck('name', 'id');

      $currencies = Currency::all()->pluck('name', 'code')->toArray();

      $account_currency_code = Account::where('id', setting('general.default_account'))->pluck('currency_code')->first();

      $customers = Customer::all()->pluck('name', 'id');

      $categories = Category::all()
      ->where('type','income')
      ->pluck('name', 'id');

      // $payment_methods = Modules::getPaymentMethods();

        return view('accounting::incomes.water.show', compact('invoice', 'accounts', 'currencies', 'account_currency_code', 'customers', 'categories', 'payment_methods'));
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('accounting::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
    protected function addTotals($invoice, $request, $taxes, $sub_total, $discount_total, $tax_total)
    {
        $sort_order = 1;

        // Added invoice sub total
        InvoiceTotal::create([
            'invoice_id' => $invoice->id,
            'code' => 'sub_total',
            'name' => 'invoices.sub_total',
            'amount' => $sub_total,
            'sort_order' => $sort_order,
        ]);

        $sort_order++;

        // Added invoice discount
        if ($discount_total) {
            InvoiceTotal::create([
                'invoice_id' => $invoice->id,
                'code' => 'discount',
                'name' => 'invoices.discount',
                'amount' => $discount_total,
                'sort_order' => $sort_order,
            ]);

            // This is for total
            $sub_total = $sub_total - $discount_total;
        }

        $sort_order++;

        // Added invoice taxes
        if ($taxes) {
            foreach ($taxes as $tax) {
                InvoiceTotal::create([
                    'invoice_id' => $invoice->id,
                    'code' => 'tax',
                    'name' => $tax['name'],
                    'amount' => $tax['amount'],
                    'sort_order' => $sort_order,
                ]);

                $sort_order++;
            }
        }

        // Added invoice total
        InvoiceTotal::create([
            'invoice_id' => $invoice->id,
            'code' => 'total',
            'name' => 'invoices.total',
            'amount' => $sub_total + $tax_total,
            'sort_order' => $sort_order,
        ]);
    }
}
