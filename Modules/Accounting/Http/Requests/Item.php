<?php

namespace Modules\Accounting\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Item extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // Check if store or update
        if ($this->getMethod() == 'PATCH') {
            $id = $this->item->getAttribute('id');
        } else {
            $id = null;
        }

        

        return [
            'name' => 'required|string',
            'sku' => 'required|string|unique:items,NULL,' . $id . ',id,deleted_at,NULL',
            'sale_price' => 'required',
            'purchase_price' => 'required',
            'quantity' => 'required|integer',            
            'enabled' => 'required',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
