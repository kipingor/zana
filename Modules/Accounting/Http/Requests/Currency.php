<?php

namespace Modules\Accounting\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Currency extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // Check if store or update
        if ($this->getMethod() == 'PATCH') {
            $id = $this->currency->getAttribute('id');
        } else {
            $id = null;
        }


        return [
            'name' => 'required|string',
            'code' => 'required|string|unique:currencies,NULL,' . $id . ',id,deleted_at,NULL',
            'rate' => 'required',
            'enabled' => 'boolean',
            'default_currency' => 'boolean',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
