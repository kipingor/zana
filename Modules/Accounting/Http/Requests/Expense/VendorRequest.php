<?php

namespace Modules\Accounting\Http\Requests\Expense;

use Illuminate\Foundation\Http\FormRequest;

class VendorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $email = '';

        
        // Check if store or update
        if ($this->getMethod() == 'PATCH') {
            $id = $this->vendor->getAttribute('id');
        } else {
            $id = null;
        }

        if (!empty($this->request->get('email'))) {
            $email = 'email|unique:vendors,NULL,' . $id . ',id,deleted_at,NULL';
        }

        return [
            'name' => 'required|string',
            'email' => $email,
            'currency_code' => 'required|string',
        ];
    }
}
