<?php

namespace Modules\Accounting\Http\Requests\Income;

use Illuminate\Foundation\Http\FormRequest;

class StoreWaterRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // Check if store or update
        if ($this->getMethod() == 'PATCH') {
            $id = $this->invoice->getAttribute('id');
        } else {
            $id = null;
        }


        return [
            'invoice_number' => 'required|string|unique:invoices,NULL,' . $id . ',id,deleted_at,NULL',
            'invoiced_at' => 'required|date',
            'due_at' => 'required|date',
            'currency_code' => 'required|string',
            'category_id' => 'required|integer',
            'previous' => 'required|integer',
            'current' => 'required|integer',
            'attachment' => 'mimes:' . setting('general.file_types') . '|between:0,' . setting('general.file_size') * 1024,
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->hasPermission('create.accounting.invoices');
    }
}
