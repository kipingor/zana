<?php

Route::group(['middleware' => 'web', 'prefix' => 'accounting', 'namespace' => 'Modules\Accounting\Http\Controllers'], function()
{
    Route::get('/', 'AccountingController@index');

    /**
     * Items
     */
    Route::group(['prefix' => 'items'], function () {
        Route::get('/', 'ItemsController@index')->name('items.index');
        Route::get('autocomplete', 'ItemsController@autocomplete');
        Route::get('/{item}/duplicate', 'ItemsController@duplicate');
        Route::post('totalItem', 'ItemsController@totalItem');
        Route::post('import', 'ItemsController@import');
        });
    Route::resource('items', 'ItemsController');

    /**
     * Categories
     */
    Route::group(['prefix' => 'categories'], function () {
    	Route::get('/', 'CategoriesController@index')->name('categories.index');
        Route::post('category', 'CategoriesController@category');
        });        
    Route::resource('categories', 'CategoriesController');

    /**
     * Currencies
     */
    Route::group(['prefix' => 'currencies'], function () {
        Route::get('/', 'CurrenciesController@index')->name('currencies.index');
    });
    Route::resource('currencies', 'CurrenciesController');

    /**
     * Expenses
     */
    Route::group(['prefix' => 'expenses'], function () {

        /**
         * Bills
         * These fall under Expenses and not income
         * This are the bills paid out
         * Use this package to log all recieted and not receited expenses
         */
        Route::group(['prefix' => 'bills'], function () {
            Route::get('/', 'Expenses\BillsController@index')->name('bills.index');
            Route::get('/{bill}/received', 'Expenses\BillsController@markReceived');
            Route::get('/{bill}/print', 'Expenses\BillsController@printBill');
            Route::get('/{bill}/pdf', 'Expenses\BillsController@pdfBill');
            Route::get('/{bill}/duplicate', 'Expenses\BillsController@duplicate');
            Route::post('payment', 'Expenses\BillsController@payment');
            Route::delete('payment/{payment}', 'Expenses\Bills@paymentDestroy');
            Route::post('import', 'Expenses\BillsController@import');
        });
        Route::resource('bills', 'Expenses\BillsController');

        /**
         * Payments
         */    
        Route::group(['prefix' => 'payments'], function () {
            Route::get('/{payment}/duplicate', 'Expenses\PaymentsController@duplicate');
            Route::post('/import', 'Expenses\PaymentsController@import');
        });
        Route::resource('payments', 'Expenses\PaymentsController');

        /**
         * Vendors
         * These are companies or individuals that have Provide goods and or service.
         */    
        Route::group(['prefix' => 'vendors'], function () {
            Route::get('currency', 'Expenses\VendorsController@currency');
            Route::get('/{vendor}/duplicate', 'Expenses\VendorsController@duplicate');
            Route::post('vendor', 'Expenses\VendorsController@vendor');
            Route::post('import', 'Expenses\VendorsController@import');
        });        
        Route::resource('vendors', 'Expenses\VendorsController');        
    });
    //End of Expenses
    
    /**
     * Expenses
     */
    Route::group(['prefix' => 'incomes'], function () {
      /**
       * invoices
       */
      Route::group(['prefix' => 'invoices'], function () {

        Route::get('/{invoice}/sent', 'Incomes\InvoicesController@markSent');
        Route::get('/{invoice}/email', 'Incomes\InvoicesController@emailInvoice');
        Route::get('/{invoice}/pay', 'Incomes\InvoicesController@markPaid');
        Route::get('/{invoice}/print', 'Incomes\InvoicesController@printInvoice');
        Route::get('/{invoice}/pdf', 'Incomes\InvoicesController@pdfInvoice');
        Route::get('/{invoice}/duplicate', 'Incomes\InvoicesController@duplicate');
        Route::post('/payment', 'Incomes\InvoicesController@payment');
        Route::delete('/payment/{payment}', 'Incomes\InvoicesController@paymentDestroy');
        Route::post('/import', 'Incomes\InvoicesController@import');
        Route::get('/createUtility', 'Incomes\InvoicesController@createUtility');
        Route::get('water/{invoice}', 'Incomes\InvoicesController@water');

      });
      Route::resource('invoices', 'Incomes\InvoicesController');

      /**
       * revenues
       */
      Route::group(['prefix' => 'revenues'], function() {
        Route::get('/{revenue}/duplicate', 'Incomes\RevenuesController@duplicate');
        Route::post('/import', 'Incomes\RevenuesController@import');        
      });
      Route::resource('revenues', 'Incomes\RevenuesController');

      /**
       * Water
       */
      Route::group(['prefix' => 'water'], function () {
        // Route::
      });
      Route::resource('water', 'Incomes\WaterController');
      /**
       * Customers
       */
      Route::group(['prefix' => 'customers'], function () {

      Route::get('invoice/{invoice}', function (Request $request) {
            if (! $request->hasValidSignature()) {
                abort(401);
            }
        })->name('customer.invoice');
      Route::get('currency/{customer_id}', 'Incomes\CustomersController@currency');
      Route::get('reading/{customer_id}', 'Incomes\CustomersController@reading');
      });
      Route::resource('customers', 'Incomes\CustomersController');
      
    });
    /**
   * Banking
   */
    Route::group(['prefix' => 'banking'], function () {
        Route::resource('accounts', 'Banking\AccountsController');
        Route::resource('transactions', 'Banking\TransactionsController');
        Route::resource('transfers', 'Banking\TransfersController');
    });
        
});
