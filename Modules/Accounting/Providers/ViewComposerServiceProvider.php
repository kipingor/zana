<?php

namespace Modules\Accounting\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    // protected $defer = false;

    public function boot()
    {
        // Add limits to index
        view()->composer(
            ['accounting::categories.index'],
            'Modules\Accounting\Http\ViewComposers\IndexComposer'
        );

        // Add Vendor to index
        // view()->composer(
        //     ['accounting::expenses.bills.create'],
        //     'Modules\Accounting\Http\ViewComposers\VendorComposer'
        // );
        
       // Add company info to menu
        view()->composer(
            ['partials.admin.menu', 'partials.customer.menu'], 'Modules\Accounting\Http\ViewComposers\Menu'
        );

        // Add notifications to header
        view()->composer(
            ['partials.admin.header', 'partials.customer.header'], 'Modules\Accounting\Http\ViewComposers\HeaderComposer'
        );

        
        
        // Add recurring
        view()->composer(
            ['partials.form.recurring',], 'Modules\Accounting\Http\ViewComposers\RecurringComposer'
        );

        // Add logo
        view()->composer(
            ['accounting::incomes.invoices.invoice', 'accounting::expenses.bills.bill'], 'Modules\Accounting\Http\ViewComposers\LogoComposer'
        );
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
