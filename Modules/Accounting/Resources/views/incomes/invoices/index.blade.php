@extends('accounting::layouts.master')

@section('navlink')
<nav class="navbar navbar-default bg-master-lighter sm-padding-10" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sub-nav">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="sub-nav">
      <div class="row">
        <div class="col-sm-4">
          <ul class="nav navbar-nav navbar-center">
            <li><a href="{{ url('accounting/incomes/invoices/create') }}" data-toggle="tooltip" data-placement="bottom" title="Add Invoice"><i class="fas fa-file-invoice fa-2x" style="color: green"></i></a></li>
            <li><a href="{{ url('accounting/incomes/invoices/createUtility') }}" data-toggle="tooltip" data-placement="bottom" title="Add Utility Bill"><i class="fas fa-file-invoice-dollar fa-2x" style="color: orange"></i></a></li>
            <li><a href="{{ url('common/import/incomes/invoices') }}" data-toggle="tooltip" data-placement="bottom" title="Import"><i class="fas fa-file-import fa-2x" style="color: blue"></i></a></li>
          </ul>
        </div>        
      </div>
    </div>
    <!-- /.navbar-collapse -->
  </div>
  <!-- /.container-fluid -->
</nav>
@endsection

@section('content')
<div class="container-fluid container-fixed-lg bg-white">
  <!-- START PANEL -->
  <div class="panel panel-transparent">
    <div class="panel-heading">
      <div class="panel-title">Invoices
      </div>
      <div class="pull-right">
        <div class="col-xs-12">
          <input type="text" id="search-table" class="form-control pull-right" placeholder="Search">
        </div>
      </div>
      <div class="clearfix"></div>
    </div>
    <div class="panel-body">
      <table class="table table-hover demo-table-search table-responsive-block" id="tableWithInvoicesAndSearch">
        <thead>
          <tr>
            <th class="col-md-2">{{ trans_choice('general.numbers', 1) }}</th>
            <th class="col-md-2">{{ trans_choice('general.customers', 1) }}</th>
            <th class="col-md-2 text-right amount-space">{{ trans('general.amount') }}</th>
            <th class="col-md-2">{{ trans('invoices.invoice_date') }}</th>
            <th class="col-md-2">{{ trans('invoices.due_date') }}</th>
            <th class="col-md-1">{{ trans_choice('general.statuses', 1) }}</th>
            <th class="col-md-1 text-center">{{ trans('general.actions') }}</th>
          </tr>
        </thead>
        <tbody>
          @foreach($invoices as $item)
          <tr>
            <td class="v-align-middle semi-bold">
              <p><a href="{{ url('accounting/incomes/invoices/' . $item->id . ' ') }}">{{ $item->invoice_number }}</a></p>
            </td>
            <td class="v-align-middle">
              <p>{{ $item->customer_name }}</p>
            </td>
            <td class="v-align-middle text-right">
              <p>@money($item->amount, $item->currency_code, true)</p>
            </td>
            <td class="v-align-middle">
              <p>{{ $item->invoiced_at }}</p>
            </td>
            <td class="v-align-middle">
              <p>{{ $item->due_at }}</p>
            </td>
            <td class="v-align-middle">
              <p><span class="label label-default">{{ $item->invoice_status_code }}</span></p>
            </td>
            <td class="v-align-middle">
              <div class="btn-group">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" data-toggle-position="left" aria-expanded="false">
                  <i class="fa fa-ellipsis-h"></i>
                </button>
                <ul class="dropdown-menu dropdown-menu-right">
                  <li><a href="{{ url('accounting/incomes/invoices/' . $item->id) }}">{{ trans('general.show') }}</a></li>
                  <li><a href="{{ url('accounting/incomes/invoices/' . $item->id . '/edit') }}">{{ trans('general.edit') }}</a></li>
                  @if(auth()->user()->hasPermission('create.incomes.invoices'))
                  <li class="divider"></li>
                  <li><a href="{{ url('accounting/incomes/invoices/' . $item->id . '/duplicate') }}">{{ trans('general.duplicate') }}</a></li>
                  @endif
                  <li class="divider"></li>
                  @if(auth()->user()->hasPermission('delete.incomes.invoices'))
                  <li>{!! Form::deleteLink($item, 'accounting/incomes/invoices') !!}</li>
                  @endif
                </ul>
              </div>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
  <!-- END PANEL -->
</div>
@endsection

@include('layouts.admin.partials.datatables')
@push('script')
<script type="text/javascript">
  $(document).ready(function() {
    $('#tableWithSearch').DataTable( {
        "order": [[ 1, "desc" ]]
    } );
} );
</script>
@endpush