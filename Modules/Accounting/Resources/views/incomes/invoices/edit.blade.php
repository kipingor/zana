@extends('accounting::layouts.master')

@section('content')
<div class="container-fluid container-fixed-lg bg-white">
	<div class="panel panel-transparent">
		<div class="panel-heading">
      <div class="panel-title">
      	Edit Invoice {{ $invoice->invoice_number}}
      </div>
    </div>
    <div class="panel-body">
    	{!! Form::model($invoice, ['method' => 'PATCH', 'files' => true, 'url' => ['accounting/incomes/invoices', $invoice->id], 'role' => 'form']) !!}

    	{{ Form::selectGroup('customer_id', trans_choice('general.customers', 1), 'user', $customers, config('general.customers')) }}

            {{ Form::selectGroup('currency_code', trans_choice('general.currencies', 1), 'exchange', $currencies) }}

            {{ Form::textGroup('invoiced_at', trans('invoices.invoice_date'), 'calendar', ['id' => 'invoiced_at', 'class' => 'form-control', 'required' => 'required', 'data-inputmask' => '\'alias\': \'yyyy-mm-dd\'', 'data-mask' => ''], Carbon\Carbon::parse($invoice->invoiced_at)->toDateString()) }}

            {{ Form::textGroup('due_at', trans('invoices.due_date'), 'calendar', ['id' => 'due_at', 'class' => 'form-control', 'required' => 'required', 'data-inputmask' => '\'alias\': \'yyyy-mm-dd\'', 'data-mask' => ''], Carbon\Carbon::parse($invoice->due_at)->toDateString()) }}

            {{ Form::textGroup('invoice_number', trans('invoices.invoice_number'), 'file-text-o') }}

            {{ Form::textGroup('order_number', trans('invoices.order_number'), 'shopping-cart',[]) }}
            <div class="form-group col-md-12">
                {!! Form::label('items', trans_choice('general.items', 2), ['class' => 'control-label']) !!}
                <div class="table-responsive">
                    <table class="table table-bordered" id="items">
                        <thead>
                            <tr style="background-color: #f9f9f9;">
                                <th width="5%"  class="text-center">{{ trans('general.actions') }}</th>
                                <th width="40%" class="text-left">{{ trans('general.name') }}</th>
                                <th width="5%" class="text-center">{{ trans('invoices.quantity') }}</th>
                                <th width="10%" class="text-right">{{ trans('invoices.price') }}</th>
                                <th width="15%" class="text-right">{{ trans_choice('general.taxes', 1) }}</th>
                                <th width="10%" class="text-right">{{ trans('invoices.total') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $item_row = 0; ?>
                        @foreach($invoice->items as $item)
                            <tr id="item-row-{{ $item_row }}">
                                <td class="text-center" style="vertical-align: middle;">
                                    <button type="button" onclick="$(this).tooltip('destroy'); $('#item-row-{{ $item_row }}').remove(); totalItem();" data-toggle="tooltip" title="{{ trans('general.delete') }}" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>
                                </td>
                                <td>
                                    <input value="{{ $item->name }}" class="form-control typeahead" required="required" placeholder="{{ trans('general.form.enter', ['field' => trans_choice('invoices.item_name', 1)]) }}" name="item[{{ $item_row }}][name]" type="text" id="item-name-{{ $item_row }}">
                                    <input value="{{ $item->item_id }}" name="item[{{ $item_row }}][item_id]" type="hidden" id="item-id-{{ $item_row }}">
                                </td>
                                <td>
                                    <input value="{{ $item->quantity }}" class="form-control text-center" required="required" name="item[{{ $item_row }}][quantity]" type="text" id="item-quantity-{{ $item_row }}">
                                </td>
                                <td>
                                    <input value="{{ $item->price }}" class="form-control text-right" required="required" name="item[{{ $item_row }}][price]" type="text" id="item-price-{{ $item_row }}">
                                </td>
                                <td>
                                    {!! Form::select('item[' . $item_row . '][tax_id]', $taxes, $item->tax_id, ['id'=> 'item-tax-'. $item_row, 'class' => 'form-control tax-select2', 'placeholder' => trans('general.form.enter', ['field' => trans_choice('general.taxes', 1)])]) !!}
                                </td>
                                <td class="text-right" style="vertical-align: middle;">
                                    <span id="item-total-{{ $item_row }}">@money($item->total, $invoice->currency_code, true)</span>
                                </td>
                            </tr>
                            <?php $item_row++; ?>
                        @endforeach
                        @if (empty($invoice->items))
                            <tr id="item-row-{{ $item_row }}">
                                <td class="text-center" style="vertical-align: middle;">
                                    <button type="button" onclick="$(this).tooltip('destroy'); $('#item-row-{{ $item_row }}').remove(); totalItem();" data-toggle="tooltip" title="{{ trans('general.delete') }}" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>
                                </td>
                                <td>
                                    <input class="form-control typeahead" required="required" placeholder="{{ trans('general.form.enter', ['field' => trans_choice('invoices.item_name', 1)]) }}" name="item[{{ $item_row }}][name]" type="text" id="item-name-{{ $item_row }}" autocomplete="off">
                                    <input name="item[{{ $item_row }}][item_id]" type="hidden" id="item-id-{{ $item_row }}">
                                </td>
                                <td>
                                    <input class="form-control text-center" required="required" name="item[{{ $item_row }}][quantity]" type="text" id="item-quantity-{{ $item_row }}">
                                </td>
                                <td>
                                    <input class="form-control text-right" required="required" name="item[{{ $item_row }}][price]" type="text" id="item-price-{{ $item_row }}">
                                </td>
                                <td>
                                    {!! Form::select('item[' . $item_row . '][tax_id]', $taxes, null, ['id'=> 'item-tax-'. $item_row, 'class' => 'form-control select2', 'placeholder' => trans('general.form.select.field', ['field' => trans_choice('general.taxes', 1)])]) !!}
                                </td>
                                <td class="text-right" style="vertical-align: middle;">
                                    <span id="item-total-{{ $item_row }}">0</span>
                                </td>
                            </tr>
                        @endif
                            <?php $item_row++; ?>
                            <tr id="addItem">
                                <td class="text-center"><button type="button" onclick="addItem();" data-toggle="tooltip" title="{{ trans('general.add') }}" class="btn btn-xs btn-primary" data-original-title="{{ trans('general.add') }}"><i class="fa fa-plus"></i></button></td>
                                <td class="text-right" colspan="5"></td>
                            </tr>
                            <tr>
                                <td class="text-right" colspan="5"><strong>{{ trans('invoices.sub_total') }}</strong></td>
                                <td class="text-right"><span id="sub-total">0</span></td>
                            </tr>
                            <tr>
                                <td class="text-right" style="vertical-align: middle;" colspan="5">
                                    <a href="javascript:void(0)" id="discount-text" rel="popover">{{ trans('invoices.add_discount') }}</a>
                                </td>
                                <td class="text-right">
                                    <span id="discount-total"></span>
                                    {!! Form::hidden('discount', null, ['id' => 'discount', 'class' => 'form-control text-right']) !!}
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right" colspan="5"><strong>{{ trans_choice('general.taxes', 1) }}</strong></td>
                                <td class="text-right"><span id="tax-total">0</span></td>
                            </tr>
                            <tr>
                                <td class="text-right" colspan="5"><strong>{{ trans('invoices.total') }}</strong></td>
                                <td class="text-right"><span id="grand-total">0</span></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            {{ Form::textareaGroup('notes', trans_choice('general.notes', 2)) }}

            {{ Form::selectGroup('category_id', trans_choice('general.categories', 1), 'folder-open-o', $categories) }}

            {{ Form::recurring('edit', $invoice) }}

            {{ Form::fileGroup('attachment', trans('general.attachment')) }}

            @permission('update.incomes.invoices')
		        <div class="box-footer">
		            {{ Form::saveButtons('incomes/invoices') }}
		        </div>
		        <!-- /.box-footer -->
		        @endpermission
		        {!! Form::close() !!}
    </div>
	</div>
</div>
@endsection

@include('accounting::incomes.invoices.view')