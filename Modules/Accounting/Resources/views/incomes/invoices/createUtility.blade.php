@extends('accounting::layouts.master')

@section('content')
<div class="container-fluid container-fixed-lg bg-white">  
  <div class="panel panel-transparent">
    <div class="panel-heading">
      <div class="panel-title">Add Utility Bill
      </div>
    </div>
    <div class="panel-body">
      {!! Form::open(['url' => 'accounting/incomes/water', 'files' => true, 'role' => 'form']) !!}
      <div class="col-md-6">
        <div class="form-group form-group-default form-group-default-select2 input-group required {{ $errors->has('customer_id') ? 'has-error' : ''}}">
          <span class="input-group-addon primary"><i class="fas fa-user"></i></span>
            {!! Form::label('customer_id', trans_choice('general.customers', 1), ['class' => 'control-label']) !!}
                
            {!! Form::select('customer_id', $customers, null, array_merge(['class' => 'full-width', 'data-placeholder' => trans('general.form.select.field',  ['field' => trans_choice('general.customers', 1)]), 'data-init-plugin' => 'select2'])) !!}
                <div class="input-group-btn">
                  <button type="button" onclick="createCustomer();" class="btn btn-primary btn-icon"><i class="fas fa-plus"></i></button>
                </div>
            {!! $errors->first('customer_id', '<p class="help-block">:message</p>') !!}
        </div>
      </div>

      {{ Form::selectGroup('currency_code', trans_choice('general.currencies', 1), 'money-bill-wave', $currencies, setting('general.default_currency')) }}

      {{ Form::textGroup('invoiced_at', trans('invoices.invoice_date'), 'calendar',['id' => 'invoiced_at', 'class' => 'form-control', 'required' => 'required', 'data-inputmask' => '\'alias\': \'yyyy/mm/dd\'', 'data-mask' => ''], Carbon\Carbon::now()->toDateString()) }}

      {{ Form::textGroup('due_at', trans('invoices.due_date'), 'calendar',['id' => 'due_at', 'class' => 'form-control', 'required' => 'required', 'data-inputmask' => '\'alias\': \'yyyy/mm/dd\'', 'data-mask' => '', 'autocomplete'=> 'off'], Carbon\Carbon::now()->addDays(15)->toDateString()) }}

      {{ Form::textGroup('invoice_number', trans('invoices.invoice_number'), 'file-invoice', ['required' => 'required'], $number) }}

      {{ Form::textGroup('order_number', trans('invoices.order_number'), 'shopping-cart', []) }}

      <div class="form-group col-md-12">
        {!! Form::label('items', trans_choice('general.items', 2), ['class' => 'control-label']) !!}
        <div class="table-responsive">
          <table class="table table-bordered" id="items">
            <thead>
              <tr style="background-color: #f9f9f9;">
                <th width="5%"  class="text-center">{{ trans('general.actions') }}</th>
                <th width="15%" class="text-left">{{ trans('utilities.previous_reading') }}</th>
                <th width="15%" class="text-left">{{ trans('utilities.current_reading') }}</th>
                <th width="15%" class="text-center">{{ trans('invoices.quantity') }}</th>
                <th width="10%" class="text-right">{{ trans('invoices.price') }}</th>
                <th width="15%" class="text-right">{{ trans_choice('general.taxes', 1) }}</th>
                <th width="10%" class="text-right">{{ trans('invoices.total') }}</th>
              </tr>
            </thead>
            <tbody>              
              <?php $item_row = 0; ?>
              <tr>
                <td class="text-center" style="vertical-align: middle;">
                  <button type="button" onclick="$(this).tooltip('destroy'); $('#item-row-{{ $item_row }}').remove(); totalItem();" data-toggle="tooltip" title="{{ trans('general.delete') }}" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>
                </td>
                <td>
                  <input class="form-control" required="required" placeholder="Previous Meter Reading" name="previous" type="text" id="item_previous" autocomplete="off">
                  <p id="ptext"></p>
                </td>
                <td>
                  <input class="form-control" required="required" placeholder="Current Meter Reading" name="current" type="text" id="item_current" autocomplete="off"> 
                  <p id="ctext"></p>                 
                </td>
                <td>
                  <input class="form-control text-center" required="required" name="quantity" type="text" id="item-quantity">
                </td>
                <td>
                  <input class="form-control text-right" required="required" name="price" type="text" id="item-price">
                </td>
                <td>
                  {!! Form::select('item[' . $item_row . '][tax_id]', $taxes, setting('general.default_tax'), ['id'=> 'item-tax-'. $item_row, 'class' => 'form-control tax-select2', 'placeholder' => trans('general.form.select.field', ['field' => trans_choice('general.taxes', 1)])]) !!}
                </td>
                <td class="text-right" style="vertical-align: middle;">
                  <span id="item-total">0</span>
                </td>
              </tr>
            
              <?php $item_row++; ?>
              <tr id="addItem">
                <td class="text-center"><button type="button" onclick="addItem();" data-toggle="tooltip" title="{{ trans('general.add') }}" class="btn btn-xs btn-primary" data-original-title="{{ trans('general.add') }}"><i class="fa fa-plus"></i></button></td>
                <td class="text-right" colspan="6"></td>
              </tr>
              <tr>
                <td class="text-right" colspan="6"><strong>{{ trans('invoices.sub_total') }}</strong></td>
                <td class="text-right"><span id="sub-total">0</span></td>
              </tr>
              <tr>
                <td class="text-right" style="vertical-align: middle;" colspan="6">
                  <a href="javascript:void(0)" id="discount-text" rel="popover">{{ trans('invoices.add_discount') }}</a>
                </td>
                <td class="text-right">
                  <span id="discount-total"></span>
                  {!! Form::hidden('discount', null, ['id' => 'discount', 'class' => 'form-control text-right']) !!}
                </td>
              </tr>
              <tr>
                <td class="text-right" colspan="6"><strong>{{ trans_choice('general.taxes', 1) }}</strong></td>
                <td class="text-right"><span id="tax-total">0</span></td>
              </tr>
              <tr>
                <td class="text-right" colspan="6"><strong>{{ trans('invoices.total') }}</strong></td>
                <td class="text-right"><span id="grand-total">0</span></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>

      {{ Form::textareaGroup('notes', trans_choice('general.notes', 2)) }}

      <div class="form-group col-md-6 required {{ $errors->has('category_id') ? 'has-error' : ''}}">
        {!! Form::label('category_id', trans_choice('general.categories', 1), ['class' => 'control-label']) !!}
        <div class="input-group">
          <div class="input-group-addon"><i class="fa fa-folder-open-o"></i></div>
          {!! Form::select('category_id', $categories, null, array_merge(['class' => 'form-control', 'placeholder' => trans('general.form.select.field', ['field' => trans_choice('general.categories', 1)])])) !!}
          <div class="input-group-btn">
            <button type="button" onclick="createCategory();" class="btn btn-default btn-icon"><i class="fa fa-plus"></i></button>
          </div>
        </div>
        {!! $errors->first('category_id', '<p class="help-block">:message</p>') !!}
      </div>

      {{ Form::recurring('create') }}

      {{ Form::fileGroup('attachment', trans('general.attachment')) }}

      {{ Form::saveButtons('incomes/invoices') }}
      {!! Form::close() !!}    
    </div>
  </div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('assets/plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-typehead/typeahead.bundle.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-typehead/typeahead.jquery.min.js') }}"></script>

<script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.2/js/bootstrap-colorpicker.js"></script>
<script src="{{ asset('assets/js/bootstrap-fancyfile.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
    {{-- <script src="{{ asset('assets/js/invoice_elements.js') }}" type="text/javascript"></script> --}}
<script type="text/javascript">
    $(document).ready(function(){
      //Date picker
      $('#invoiced_at').datepicker({
          format: 'yyyy-mm-dd',
          autoclose: true,
          language: '{{ language()->getShortCode() }}'
      });

      //Date picker
        $('#due_at').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            language: '{{ language()->getShortCode() }}'
        });

         $(".tax-select2").select2({
            placeholder: "{{ trans('general.form.select.field', ['field' => trans_choice('general.taxes', 1)]) }}"
        });

         $("#customer_id").select2({
            placeholder: "{{ trans('general.form.select.field', ['field' => trans_choice('general.customers', 1)]) }}"
        });

         $("#currency_code").select2({
            placeholder: "{{ trans('general.form.select.field', ['field' => trans_choice('general.currencies', 1)]) }}"
        });

         $("#category_id").select2({
            placeholder: "{{ trans('general.form.select.field', ['field' => trans_choice('general.categories', 1)]) }}"
        });

         $('#attachment').fancyfile({
            text  : '{{ trans('general.form.select.file') }}',
            style : 'btn-default',
            placeholder : '{{ trans('general.form.no_file_selected') }}'
        });


        $( "#item_current" ).keyup(function() {
          let current = $(this).val();
          const previous = $('#item_previous').val();
          qty = current - previous;
          $('#item-quantity').val(qty); 
          item_total();
        });

        //Calculate the Total Line amount based on the quantity and price
        function item_total() {         
          let qty = $("#item-quantity").val();
          const price = 200
          let total = qty * price;
          $('#item-total').text(total);
          totalItem();
        }
    });

$(document).on('change', '#customer_id', function() {
  let reading_path = "{{ url('accounting/incomes/customers/reading') }}";
  axios.get('/accounting/incomes/customers/reading/'+ $(this).val())
    .then(response => {        
        let reading = response.data;
        console.log(reading);   
        $('#item_previous').val(reading);
        item_total()
    })
    .catch(function (error) {
    console.log(error);
  });
  
});
        
$(document).ready(function(){
        $('a[rel=popover]').popover({
            html: 'true',
            placement: 'bottom',
            title: '{{ trans('invoices.discount') }}',
            content: function () {
                html  = '<div class="discount box-body">';
                html += '    <div class="col-md-6">';
                html += '        <div class="input-group" id="input-discount">';
                html += '            {!! Form::number('pre-discount', null, ['id' => 'pre-discount', 'class' => 'form-control text-right']) !!}';
                html += '            <div class="input-group-addon"><i class="fa fa-percent"></i></div>';
                html += '        </div>';
                html += '    </div>';
                html += '    <div class="col-md-6">';
                html += '        <div class="discount-description">';
                html += '           {{ trans('invoices.discount_desc') }}';
                html += '        </div>';
                html += '    </div>';
                html += '</div>';
                html += '<div class="discount box-footer">';
                html += '    <div class="col-md-12">';
                html += '        <div class="form-group no-margin">';
                html += '            {!! Form::button('<span class="fa fa-save"></span> &nbsp;' . trans('general.save'), ['type' => 'button', 'id' => 'save-discount','class' => 'btn btn-success']) !!}';
                html += '            <a href="javascript:void(0)" id="cancel-discount" class="btn btn-default"><span class="fa fa-times-circle"></span> &nbsp;{{ trans('general.cancel') }}</a>';
                html += '       </div>';
                html += '    </div>';
                html += '</div>';

                return html;
            }
        });

        $(document).on('keyup', '#pre-discount', function(e){
            e.preventDefault();

            $('#discount').val($(this).val());

            totalItem();
        });

        $(document).on('click', '#save-discount', function(){
            $('a[rel=popover]').trigger('click');
        });

        $(document).on('click', '#cancel-discount', function(){
            $('#discount').val('');

            totalItem();

            $('a[rel=popover]').trigger('click');
        });

        $(document).on('change', '#currency_code, #items tbody select', function(){
            totalItem();
        });

        

        
    });

    function totalItem() {
        $.ajax({
            url: '{{ url("accounting/items/totalItem") }}',
            type: 'POST',
            dataType: 'JSON',
            data: $('#currency_code, #discount input[type=\'number\'], #items input[type=\'text\'],#items input[type=\'number\'],#items input[type=\'hidden\'], #items textarea, #items select'),
            headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
            success: function(data) {
                if (data) {
                    $('#discount-text').text(data.discount_text);

                    $('#sub-total').html(data.sub_total);
                    $('#discount-total').html(data.discount_total);
                    $('#tax-total').html(data.tax_total);
                    $('#grand-total').html(data.grand_total);
                }
            }
        });
    }

    

    
    
</script>

@endpush

@include('accounting::partials.create_customer')
@include('accounting::partials.create_category')