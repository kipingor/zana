@extends('accounting::layouts.master')
@section('navlink')
<nav class="navbar navbar-default bg-master-lighter sm-padding-10" role="navigation">
	<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sub-nav">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			</button>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="sub-nav">
			<div class="row">
				<div class="col-sm-4">
					<ul class="nav navbar-nav">
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-file-text m-r-10"></i> Zana <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								@if($invoice->invoice_status_code != 'paid')
								@if(auth()->user()->hasPermission('update.incomes.invoices'))
								<li><a href="{{ url('accounting/incomes/invoices/' . $invoice->id . '/pay') }}">{{ trans('invoices.mark_paid') }}</a></li>
								@endif
								@if(empty($invoice->payments()->count()) || (!empty($invoice->payments()->count()) && $invoice->payments()->paid() != $invoice->amount))
								<li><a href="#" id="button-payment">{{ trans('invoices.add_payment') }}</a></li>
								@endif
								<li class="divider"></li>
								@endif
								@if(auth()->user()->hasPermission('update.incomes.invoices'))
								@if($invoice->invoice_status_code == 'draft')
								<li><a href="{{ url('accounting/incomes/invoices/' . $invoice->id . '/sent') }}">{{ trans('invoices.mark_sent') }}</a></li>
								@else
								<li><a href="javascript:void(0);" class="disabled"><span class="text-disabled">{{ trans('invoices.mark_sent') }}</span></a></li>
								@endif
								@endif
								@if($invoice->customer_email)
								<li><a href="{{ url('accounting/incomes/invoices/' . $invoice->id . '/email') }}">{{ trans('invoices.send_mail') }}</a></li>
								@else
								<li><a href="javascript:void(0);" class="green-tooltip disabled" data-toggle="tooltip" data-placement="right" title="{{ trans('invoices.messages.email_required') }}"><span class="text-disabled">{{ trans('invoices.send_mail') }}</span></a></li>
								@endif
								<li class="divider"></li>
								@if(auth()->user()->hasPermission('delete.incomes.invoices'))
								<li>{!! Form::deleteLink($invoice, 'accounting/incomes/invoices') !!}</li>
								@endif
							</ul>
						</li>
					</ul>
				</div>
				<div class="col-sm-4">
					<ul class="nav navbar-nav navbar-center">
						<li><a href="{{ url('accounting/incomes/invoices/' . $invoice->id . '/edit') }}"><i class="fa fa-pencil-square-o"></i>&nbsp; {{ trans('general.edit') }}</a></li>
						<li><a href="{{ url('accounting/incomes/invoices/' . $invoice->id . '/print') }}" target="_blank" data-toggle="tooltip" data-placement="bottom" title="Print"><i class="fa fa-print"></i></a></li>
						<li><a href="{{ url('accounting/incomes/invoices/' . $invoice->id . '/pdf') }}" data-toggle="tooltip" data-placement="bottom" title="Download"><i class="fa fa-download"></i></a></li>
					</ul>
					@if($invoice->attachment)
					<span class="attachment">
						<a href="{{ url('uploads/' . $invoice->attachment->id . '/download') }}">
							<span id="download-attachment" class="text-primary">
								<i class="fa fa-file-{{ $invoice->attachment->aggregate_type }}-o"></i> {{ $invoice->attachment->basename }}
							</span>
						</a>
						{!! Form::open([
						'id' => 'attachment-' . $invoice->attachment->id,
						'method' => 'DELETE',
						'url' => [url('uploads/' . $invoice->attachment->id)],
						'style' => 'display:inline'
						]) !!}
						<a id="remove-attachment" href="javascript:void();">
							<span class="text-danger"><i class="fa fa fa-times"></i></span>
						</a>
						{!! Form::close() !!}
					</span>
					@endif
				</div>
				<div class="col-sm-4">
					<ul class="nav navbar-nav navbar-right">
						<li>
							<a href="#" class="p-r-10"><img width="25" height="25" alt="" class="icon-pdf" data-src-retina="{{ asset('assets/img/invoice/pdf2x.png') }}" data-src="{{ asset('assets/img/invoice/pdf.png') }}" src="{{ asset('assets/img/invoice/pdf2x.png') }}"></a>
						</li>
						<li>
							<a href="#" class="p-r-10"><img width="25" height="25" alt="" class="icon-image" data-src-retina="{{ asset('assets/img/invoice/image2x.png') }}" data-src="{{ asset('assets/img/invoice/image.png') }}" src="{{ asset('assets/img/invoice/image2x.png') }}"></a>
						</li>
						<li>
							<a href="#" class="p-r-10"><img width="25" height="25" alt="" class="icon-doc" data-src-retina="{{ asset('assets/img/invoice/doc2x.png') }}" data-src="{{ asset('assets/img/invoice/doc.png') }}" src="{{ asset('assets/img/invoice/doc2x.png') }}"></a>
						</li>
						<li><a href="#" class="p-r-10" onclick="$.Pages.setFullScreen(document.querySelector('html'));"><i class="fa fa-expand"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
		<!-- /.navbar-collapse -->
	</div>
	<!-- /.container-fluid -->
</nav>
@endsection
@section('content')
<div class="container-fluid container-fixed-lg">
	@if (($recurring = $invoice->recurring) && ($next = $recurring->next()))
	<div>
		<h4>{{ trans('recurring.recurring') }}</h4>
		<p>{{ trans('recurring.message', [
			'type' => mb_strtolower(trans_choice('general.invoices', 1)),
			'date' => $next->format($date_format)
			]) }}
		</p>
	</div>
	@endif
	<div class="panel panel-default">
		<div class="panel-body">
			<span class="badge badge-success">{{ $invoice->invoice_status_code }}</span>
			<div class="invoice padding-50 sm-padding-10">
				<div>
					<div class="pull-left">
						@if (setting('general.invoice_logo'))
						<img src="{{ auth()->user()->invoice_logo ? auth()->user()->getMedia('invoice_logo')->first()->getUrl() : asset('img/akaunting-logo-green.png') }}" class="invoice-logo" />
						@elseif (setting('general.company_logo'))
						<img src="{{ Storage::url(setting('general.company_logo')) }}" class="invoice-logo" />
						@else
						<img src="{{ asset('public/img/company.png') }}" class="invoice-logo" />
						@endif
						<address class="m-t-10">
							<strong>{{ setting('general.company_name') }}</strong><br>
							{!! nl2br(setting('general.company_address')) !!}<br>
							@if (setting('general.company_tax_number'))
							{{ trans('general.tax_number') }}: {{ setting('general.company_tax_number') }}<br>
							@endif
							<br>
							@if (setting('general.company_phone'))
							{{ setting('general.company_phone') }}<br>
							@endif
							{{ setting('general.company_email') }}
						</address>
					</div>
					<div class="pull-right sm-m-t-20">
						<h2 class="font-montserrat all-caps hint-text">Invoice</h2>
					</div>
					<div class="clearfix"></div>
				</div>
				<br>
				<br>
				<div class="container-sm-height">
					<div class="row-sm-height">
						<div class="col-md-9 col-sm-height sm-no-padding">
							<p class="small no-margin">{{ trans('invoices.bill_to') }}</p>
							<h5 class="semi-bold m-t-0">{{ $invoice->customer_name }}</h5>
							<address>
								@if ($invoice->customer_tax_number)
								{{ trans('general.tax_number') }}: {{ $invoice->customer_tax_number }}<br>
								@endif
								<br>
								@if ($invoice->customer_phone)
								{{ $invoice->customer_phone }}<br>
								@endif
								{{ $invoice->customer_email }}
								<br>
								{!! nl2br($invoice->customer_address) !!}
							</address>
						</div>
						<div class="col-md-3 col-sm-height col-bottom sm-no-padding sm-p-b-20">
							<br>
							<div>
								<div class="pull-left font-montserrat bold all-caps">{{ trans('invoices.invoice_number') }} :</div>
								<div class="pull-right">{{ $invoice->invoice_number }}</div>
								<div class="clearfix"></div>
							</div>
							@if ($invoice->order_number)
							<div>
								<div class="pull-left font-montserrat bold all-caps">{{ trans('invoices.order_number') }} :</div>
								<div class="pull-right">{{ $invoice->order_number }}</div>
								<div class="clearfix"></div>
							</div>
							@endif
							<div>
								<div class="pull-left font-montserrat bold all-caps">{{ trans('invoices.invoice_date') }} :</div>
								<div class="pull-right">{{ Carbon\Carbon::parse($invoice->invoiced_at)->toFormattedDateString() }}</div>
								<div class="clearfix"></div>
							</div>
							<div>
								<div class="pull-left font-montserrat bold all-caps">{{ trans('invoices.payment_due') }} :</div>
								<div class="pull-right">{{ Carbon\Carbon::parse($invoice->due_at)->toFormattedDateString() }}</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="table-responsive">
					<table class="table m-t-50">
						<thead>
							<tr>
								<th class="">{{ trans_choice('general.items', 1) }}</th>
								<th class="text-center">{{ trans('invoices.quantity') }}</th>
								<th class="text-center">{{ trans('invoices.price') }}</th>
								<th class="text-right">{{ trans('invoices.total') }}</th>
							</tr>
						</thead>
						<tbody>
							@foreach($invoice->items as $item)
							<tr>
								<td class="">
									<p class="text-black">{{ $item->name }}</p>
									@if ($item->sku)
									<p class="small hint-text">
										{{ trans('items.sku') }}: {{ $item->sku }}
									</p>
									@endif
								</td>
								<td class="text-center">{{ $item->quantity }}</td>
								<td class="text-center">@money($item->price, $invoice->currency_code, true)</td>
								<td class="text-right">@money($item->total, $invoice->currency_code, true)</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<br>
				<br>
				<br>
				<br>
				<br>
				<div>
					<img width="150" height="58" alt="" class="invoice-signature" data-src-retina="assets/img/invoice/signature2x.png" data-src="assets/img/invoice/signature.png" src="assets/img/invoice/signature2x.png">
					@if ($invoice->notes)
					<p class="lead">{{ trans_choice('general.notes', 2) }}</p>
					<p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
						{{ $invoice->notes }}
					</p>
					@endif
				</div>
				<br>
				<br>
				<div class="container-sm-height">
					@foreach ($invoice->totals as $total)
					<div class="row row-sm-height b-a b-grey">
						<div class="col-sm-7 col-sm-height col-middle p-l-25 sm-p-t-15 sm-p-l-15 clearfix sm-p-b-15">
						</div>
						@if ($total->code != 'total')
						<div class="col-sm-5 text-right bg-menu col-sm-height padding-15">
							<h5 class="font-montserrat all-caps small no-margin hint-text text-white bold">
							{{ trans($total->title) }}
							</h5>
							<h1 class="no-margin text-white">@money($total->amount, $invoice->currency_code, true)</h1>
						</div>
						@else
						@if ($invoice->paid)
						<div class="col-sm-5 text-right bg-success col-sm-height padding-15">
							<h5 class="font-montserrat all-caps small no-margin hint-text text-white bold">
							{{ trans('invoices.paid') }}
							</h5>
							<h1 class="no-margin text-white">- @money($invoice->paid, $invoice->currency_code, true)</h1>
						</div>
						@endif
						<div class="col-sm-5 text-right bg-menu col-sm-height padding-15">
							<h5 class="font-montserrat all-caps small no-margin hint-text text-white bold">
							{{ trans($total->name) }}
							</h5>
							<h1 class="no-margin text-white">@money($total->amount - $invoice->paid, $invoice->currency_code, true)</h1>
						</div>
						@endif
					</div>
					@endforeach
				</div>
				<hr>
				<p class="small hint-text">Services will be invoiced in accordance with the Service Description. You must pay all undisputed invoices in full within 30 days of the invoice date, unless otherwise specified under the Special Terms and Conditions. All payments must reference the invoice number. Unless otherwise specified, all invoices shall be paid in the currency of the invoice</p>
				<p class="small hint-text">Insight retains the right to decline to extend credit and to require that the applicable purchase price be paid prior to performance of Services based on changes in insight's credit policies or your financial condition and/or payment record. Insight reserves the right to charge interest of 1.5% per month or the maximum allowable by applicable law, whichever is less, for any undisputed past due invoices. You are responsible for all costs of collection, including reasonable attorneys' fees, for any payment default on undisputed invoices. In addition, Insight may terminate all further work if payment is not received in a timely manner.</p>
				<br>
				<hr>
				<div>
					<img src="assets/img/logo.png" alt="logo" data-src="assets/img/logo.png" data-src-retina="assets/img/logo_2x.png" width="78" height="22">
					<span class="m-l-70 text-black sm-pull-right">+34 346 4546 445</span>
					<span class="m-l-40 text-black sm-pull-right">support@revox.io</span>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- End of Invoice View -->
<div class="container-fluid container-fixed-lg">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-transparent">
				<div class="panel-heading">
					<div class="panel-title">Portlet Tools
					</div>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-6">
							<div data-pages="portlet" class="panel panel-default" id="portlet-basic">
								<div class="panel-heading ">
									<div class="panel-title">{{ trans('invoices.histories') }}
									</div>
									<div class="panel-controls">
										<ul>
											<li>
												<a data-toggle="collapse" class="portlet-collapse" href="#">
													<i class="portlet-icon portlet-icon-collapse"></i>
												</a>
											</li>
											<li>
												<a data-toggle="refresh" class="portlet-refresh" href="#">
													<i class="portlet-icon portlet-icon-refresh"></i>
												</a>
											</li>
											<li>
												<a data-toggle="close" class="portlet-close" href="#">
													<i class="portlet-icon portlet-icon-close"></i>
												</a>
											</li>
										</ul>
									</div>
								</div>
								<div class="panel-body">
									<div class="table-responsive">
										<table class="table">
											<thead>
												<tr>
													<th>{{ trans('general.date') }}</th>
													<th>{{ trans_choice('general.statuses', 1) }}</th>
													<th>{{ trans('general.description') }}</th>
												</tr>
											</thead>
											<tbody>
												@foreach($invoice->histories as $history)
												<tr>
													<td>{{ $history->created_at->toFormattedDateString() }}</td>
													<td>{{ $history->status_code }}</td>
													<td>{{ $history->description }}</td>
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div data-pages="portlet" class="panel panel-default" id="portlet-basic">
								<div class="panel-heading ">
									<div class="panel-title">{{ trans('invoices.payments') }}
									</div>
									<div class="panel-controls">
										<ul>
											<li>
												<a data-toggle="collapse" class="portlet-collapse" href="#">
													<i class="portlet-icon portlet-icon-collapse"></i>
												</a>
											</li>
											<li>
												<a data-toggle="refresh" class="portlet-refresh" href="#">
													<i class="portlet-icon portlet-icon-refresh"></i>
												</a>
											</li>
											<li>
												<a data-toggle="close" class="portlet-close" href="#">
													<i class="portlet-icon portlet-icon-close"></i>
												</a>
											</li>
										</ul>
									</div>
								</div>
								<div class="panel-body">
									<div class="table-responsive">
										<table class="table">
											<thead>
												<tr>
													<th>{{ trans('general.date') }}</th>
													<th>{{ trans('general.amount') }}</th>
													<th>{{ trans_choice('general.accounts', 1) }}</th>
													<th style="width: 15%;">{{ trans('general.actions') }}</th>
												</tr>
											</thead>
											<tbody>
												@foreach($invoice->payments as $payment)
												<tr>
													<td>{{ $payment->paid_at->toFormattedDateString() }}</td>
													<td>@money($payment->amount, $payment->currency_code, true)</td>
													<td>{{ $payment->account->name }}</td>
													<td>
														<a href="{{ url('incomes/invoices/' . $payment->id . '') }}" class="btn btn-info btn-xs hidden"><i class="fa fa-eye" aria-hidden="true"></i> {{ trans('general.show') }}</a>
														<a href="{{ url('incomes/revenues/' . $payment->id . '/edit') }}" class="btn btn-primary btn-xs  hidden"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{ trans('general.edit') }}</a>
														{!! Form::open([
														'id' => 'invoice-payment-' . $payment->id,
														'method' => 'DELETE',
														'url' => ['incomes/invoices/payment', $payment->id],
														'style' => 'display:inline'
														]) !!}
														{!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> ' . trans('general.delete'), array(
														'type'    => 'button',
														'class'   => 'btn btn-danger btn-xs',
														'title'   => trans('general.delete'),
														'onclick' => 'confirmDelete("' . '#invoice-payment-' . $payment->id . '", "' . trans_choice('general.payments', 2) . '", "' . trans('general.delete_confirm', ['name' => '<strong>' . $payment->paid_at->toFormattedDateString() . ' - ' . money($payment->amount, $payment->currency_code, true) . ' - ' . $payment->account->name . '</strong>', 'type' => strtolower(trans_choice('general.revenues', 1))]) . '", "' . trans('general.cancel') . '", "' . trans('general.delete') . '")'
														)) !!}
														{!! Form::close() !!}
													</td>
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@push('styles')
<link href="{{ asset('assets/plugins/bootstrap-datepicker/css/datepicker3.css') }}" rel="stylesheet" type="text/css" media="screen">
<link href="{{ asset('assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet" type="text/css" media="screen">
<link href="{{ asset('assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css') }}" rel="stylesheet" type="text/css" media="screen">
@endpush
@push('scripts')
<script src="{{ asset('assets/js/portlets.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/jquery/jquery-easy.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/jquery-bez/jquery.bez.min.js') }}"></script>
<script src="{{ asset('assets/plugins/jquery-ios-list/jquery.ioslist.min.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ asset('assets/plugins/classie/classie.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.js') }}"></script>

{{-- <script src="{{ asset('assets/js/income_invoice.js') }}" type="text/javascript"></script> --}}
<script type="text/javascript">
	function addPayment() {
            $('.help-block').remove();

            $.ajax({
                url: '{{ url("/accounting/incomes/invoices/payment") }}',
                type: 'POST',
                dataType: 'JSON',
                data: $('#payment-modal input[type=\'text\'], #payment-modal input[type=\'hidden\'], #payment-modal textarea, #payment-modal select'),
                headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                beforeSend: function() {
                    $('#payment-modal .modal-content').append('<div id="loading" class="text-center"><i class="fa fa-spinner fa-spin fa-5x checkout-spin"></i></div>');
                },
                complete: function() {
                    $('#loading').remove();
                },
                success: function(json) {
                    if (json['error']) {
                        $('#payment-modal .modal-message').append('<div class="alert alert-danger">' + json['message'] + '</div>');
                        $('div.alert-danger').delay(3000).fadeOut(350);
                    }

                    if (json['success']) {
                        $('#payment-modal .modal-message').before('<div class="alert alert-success">' + json['message'] + '</div>');
                        $('div.alert-success').delay(3000).fadeOut(350);

                        setTimeout(function(){
                            $("#payment-modal").modal('hide');

                            location.reload();
                        }, 3000);
                    }
                },
                error: function(data){
                    var errors = data.responseJSON;

                    if (typeof errors !== 'undefined') {
                        if (errors.paid_at) {
                            $('#payment-modal #paid_at').parent().after('<p class="help-block">' + errors.paid_at + '</p>');
                        }

                        if (errors.amount) {
                            $('#payment-modal #amount').parent().after('<p class="help-block">' + errors.amount + '</p>');
                        }

                        if (errors.account_id) {
                            $('#payment-modal #account_id').parent().after('<p class="help-block">' + errors.account_id + '</p>');
                        }

                        if (errors.currency_code) {
                            $('#payment-modal #currency_code').parent().after('<p class="help-block">' + errors.currency_code + '</p>');
                        }

                        if (errors.category_id) {
                            $('#payment-modal #category_id').parent().after('<p class="help-block">' + errors.category_id + '</p>');
                        }

                        if (errors.payment_method) {
                            $('#payment-modal #payment_method').parent().after('<p class="help-block">' + errors.payment_method + '</p>');
                        }
                    }
                }
            });
        }
	$(document).ready(function(){

		$('#button-payment').click(function() {
    		$('#payment-modal').modal('show');
    	});
    	$('#add-app').click(function() {            
        $('#addNewAppModal').modal('hide');
      });
      const payment = {
			  heading: '{{ trans("invoices.add_payment") }}',
			  guardian: 'Mr. Kalehoff'
			};

      //Create Modal
      let html = `<div class="modal fade" id="payment-modal" tabindex="-1" role="dialog" aria-labelledby="paymentModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content box box-success">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="paymentModalLabel">{{ trans('invoices.add_payment') }}</h4> 
                            </div> 
                            <div class="modal-body box-body">
                                <div class="modal-message"></div>
                                <div class="form-group col-md-6 required"> 
                                    {!! Form::label('paid_at', trans('general.date'), ['class' => 'control-label']) !!} 
                                    <div class="input-group"> 
                                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div> 
                                        {!! Form::text('paid_at', \Carbon\Carbon::now()->toDateString(), ['id' => 'paid_at', 'class' => 'form-control', 'required' => 'required', 'data-inputmask' => '\'alias\': \'yyyy-mm-dd\'', 'data-mask' => '']) !!} 
                                    </div> 
                                </div> 
                                <div class="form-group col-md-6 required"> 
                                    {!! Form::label('amount', trans('general.amount'), ['class' => 'control-label']) !!} 
                                    <div class="input-group"> 
                                        <div class="input-group-addon"><i class="fa fa-money"></i></div> 
                                        {!! Form::text('amount', $invoice->grand_total, ['class' => 'form-control', 'required' => 'required', 'placeholder' => trans('general.form.enter', ['field' => trans('general.amount')])]) !!} 
                                    </div> 
                                </div> 
                                <div class="form-group col-md-6 required"> 
                                    {!! Form::label('account_id', trans_choice('general.accounts', 1), ['class' => 'control-label']) !!} 
                                    <div class="input-group"> 
                                        <div class="input-group-addon"><i class="fa fa-university"></i></div> 
                                        {!! Form::select('account_id', $accounts, setting('general.default_account'), ['class' => 'form-control', 'required' => 'required', 'placeholder' => trans('general.form.select.field', ['field' => trans_choice('general.accounts', 1)])]) !!} 
                                    </div> 
                                </div> 
                                <div class="form-group col-md-6 required"> 
                                    {!! Form::label('currency_code', trans_choice('general.currencies', 1), ['class' => 'control-label']) !!} 
                                    <div class="input-group"> 
                                        <div class="input-group-addon"><i class="fa fa-exchange"></i></div> 
                                        {!! Form::text('currency', $currencies[$account_currency_code], ['id' => 'currency', 'class' => 'form-control', 'required' => 'required', 'disabled' => 'disabled']) !!} 
                                        {!! Form::hidden('currency_code', $account_currency_code, ['id' => 'currency_code', 'class' => 'form-control', 'required' => 'required']) !!} 
                                    </div> 
                                </div> 
                                <div class="form-group col-md-12"> 
                                    {!! Form::label('description', trans('general.description'), ['class' => 'control-label']) !!} 
                                    {!! Form::textarea('description', null, ['class' => 'form-control', 'rows' => '3', 'placeholder' => trans('general.form.enter', ['field' => trans('general.description')])]) !!} 
                                </div> 
                                <div class="form-group col-md-6 required"> 
                                    {!! Form::label('payment_method', trans_choice('general.payment_methods', 1), ['class' => 'control-label']) !!} 
                                    <div class="input-group"> 
                                        <div class="input-group-addon"><i class="fa fa-folder-open-o"></i></div> 
                                        {!! Form::select('payment_method', $payment_methods, setting('general.default_payment_method'), ['class' => 'form-control', 'required' => 'required', 'placeholder' => trans('general.form.select.field', ['field' => trans_choice('general.payment_methods', 1)])]) !!} 
                                    </div> 
                                </div> 
                                <div class="form-group col-md-6"> 
                                    {!! Form::label('reference', trans('general.reference'), ['class' => 'control-label']) !!} 
                                    <div class="input-group"> 
                                        <div class="input-group-addon"><i class="fa fa-file-text-o"></i></div> 
                                        {!! Form::text('reference', null, ['class' => 'form-control', 'placeholder' => trans('general.form.enter', ['field' => trans('general.reference')])]) !!} 
                                    </div> 
                                </div> 
                                {!! Form::hidden('invoice_id', $invoice->id, ['id' => 'invoice_id', 'class' => 'form-control', 'required' => 'required']) !!} 
                            </div> 
                            <div class="modal-footer" style="text-align: left;"> 
                                <button type="button" onclick="addPayment();" class="btn btn-primary">{{ trans('general.save') }}</button> 
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('general.cancel') }}</button> 
                            </div> 
                        </div> 
                    </div> 
                 </div>`
          $('body').append(html);



          
	});
</script>
<script src="{{ asset('assets/js/form_elements.js') }}" type="text/javascript"></script>
@endpush