@extends('accounting::layouts.master')

@section('content')
<div class="container-fluid container-fixed-lg bg-white">
  <div class="panel panel-transparent">
    <div class="panel-heading">
      <div class="panel-title">Add Invoice
      </div>
    </div>
    <div class="panel-body">
      {!! Form::open(['url' => 'accounting/incomes/invoices', 'files' => true, 'role' => 'form']) !!}

      <div class="form-group col-md-6 required {{ $errors->has('customer_id') ? 'has-error' : ''}}">
        {!! Form::label('customer_id', trans_choice('general.customers', 1), ['class' => 'control-label']) !!}
        <div class="input-group">
          <div class="input-group-addon"><i class="fa fa-user"></i></div>
          {!! Form::select('customer_id', $customers, null, array_merge(['class' => 'form-control', 'placeholder' => trans('general.form.select.field', ['field' => trans_choice('general.customers', 1)])])) !!}
          <div class="input-group-btn">
            <button type="button" onclick="createCustomer();" class="btn btn-default btn-icon"><i class="fa fa-plus"></i></button>
          </div>
        </div>
        {!! $errors->first('customer_id', '<p class="help-block">:message</p>') !!}
      </div>

      {{ Form::selectGroup('currency_code', trans_choice('general.currencies', 1), 'exchange', $currencies, setting('general.default_currency')) }}

      {{ Form::textGroup('invoiced_at', trans('invoices.invoice_date'), 'calendar',['id' => 'invoiced_at', 'class' => 'form-control', 'required' => 'required', 'data-inputmask' => '\'alias\': \'yyyy/mm/dd\'', 'data-mask' => ''], Carbon\Carbon::now()->toDateString()) }}

      {{ Form::textGroup('due_at', trans('invoices.due_date'), 'calendar',['id' => 'due_at', 'class' => 'form-control', 'required' => 'required', 'data-inputmask' => '\'alias\': \'yyyy/mm/dd\'', 'data-mask' => '']) }}

      {{ Form::textGroup('invoice_number', trans('invoices.invoice_number'), 'file-text-o', ['required' => 'required'], $number) }}

      {{ Form::textGroup('order_number', trans('invoices.order_number'), 'shopping-cart', []) }}

      <div class="form-group col-md-12">
        {!! Form::label('items', trans_choice('general.items', 2), ['class' => 'control-label']) !!}
        <div class="table-responsive">
          <table class="table table-bordered" id="items">
            <thead>
              <tr style="background-color: #f9f9f9;">
                <th width="5%"  class="text-center">{{ trans('general.actions') }}</th>
                <th width="40%" class="text-left">{{ trans('general.name') }}</th>
                <th width="5%" class="text-center">{{ trans('invoices.quantity') }}</th>
                <th width="10%" class="text-right">{{ trans('invoices.price') }}</th>
                <th width="15%" class="text-right">{{ trans_choice('general.taxes', 1) }}</th>
                <th width="10%" class="text-right">{{ trans('invoices.total') }}</th>
              </tr>
            </thead>
            <tbody>
                <?php $item_row = 0; ?>
              <tr id="item-row-{{ $item_row }}">
                <td class="text-center" style="vertical-align: middle;">
                  <button type="button" onclick="$(this).tooltip('destroy'); $('#item-row-{{ $item_row }}').remove(); totalItem();" data-toggle="tooltip" title="{{ trans('general.delete') }}" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>
                </td>
                <td>
                  <div class="form-group form-group-default required typehead" id="sample-three">
                        <label>Items</label>
                        <input class="typeahead form-control sample-typehead" name="item[{{ $item_row }}][name]" type="text" placeholder="{{ trans('general.form.enter', ['field' => trans_choice('invoices.item_name', 1)]) }}" id="item-name-{{ $item_row }}" autocomplete="off">
                        <input name="item[{{ $item_row }}][item_id]" type="hidden" id="item-id-{{ $item_row }}">
                      </div>
                  {{-- <input class="form-control typeahead" required="required" placeholder="{{ trans('general.form.enter', ['field' => trans_choice('invoices.item_name', 1)]) }}" name="item[{{ $item_row }}][name]" type="text" id="item-name-{{ $item_row }}" autocomplete="off">
                  <input name="item[{{ $item_row }}][item_id]" type="hidden" id="item-id-{{ $item_row }}"> --}}
                </td>
                <td>
                  <input class="form-control text-center" required="required" name="item[{{ $item_row }}][quantity]" type="text" id="item-quantity-{{ $item_row }}">
                </td>
                <td>
                  <input class="form-control text-right" required="required" name="item[{{ $item_row }}][price]" type="text" id="item-price-{{ $item_row }}">
                </td>
                <td>
                  {!! Form::select('item[' . $item_row . '][tax_id]', $taxes, setting('general.default_tax'), ['id'=> 'item-tax-'. $item_row, 'class' => 'form-control tax-select2', 'placeholder' => trans('general.form.select.field', ['field' => trans_choice('general.taxes', 1)])]) !!}
                </td>
                <td class="text-right" style="vertical-align: middle;">
                  <span id="item-total-{{ $item_row }}">0</span>
                </td>
              </tr>
              <?php $item_row++; ?>
              <tr id="addItem">
                <td class="text-center"><button type="button" onclick="addItem();" data-toggle="tooltip" title="{{ trans('general.add') }}" class="btn btn-xs btn-primary" data-original-title="{{ trans('general.add') }}"><i class="fa fa-plus"></i></button></td>
                <td class="text-right" colspan="5"></td>
              </tr>
              <tr>
                <td class="text-right" colspan="5"><strong>{{ trans('invoices.sub_total') }}</strong></td>
                <td class="text-right"><span id="sub-total">0</span></td>
              </tr>
              <tr>
                <td class="text-right" style="vertical-align: middle;" colspan="5">
                  <a href="javascript:void(0)" id="discount-text" rel="popover">{{ trans('invoices.add_discount') }}</a>
                </td>
                <td class="text-right">
                  <span id="discount-total"></span>
                  {!! Form::hidden('discount', null, ['id' => 'discount', 'class' => 'form-control text-right']) !!}
                </td>
              </tr>
              <tr>
                <td class="text-right" colspan="5"><strong>{{ trans_choice('general.taxes', 1) }}</strong></td>
                <td class="text-right"><span id="tax-total">0</span></td>
              </tr>
              <tr>
                <td class="text-right" colspan="5"><strong>{{ trans('invoices.total') }}</strong></td>
                <td class="text-right"><span id="grand-total">0</span></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>

      {{ Form::textareaGroup('notes', trans_choice('general.notes', 2)) }}

      <div class="form-group col-md-6 required {{ $errors->has('category_id') ? 'has-error' : ''}}">
        {!! Form::label('category_id', trans_choice('general.categories', 1), ['class' => 'control-label']) !!}
        <div class="input-group">
          <div class="input-group-addon"><i class="fa fa-folder-open-o"></i></div>
          {!! Form::select('category_id', $categories, null, array_merge(['class' => 'form-control', 'placeholder' => trans('general.form.select.field', ['field' => trans_choice('general.categories', 1)])])) !!}
          <div class="input-group-btn">
            <button type="button" onclick="createCategory();" class="btn btn-default btn-icon"><i class="fa fa-plus"></i></button>
          </div>
        </div>
        {!! $errors->first('category_id', '<p class="help-block">:message</p>') !!}
      </div>

      {{ Form::recurring('create') }}

      {{ Form::fileGroup('attachment', trans('general.attachment')) }}

      {{ Form::saveButtons('incomes/invoices') }}
      {!! Form::close() !!}
    </div>
  </div>
</div>
@endsection

@include('accounting::incomes.invoices.view')

