@extends('accounting::layouts.master')

@section('navlink')
<nav class="navbar navbar-default bg-master-lighter sm-padding-10" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sub-nav">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="sub-nav">
      <div class="row">
        <div class="col-sm-4">
        	
          <ul class="nav navbar-nav navbar-center">
          	<li>
          		<div class="btn-group btn-lg">
		            <a href="{{ url('/accounting/incomes/revenues/create') }}" class="btn btn-primary">
		              <i class="fa fa-plus"></i> &nbsp;{{ trans('general.add_new') }}
		            </a>
		            <a href="{{ url('accounting/common/import/incomes/revenues') }}" class="btn btn-default">
		              <i class="fa fa-download"></i> &nbsp;{{ trans('import.import') }}
		            </a>            
		          </div>
          	</li>
          </ul>
        </div>        
      </div>
    </div>
    <!-- /.navbar-collapse -->
  </div>
  <!-- /.container-fluid -->
</nav>
@endsection

@section('content')
<div class="container-fluid container-fixed-lg bg-white">
  <!-- START PANEL -->
  <div class="panel panel-transparent">
    <div class="panel-heading">
      <div class="panel-title">Revenue
      </div>
      <div class="pull-right">
        <div class="col-xs-12">
          <input type="text" id="search-table" class="form-control pull-right" placeholder="Search">
        </div>
      </div>
      <div class="clearfix"></div>
    </div>
    <div class="panel-body">
      <table class="table table-hover demo-table-search table-responsive-block" id="tableWithInvoicesAndSearch">
        <thead>
          <tr>
            <th class="col-md-1">{{ trans('general.date') }}</th>
            <th class="col-md-1 text-right amount-space">{{ trans('general.amount') }}</th>
            <th class="col-md-3">{{ trans_choice('general.customers', 1) }}</th>
            <th class="col-md-2">{{ trans_choice('general.categories', 1) }}</th>
            <th class="col-md-3">{{ trans_choice('general.accounts', 1) }}</th>
            <th class="col-md-1">{{ trans('general.actions') }}</th>
          </tr>
        </thead>
   
                <tbody>
                @foreach($revenues as $item)
                    <tr>
                        <td><a href="{{ url('/accounting/incomes/revenues/' . $item->id . '/edit') }}">{{ $item->paid_at->toFormattedDateString() }}</a></td>
                        <td class="text-right amount-space">@money($item->amount, $item->currency_code, true)</td>
                        <td class="hidden-xs">{{ !empty($item->customer->name) ? $item->customer->name : trans('general.na') }}</td>
                        <td class="hidden-xs">{{ $item->category->name }}</td>
                        <td class="hidden-xs">{{ $item->account->name }}</td>
                        <td class="text-center">
                            {{-- @if ($item->category->id != $transfer_cat_id) --}}
                            <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" data-toggle-position="left" aria-expanded="false">
                                    <i class="fa fa-ellipsis-h"></i>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="{{ url('accounting/incomes/revenues/' . $item->id . '/edit') }}">{{ trans('general.edit') }}</a></li>
                                    @permission('create-incomes-revenues')
                                    <li class="divider"></li>
                                    <li><a href="{{ url('incomes/revenues/' . $item->id . '/duplicate') }}">{{ trans('general.duplicate') }}</a></li>
                                    @endpermission
                                    @permission('delete-incomes-revenues')
                                    <li class="divider"></li>
                                    <li>{!! Form::deleteLink($item, 'incomes/revenues') !!}</li>
                                    @endpermission
                                </ul>
                            </div>
                            {{-- @endif --}}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    
</div>
<!-- /.box -->
@endsection

@include('/layouts.admin.partials.datatables')