@extends('accounting::layouts.master')

@section('content')
<div class="container-fluid container-fixed-lg">
  <table class="table table-hover " id="clients-table">
        <thead>
        <tr>
            <th style="width:1%">
                <button class="btn"><i class="fa fa-trash-o"></i>
                </button>
            </th>
            <th style="width:29%">{{ __('Account Name') }}</th>
            <th style="width:15%">{{ __('Phone') }}</th>
            <th style="width:15%">{{ __('Email') }}</th>
            <th style="width:20%">{{ __('Account Ownwer') }}</th>
            <th style="width:10%"></th>
            <th style="width:10%"></th>
        </tr>
        </thead>
        <tbody>
            @foreach($customers as $client)
            <tr>
                <td class="v-align-middle">
                    <div class="checkbox ">
                      <input type="checkbox" value="{{ $client->id }}" id="checkbox1">
                      <label for="checkbox1"></label>
                    </div>
                </td>
                <td><a href="/crm/clients/{{$client->id}}">{{$client->name}}</a></td>
                <td>{{$client->primary_number}}</td>
                <td>{{$client->email}}</td>
                <td>{{$client->user->name}}</td>
                <td>
                     @if(Auth::user()->hasPermission('edit.client'))
                     <a href="{{ route('clients.edit', $client->id) }}" class="btn btn-success" >Edit</a>
                    @endif
                </td>
                <td>
                    @if(Auth::user()->hasPermission('delete.client'))
                    <form action="{{ route('clients.destroy', $client->id) }}" method="POST">
                        <input type="hidden" name="_method" value="DELETE">
                        <input type="submit" name="submit" value="Delete" class="btn btn-danger" onclick="return confirm('Are you sure?')">
                        @csrf
                    </form>
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection