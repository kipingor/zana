@extends('accounting::layouts.master')

@section('content')
<div class="container-fluid container-fixed-lg">
	<div class="panel panel-transparent">
		<div class="panel-heading">
			{!! Form::model($item, [
				'method' => 'PATCH',
				'url' => ['accounting/items', $item->id],
				'files' => true,
				'role' => 'form'
				]) !!}
		</div>
		<div class="panel-body">			
			
			{{ Form::textGroup('name', trans('general.name'), 'id-card-o') }}

      {{ Form::textGroup('sku', trans('items.sku'), 'key') }}

            {{ Form::textareaGroup('description', trans('general.description')) }}

            {{ Form::textGroup('sale_price', trans('items.sales_price'), 'money') }}

            {{ Form::textGroup('purchase_price', trans('items.purchase_price'), 'money') }}

            {{ Form::textGroup('quantity', trans_choice('items.quantities', 1), 'cubes', ['required' => 'required'], '1') }}

            {{ Form::selectGroup('tax_id', trans_choice('general.taxes', 1), 'percent', $taxes, [], []) }}
            <!-- setting('general.default_tax') -->
						<div class="col-md-6">
	            <div class="form-group form-group-default input-group required {{ $errors->has('category_id') ? 'has-error' : ''}}">
	            	<span class="input-group-addon primary"><i class="fa fa-folder-open-o"></i></span>
	              {!! Form::label('category_id', trans_choice('general.categories', 1), ['class' => 'control-label']) !!}
	                
	                {!! Form::select('category_id', $categories, null, array_merge(['class' => 'form-control', 'placeholder' => trans('general.form.select.field', ['field' => trans_choice('general.categories', 1)])])) !!}
	                    <span class="input-group-btn">
	                        <button type="button" onclick="createCategory();" class="btn btn-primary btn-icon"><i class="fa fa-plus"></i></button>
	                    </span>
	                {!! $errors->first('category_id', '<p class="help-block">:message</p>') !!}
	            </div>
	          </div>

            {{ Form::fileGroup('picture', trans_choice('general.pictures', 1)) }}

            {{ Form::radioGroup('enabled', trans('general.enabled')) }}
        </div>
        <!-- /.box-body -->

        <div class="panel-footer">
            {{ Form::saveButtons('items/items') }}
        </div>

			{!! Form::close() !!}
		</div>
	</div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
  var text_yes = '{{ trans('general.yes') }}';
  var text_no = '{{ trans('general.no') }}';

  $(document).ready(function(){
    $("#tax_id").select2({
      placeholder: "{{ trans('general.form.select.field', ['field' => trans_choice('general.taxes', 1)]) }}"
    });

    $("#category_id").select2({
      placeholder: "{{ trans('general.form.select.field', ['field' => trans_choice('general.categories', 1)]) }}"
    });

    $('#picture').fancyfile({
      text  : '{{ trans('general.form.select.file') }}',
      style : 'btn-default',
      @if($item->picture)
        placeholder : '<?php echo $item->picture->basename; ?>'
      @else
        placeholder : '{{ trans('general.form.no_file_selected') }}'
      @endif
    });

      @if($item->picture)
          picture_html  = '<span class="picture">';
          picture_html += '    <a href="{{ url('public/img/' . $item->picture->id . '/download') }}">';
          picture_html += '        <span id="download-picture" class="text-primary">';
          picture_html += '            <i class="fa fa-file-{{ $item->picture->aggregate_type }}-o"></i> {{ $item->picture->basename }}';
          picture_html += '        </span>';
          picture_html += '    </a>';
          picture_html += '    {!! Form::open(['id' => 'picture-' . $item->picture->id, 'method' => 'DELETE', 'url' => [url('public/img//' . $item->picture->id)], 'style' => 'display:inline']) !!}';
          picture_html += '    <a id="remove-picture" href="javascript:void();">';
          picture_html += '        <span class="text-danger"><i class="fa fa fa-times"></i></span>';
          picture_html += '    </a>';
          picture_html += '    {!! Form::close() !!}';
          picture_html += '</span>';

          $('.fancy-file .fake-file').append(picture_html);

          $(document).on('click', '#remove-picture', function (e) {
              confirmDelete("#picture-{!! $item->picture->id !!}", "{!! trans('general.attachment') !!}", "{!! trans('general.delete_confirm', ['name' => '<strong>' . $item->picture->basename . '</strong>', 'type' => strtolower(trans('general.attachment'))]) !!}", "{!! trans('general.cancel') !!}", "{!! trans('general.delete')  !!}");
          });
      @endif
    });
</script>
@endpush