@extends('accounting::layouts.master')

@section('content')
<div class="container-fluid container-fixed-lg">
	<div class="panel panel-transparent">
		<div class="panel-heading">
			{!! Form::open(['url' => 'accounting/items', 'files' => true, 'role' => 'form']) !!}
		</div>
		<div class="panel-body">			
			
			{{ Form::textGroup('name', trans('general.name'), 'id-card-o') }}

      {{ Form::textGroup('sku', trans('items.sku'), 'key') }}

            {{ Form::textareaGroup('description', trans('general.description')) }}

            {{ Form::textGroup('sale_price', trans('items.sales_price'), 'money') }}

            {{ Form::textGroup('purchase_price', trans('items.purchase_price'), 'money') }}

            {{ Form::textGroup('quantity', trans_choice('items.quantities', 1), 'cubes', ['required' => 'required'], '1') }}

            {{ Form::selectGroup('tax_id', trans_choice('general.taxes', 1), 'percent', $taxes, [], []) }}
            <!-- setting('general.default_tax') -->
						<div class="col-md-6">
	            <div class="form-group form-group-default input-group required {{ $errors->has('category_id') ? 'has-error' : ''}}">
	            	<span class="input-group-addon primary"><i class="fa fa-folder-open-o"></i></span>
	              {!! Form::label('category_id', trans_choice('general.categories', 1), ['class' => 'control-label']) !!}
	                
	                {!! Form::select('category_id', $categories, null, array_merge(['class' => 'form-control', 'placeholder' => trans('general.form.select.field', ['field' => trans_choice('general.categories', 1)])])) !!}
	                    <span class="input-group-btn">
	                        <button type="button" onclick="createCategory();" class="btn btn-primary btn-icon"><i class="fa fa-plus"></i></button>
	                    </span>
	                {!! $errors->first('category_id', '<p class="help-block">:message</p>') !!}
	            </div>
	          </div>

            {{ Form::fileGroup('picture', trans_choice('general.pictures', 1)) }}

            {{ Form::radioGroup('enabled', trans('general.enabled')) }}
        </div>
        <!-- /.box-body -->

        <div class="panel-footer">
            {{ Form::saveButtons('items/items') }}
        </div>

			{!! Form::close() !!}
		</div>
	</div>
</div>
@endsection

@include('accounting::items.scripts')