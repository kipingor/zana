@extends('accounting::layouts.master')

@section('action_menu')
<li>
	<a href="{{ url('accounting/items/create') }}"><i class="fa fa-plus"></i> &nbsp;{{ __('Add New') }}</a>
</li>
<li>
	<a href="{{ url('common/import/items/items') }}"><i class="fa fa-download"></i> &nbsp;{{ __('Import') }}</a>
</li>
@endsection


@section('content')
<div class="container-fluid container-fixed-lg bg-white">
  <!-- START PANEL -->
  <div class="panel panel-transparent">
    <div class="panel-heading">
      <div class="panel-title">{{ trans_choice('general.items', 2) }}
      </div>
      <div class="pull-right">
        <div class="col-xs-12">          
          <input type="text" id="search-table" class="form-control pull-right" placeholder="Search">          
        </div>
      </div>
      <div class="clearfix"></div>
    </div>
    <div class="panel-body">
      <table class="table table-hover demo-table-search table-responsive-block" id="tableWithSearch">
        <thead>
          <tr>
            <th style="width:8%">Pic</th>
            <th style="width:24%">Name</th>
            <th style="width:10%">Category</th>
            <th style="width:10%">Item Quantities</th>
            <th style="width:16%">Sales Price</th>
            <th style="width:16%">Purchase Price</th>
            <th style="width:8%">Status</th>
            <th style="width:8%">Action</th>
          </tr>
        </thead>
        <tbody>
          @foreach($items as $item)
          <tr>
              <td class="hidden-xs"><img src="{{ $item->picture ? $item->getMedia('picture')->first()->getUrl() : asset('img/akaunting-logo-green.png') }}" class="img-thumbnail" width="50" alt="{{ $item->name }}"></td>
              <td><a href="{{ url('accounting/items/' . $item->id . '/edit') }}">{{ $item->name }}</a></td>
              <td class="hidden-xs">{{ $item->category ? $item->category->name : trans('general.na') }}</td>
              <td class="hidden-xs">{{ $item->quantity }}</td>
              <td class="text-right amount-space">{{ money($item->sale_price, setting('general.default_currency'), true) }}</td>
              <td class="hidden-xs text-right amount-space">{{ money($item->purchase_price, setting('general.default_currency'), true) }}</td>
              <td class="hidden-xs">
                  @if ($item->enabled)
                      <span class="label label-success">{{ trans('general.enabled') }}</span>
                  @else
                      <span class="label label-danger">{{ trans('general.disabled') }}</span>
                  @endif
              </td>
              <td class="text-center">
                <div class="">
                  <div class="btn-group dropdown-default">
                    <button id="btnGroupVerticalDrop1" type="button" class="btn btn-default dropdown-toggle text-center" data-toggle="dropdown">
                      <i class="fa fa-ellipsis-h"></i>
                      <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="btnGroupVerticalDrop1">
                      <li><a href="{{ url('/accounting/items/' . $item->id . '/edit') }}">{{ trans('general.edit') }}</a></li>
                          <li class="divider"></li>
                          @if(Auth::check() && Auth::user()->hasPermission('create.item'))
                          <li><a href="{{ url('/accounting/items/' . $item->id . '/duplicate') }}">{{ trans('general.duplicate') }}</a></li>
                          <li class="divider"></li>
                          @endif
                          @if(Auth::check() && Auth::user()->hasPermission('delete.item'))
                          <li>{!! Form::deleteLink($item, 'accounting/items') !!}</li>
                          @endif
                    </ul>
                  </div>
                </div>  
              </td>
          </tr>
          @endforeach         
        </tbody>
      </table>
    </div>
  </div>
  <!-- END PANEL -->
</div>
@endsection