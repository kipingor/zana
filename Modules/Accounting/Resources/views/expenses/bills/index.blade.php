@extends('accounting::layouts.master')

@section('action_menu')
<li>
  <a href="{{ url('accounting/expenses/bills/create') }}"><i class="fa fa-plus"></i> &nbsp;{{ __('Add New') }}</a>
</li>
<li>
  <a href="{{ url('accounting/common/import/expenses/bills') }}"><i class="fa fa-download"></i> &nbsp;{{ __('Import') }}</a>
</li>
@endsection

@section('content')
<div class="container-fluid container-fixed-lg bg-white">
  <!-- START PANEL -->
  <div class="panel panel-transparent">
    <div class="panel-heading">
      <div class="panel-title">{{ trans('general.show') }}:
      </div>
      <div class="pull-right">
        <div class="col-xs-12">          
          <input type="text" id="search-table" class="form-control pull-right" placeholder="Search">          
        </div>
      </div>
      <div class="clearfix"></div>
    </div>
    <div class="panel-body">
      <table class="table table-hover demo-table-search table-responsive-block" id="tableWithSearch">
        <thead>
          <tr>
            <th class="col-md-2">{{ trans_choice('general.numbers', 1) }}</th>
            <th class="col-md-2">{{ trans_choice('general.vendors', 1) }}</th>
            <th class="col-md-2 text-right amount-space">{{ trans('general.amount') }}</th>
            <th class="col-md-2">{{ trans('bills.bill_date') }}</th>
            <th class="col-md-2">{{ trans('bills.due_date') }}</th>
            <th class="col-md-1">{{ trans_choice('general.statuses', 1) }}</th>
            <th class="col-md-1 text-center">{{ trans('general.actions') }}</th>
          </tr>
        </thead>
        <tbody>
          @foreach($bills as $item)
          <tr>
            <td><a href="{{ url('accounting/expenses/bills/' . $item->id . ' ') }}">{{ $item->bill_number }}</a></td>
            <td>{{ $item->vendor_name }}</td>
            <td class="text-right amount-space">@money($item->amount, $item->currency_code, true)</td>
            <td>{{ $item->billed_at->toFormattedDateString() }}</td>
            <td>{{ $item->due_at->toFormattedDateString() }}</td>
            <td><span class="label">{{ $item->bill_status_code }}</span></td>
            <td class="text-center">
              <div class="btn-group">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" data-toggle-position="left" aria-expanded="false">
                  <i class="fa fa-ellipsis-h"></i>
                </button>
                <ul class="dropdown-menu dropdown-menu-right">
                  <li><a href="{{ url('accounting/expenses/bills/' . $item->id) }}">{{ trans('general.show') }}</a></li>
                  <li><a href="{{ url('accounting/expenses/bills/' . $item->id . '/edit') }}">{{ trans('general.edit') }}</a></li>
                  <li class="divider"></li>
                  @if(Auth::check() && Auth::user()->hasPermission('create.expenses.bills'))
                  <li><a href="{{ url('accounting/expenses/bills/' . $item->id . '/duplicate') }}">{{ trans('general.duplicate') }}</a></li>
                  <li class="divider"></li>
                  @endif
                  @if(Auth::check() && Auth::user()->hasPermission('delete.expenses.bills'))
                  <li>{!! Form::deleteLink($item, 'accounting/expenses/bills') !!}</li>
                  @endif
                </ul>
              </div>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
  <!-- END PANEL -->
</div>
@endsection