@include('/layouts.admin.view')
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Accounting') }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
    
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />
    @stack('styles')
    <!--[if lte IE 9]>
    <link href="assets/plugins/codrops-dialogFx/dialog.ie.css" rel="stylesheet" type="text/css" media="screen" />
    <![endif]-->
  </head>
  <body class="fixed-header horizontal-menu">
    @include('/layouts.admin.nav')
    
    <!-- END SIDEBPANEL-->
    <!-- START PAGE-CONTAINER -->
    <div class="page-container ">
     @include('/layouts.admin.header')
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper @yield('page-content-wrapper-class')">
        <!-- START PAGE CONTENT -->
        <div class="content @yield('content-class')">
          @yield('navlink')
          {{-- @include('/layouts.admin.bar') --}}
          <!-- START JUMBOTRON -->
          @yield('jumbotron')
          <!-- END JUMBOTRON -->
          <!-- START CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg @yield('bg-color')">
            <!-- BEGIN PlACE PAGE CONTENT HERE -->
            @yield('content')
            <!-- END PLACE PAGE CONTENT HERE -->
          </div>
          <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->
        <!-- START COPYRIGHT -->
        <!-- START CONTAINER FLUID -->
        <!-- START CONTAINER FLUID -->
        @include('/layouts.admin.footer')
        
      </div>
      <!-- END PAGE CONTENT WRAPPER -->
    </div>
    <!-- END PAGE CONTAINER -->
    @include('/layouts.admin.search')
    @include('/layouts.admin.quickview')
    @include('/layouts.admin.errors')
    <!-- BEGIN VENDOR JS -->
    
    @stack('scripts')
    
  </body>
</html>