<!-- BEGIN SIDEBPANEL-->
<nav class="page-sidebar" data-pages="sidebar">
  <!-- BEGIN SIDEBAR MENU TOP TRAY CONTENT-->
  @include('layouts.admin.partials.sidebartop')
  <!-- START SIDEBAR MENU -->
  <div class="sidebar-menu">
    <!-- BEGIN SIDEBAR MENU ITEMS-->
    <ul class="menu-items">
      <li class="m-t-30 ">
        <a href="{{ url('/home') }}" class="detailed">
          <span class="title">Back Home</span>
          <span class="details">12 New Updates</span>
        </a>
        <span class="icon-thumbnail"><i class="pg-home"></i></span>
      </li>
      <li class="open active">
        <a href="javascript:;"><span class="title">Accounting</span>
          <span class=" arrow open active"></span></a>
        <span class="bg-success icon-thumbnail"><i class="fas fa-calculator"></i></span>
        <ul class="sub-menu">
          <li class="">
            <a href="{{ url('/accounting') }}">Dashboard</a>
            <span class="icon-thumbnail" style="color: tomato"><i class="fab fa-dashcube"></i></span>
          </li> 
          <li class="">
            <a href="{{ url('/accounting/items') }}">Items</a>
            <span class="icon-thumbnail">I</span>
          </li>            
        </ul>
      </li>
      <li class="">
        <a href="javascript:;"><span class="title">Expenses</span>
          <span class="arrow"></span></a>
        <span class="bg-danger-darker icon-thumbnail"><i class="fas fa-money-check-alt"></i></span>
        <ul class="sub-menu">              
          <li>
            <a href="/accounting/expenses/bills"><span class="title">Bills</span></a>
            <span class="icon-thumbnail"><a href="/accounting/expenses/bills/create" style="color: forestgreen"><i class="fas fa-plus-square"></i></a></span> 
          </li>
          <li>
            <a href="/accounting/expenses/vendors"><span class="title">Vendors</span></a>
            <span class="icon-thumbnail"><a href="/accounting/expenses/vendors/create" style="color: dodgerblue"><i class="fas fa-plus-square"></i></a></span>             
          </li>
          <li>
            <a href="/accounting/expenses/payments"><span class="title">Payments</span></a>
            <span class="icon-thumbnail"><a href="/accounting/expenses/payments/create" style="color: plum"><i class="fas fa-plus-square"></i></a></span>             
          </li>
        </ul>
      </li>
      <li class="">
        <a href="javascript:;"><span class="title">Incomes</span>
          <span class="arrow"></span></a>
        <span class="bg-success-darker icon-thumbnail"><i class="fas fa-wallet"></i></span>
        <ul class="sub-menu">              
          <li>
            <a href="/crm/clients"><span class="title">Customers</span></a>
            <span class="icon-thumbnail"><a href="/crm/clients/create" style="color: forestgreen"><i class="fas fa-plus-square"></i></a></span> 
          </li>
          <li>
            <a href="/accounting/incomes/invoices"><span class="title">Invoices</span></a>
            <span class="icon-thumbnail"><a href="/accounting/incomes/invoices/create" style="color: dodgerblue"><i class="fas fa-plus-square"></i></a></span>             
          </li>
          <li>
            <a href="/accounting/incomes/revenues"><span class="title">Revenues</span></a>
            <span class="icon-thumbnail"><a href="/accounting/incomes/revenues/create" style="color: plum"><i class="fas fa-plus-square"></i></a></span>             
          </li>
        </ul>
      </li>
      <li class="">
        <a href="http://pages.revox.io/dashboard/2.2.0/docs/" target="_blank"><span class="title">Docs</span></a>
        <span class="bg-complete icon-thumbnail"><i class="pg-note"></i></span>
      </li>
      <li class="">
        <a href="http://changelog.pages.revox.io/" target="_blank"><span class="title">Changelog</span></a>
        <span class="icon-thumbnail">Cl</span>
      </li>
    </ul>
    <div class="clearfix"></div>
  </div>
  <!-- END SIDEBAR MENU -->
</nav>
<!-- END SIDEBAR -->