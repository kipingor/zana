@push('scripts')
<script type="text/javascript">
	function createCustomer() {
        $('#modal-create-customer').remove();

        modal  = `
        <div class="modal fade" id="modal-create-customer" style="display: none;">
	        <div class="modal-dialog  modal-lg">
		        <div class="modal-content">
			        <div class="modal-header">
			        	<h4 class="modal-title">{{ trans('general.title.new', ['type' => trans_choice('general.customers', 1)]) }}</h4>
			        </div>
			        <div class="modal-body">
			        	{!! Form::open(['id' => 'form-create-customer', 'role' => 'form']) !!}
			        	<div class="row">
			        		<div class="form-group col-md-6 required">
			        			<label for="name" class="control-label">{{ trans('general.name') }}</label>
			       			 	<div class="input-group">
			        				<div class="input-group-addon"><i class="fa fa-id-card-o"></i></div>
			        				<input class="form-control" placeholder="{{ trans('general.name') }}" required="required" name="name" type="text" id="name">
			        				</div>
			        			</div>
			        			<div class="form-group col-md-6">
			        				<label for="email" class="control-label">{{ trans('general.email') }}</label>
			        				<div class="input-group">
			        					<div class="input-group-addon"><i class="fa fa-envelope"></i></div>
			        					<input class="form-control" placeholder="{{ trans('general.email') }}" required="required" name="email" type="text" id="email">
			        				</div>
			        			</div>
			        			<div class="form-group col-md-6">
			        				<label for="tax_number" class="control-label">{{ trans('general.tax_number') }}</label>
			        				<div class="input-group">
			        					<div class="input-group-addon"><i class="fa fa-percent"></i></div>
			        					<input class="form-control" placeholder="{{ trans('general.tax_number') }}" name="tax_number" type="text" id="tax_number">
			        				</div>
			        			</div>
			        			<div class="form-group col-md-6 required">
			        				<label for="email" class="control-label">{{ trans_choice('general.currencies', 1) }}</label>
			        				<div class="input-group">
			        					<div class="input-group-addon"><i class="fa fa-exchange"></i></div>
							        	<select class="form-control" required="required" id="currency_code" name="currency_code">
									        <option value="">{{ trans('general.form.select.field', ['field' => trans_choice('general.currencies', 1)]) }}</option>
									        @foreach($currencies as $currency_code => $currency_name)
									        <option value="{{ $currency_code }}" {{ (setting('general.default_currency') == $currency_code) ? 'selected' : '' }}>{{ $currency_name }}</option>
							       			@endforeach
							        	</select>
			        				</div>
			        			</div>
			        			<div class="form-group col-md-12">
			        				<label for="address" class="control-label">{{ trans('general.address') }}</label>
			        				<textarea class="form-control" placeholder="{{ trans('general.address') }}" rows="3" name="address" cols="50" id="address"></textarea>
			        			</div>
			        			{!! Form::hidden('enabled', '1', []) !!}
			        		</div>
			        		{!! Form::close() !!}
			        </div>
			        <div class="modal-footer">
			        	<div class="pull-left">
			        		{!! Form::button('<span class="fa fa-save"></span> &nbsp;' . trans('general.save'), ['type' => 'button', 'id' =>'button-create-customer', 'class' => 'btn btn-success']) !!}
			        		<button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-times-circle"></span> &nbsp;{{ trans('general.cancel') }}</button>
			        	</div>
			        </div>
		        </div>
	        </div>
        </div>`;

        $('body').append(modal);

        $("#modal-create-customer #currency_code").select2({
            placeholder: "{{ trans('general.form.select.field', ['field' => trans_choice('general.currencies', 1)]) }}"
        });

        $('#modal-create-customer').modal('show');
    }

    $(document).on('click', '#button-create-customer', function (e) {
        $('#modal-create-customer .modal-header').before('<span id="span-loading" style="position: absolute; height: 100%; width: 100%; z-index: 99; background: #6da252; opacity: 0.4;"><i class="fa fa-spinner fa-spin" style="font-size: 16em !important;margin-left: 35%;margin-top: 8%;"></i></span>');

        $.ajax({
            url: '{{ url("accounting/incomes/customers/customer") }}',
            type: 'POST',
            dataType: 'JSON',
            data: $("#form-create-customer").serialize(),
            beforeSend: function () {
                $(".form-group").removeClass("has-error");
                $(".help-block").remove();
            },
            success: function(data) {
                $('#span-loading').remove();

                $('#modal-create-customer').modal('hide');

                $("#customer_id").append('<option value="' + data.id + '" selected="selected">' + data.name + '</option>');
                $("#customer_id").select2('refresh');
            },
            error: function(error, textStatus, errorThrown) {
                $('#span-loading').remove();

                if (error.responseJSON.name) {
                    $("input[name='name']").parent().parent().addClass('has-error');
                    $("input[name='name']").parent().after('<p class="help-block">' + error.responseJSON.name + '</p>');
                }

                if (error.responseJSON.email) {
                    $("input[name='email']").parent().parent().addClass('has-error');
                    $("input[name='email']").parent().after('<p class="help-block">' + error.responseJSON.email + '</p>');
                }

                if (error.responseJSON.currency_code) {
                    $("select[name='currency_code']").parent().parent().addClass('has-error');
                    $("select[name='currency_code']").parent().after('<p class="help-block">' + error.responseJSON.currency_code + '</p>');
                }
            }
        });
    });
</script>
@endpush