@push('scripts')
<script type="text/javascript">
function createCategory() {
  $('#modal-create-category').remove();

  modal  = `<div class="modal fade" id="modal-create-category" style="display: none;">
              <div class="modal-dialog  modal-lg">
                <div class="modal-content">
                  <div class="modal-header">
                    <h4 class="modal-title">{{ trans('general.title.new', ['type' => trans_choice('general.categories', 1)]) }}</h4>
                  </div>
                  <div class="modal-body">
                    {!! Form::open(['id' => 'form-create-category', 'role' => 'form']) !!}
                    <div class="row">
                      <div class="form-group col-md-6 required">
                        <label for="name" class="control-label">{{ trans('general.name') }}</label>
                        <div class="input-group">
                          <div class="input-group-addon"><i class="fa fa-id-card-o"></i></div>
                          <input class="form-control" placeholder="{{ trans('general.name') }}" required="required" name="name" type="text" id="name">
                        </div>
                      </div>
                      <div class="form-group col-md-6 required">
                        <label for="color" class="control-label">{{ trans('general.color') }}</label>
                        <div  id="category-color-picker" class="input-group colorpicker-component">
                          <div class="input-group-addon"><i></i></div>
                          <input class="form-control" value="#00a65a" placeholder="{{ trans('general.color') }}" required="required" name="color" type="text" id="color">
                        </div>
                      </div>
                      {!! Form::hidden('type', 'income', []) !!}
                      {!! Form::hidden('enabled', '1', []) !!}
                    </div>
                    {!! Form::close() !!}
                  </div>
                  <div class="modal-footer">
                    <div class="pull-left">
                      {!! Form::button('<span class="fa fa-save"></span> &nbsp;' . trans('general.save'), ['type' => 'button', 'id' =>'button-create-category', 'class' => 'btn btn-success']) !!}
                      <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-times-circle"></span> &nbsp;{{ trans('general.cancel') }}</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>`;

  $('body').append(modal);

  $('#category-color-picker').colorpicker();

  $('#modal-create-category').modal('show');
}

$(document).on('click', '#button-create-category', function (e) {
  $('#modal-create-category .modal-header').before('<span id="span-loading" style="position: absolute; height: 100%; width: 100%; z-index: 99; background: #6da252; opacity: 0.4;"><i class="fa fa-spinner fa-spin" style="font-size: 10em !important;margin-left: 35%;margin-top: 8%;"></i></span>');

  $.ajax({
      url: '{{ url("accounting/categories/category") }}',
      type: 'POST',
      dataType: 'JSON',
      data: $("#form-create-category").serialize(),
      beforeSend: function () {
          $(".form-group").removeClass("has-error");
          $(".help-block").remove();
      },
      success: function(data) {
        $('#span-loading').remove();

        $('#modal-create-category').modal('hide');

        $("#category_id").append('<option value="' + data.id + '" selected="selected">' + data.name + '</option>');
        $("#category_id").select2('refresh');
      },
      error: function(error, textStatus, errorThrown) {
        $('#span-loading').remove();

        if (error.responseJSON.name) {
          $("input[name='name']").parent().parent().addClass('has-error');
          $("input[name='name']").parent().after('<p class="help-block">' + error.responseJSON.name + '</p>');
        }

        if (error.responseJSON.color) {
          $("input[name='color']").parent().parent().addClass('has-error');
          $("input[name='color']").parent().after('<p class="help-block">' + error.responseJSON.color + '</p>');
        }
    }
  });
});
</script>

@endpush