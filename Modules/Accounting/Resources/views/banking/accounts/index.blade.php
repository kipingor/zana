@extends('accounting::layouts.master')
@section('navlink')
<nav class="navbar navbar-default bg-master-lighter sm-padding-10" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sub-nav">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="sub-nav">
      <div class="row">
        <div class="col-sm-4">
          
      		<a href="{{ url('/accounting/banking/accounts/create') }}" class="btn btn-primary btn-cons m-b-10" type="button"><i class="fas fa-plus"></i> <span class="bold">&nbsp;{{ trans('general.add_new') }}</span>
          </a>        
          	
        </div>        
      </div>
    </div>
    <!-- /.navbar-collapse -->
  </div>
  <!-- /.container-fluid -->
</nav>
@endsection

@section('content')
<div class="container-fluid container-fixed-lg bg-white">
  <!-- START PANEL -->
  <div class="panel panel-transparent">
    <div class="panel-heading">
      <div class="panel-title">Accounts
      </div>
      <div class="pull-right">
        <div class="col-xs-12">
          <input type="text" id="search-table" class="form-control pull-right" placeholder="Search">
        </div>
      </div>
      <div class="clearfix"></div>
    </div>
    <div class="panel-body">
      <table class="table table-hover demo-table-search table-responsive-block" id="tableWithInvoicesAndSearch">
        <thead>
          <tr>
            <th class="col-md-3">{{ trans('general.name') }}</th>
            <th class="col-md-3">{{ trans('accounts.number') }}</th>
            <th class="col-md-2">{{ trans('accounts.current_balance') }}</th>
            <th class="col-md-2">{{ trans_choice('general.statuses', 1) }}</th>
            <th class="col-md-2">{{ trans('general.actions') }}</th>
          </tr>
        </thead>
   
                <tbody>
                @foreach($accounts as $item)
                    <tr>
                        <td><a href="{{ url('banking/accounts/' . $item->id . '/edit') }}">{{ $item->name }}</a></td>
                        <td class="hidden-xs">{{ $item->number }}</td>
                        <td class="text-right amount-space">@money($item->balance, $item->currency_code, true)</td>
                        <td class="hidden-xs">
                            @if ($item->enabled)
                                <span class="label label-success">{{ trans('general.enabled') }}</span>
                            @else
                                <span class="label label-danger">{{ trans('general.disabled') }}</span>
                            @endif
                        </td>
                        <td class="text-center">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" data-toggle-position="left" aria-expanded="false">
                                    <i class="fa fa-ellipsis-h"></i>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="{{ url('/accounting/banking/accounts/' . $item->id . '/edit') }}">{{ trans('general.edit') }}</a></li>
                                    {{-- @permission('delete-banking-accounts') --}}
                                    <li>{!! Form::deleteLink($item, '/accounting/banking/accounts') !!}</li>
                                    {{-- @endpermission --}}
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    
</div>
<!-- /.box -->
@endsection

@include('/layouts.admin.partials.datatables')

@push('scripts')
<script type="text/javascript">
  function confirmDelete(form_id, title, message, button_cancel, button_delete) {
    $('#confirm-modal').remove();

    let delete_modal  = `
    <div class="modal fade" id="confirm-modal" tabindex="-1" role="dialog" aria-labelledby="confirmModalLabel" aria-hidden="true">
       <div class="modal-dialog">
           <div class="modal-content">
               <div class="modal-header">
                   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                   <h4 class="modal-title" id="confirmModalLabel">${title}</h4>
               </div>
               <div class="modal-body">
                   <p> ${message}</p>
                   <p></p>
               </div>';
               <div class="modal-footer">
                   <div class="pull-left">
                       <button type="button" class="btn btn-danger" onclick="$('${form_id}').submit();">${button_delete}</button>
                       <button type="button" class="btn btn-default" data-dismiss="modal">${button_cancel} </button>
                   </div>
               </div>
           </div>
       </div>
     </div>`

    $('body').append(delete_modal);

    $('#confirm-modal').modal('show');
}
</script>
@endpush