<?php

namespace Modules\Accounting\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Auth;
use Modules\Accounting\Entities\Invoice;
use Illuminate\Support\Facades\URL;


class InvoiceActionNotification extends Notification
{
    private $invoice;

    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($invoice)
    {
        $this->invoice = $invoice;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail','database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $message = (new MailMessage)
            ->subject('Invoice')
            ->greeting(trans('invoices.notification.greeting', ['customer' => $this->invoice->customer_name]))
            ->line('Thanks for your business.')
            ->line(trans('invoices.notification.message', ['amount' => money($this->invoice->amount, $this->invoice->currency_code, true), 'customer' => $this->invoice->customer_name, 'invoice' => $this->invoice->invoice_number]))
            ->line("Here's an overview of the invoice for your reference.")
            ->line('Invoice Overview:')
            ->line('Invoice # : '.$this->invoice->invoice_number)
            ->line('Date : '.$this->invoice->invoiced_at)
            ->line('Amount : '.money($this->invoice->amount, $this->invoice->currency_code, true))
            ->action(trans('invoices.notification.button'), URL::signedRoute('customer.invoice', ['invoice' => $this->invoice->id]))
             ->line('Thank you for using Zana!');;

        // Override per company as Laravel doesn't read config
        $message->from(config('mail.from.address'), config('mail.from.name'));

        // Attach the PDF file if available
        if (isset($this->invoice->pdf_path)) {
            $message->attach($this->invoice->pdf_path, [
                'mime' => 'application/pdf',
            ]);
        }

        return $message;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        
            $text = __(':invoice was created for :customer', [
            'invoice' => $this->invoice->invoice_number,
            'customer' => $this->invoice->customer_name
            ]);
            return [
            'assigned_user' => Auth()->user()->id, //Assigned user ID
            'created_user' => Auth()->user()->id,
            'creator' => Auth()->user()->name,
            'title' => $this->invoice->invoice_number,
            'message' => $text,
            'type' => Invoice::class,
            'type_id' =>  $this->invoice->id,
            'url' => url('accounting/incomes/invoices/' . $this->invoice->id),
            'action' => 'New Invoice Created'
        ];
    }
}
