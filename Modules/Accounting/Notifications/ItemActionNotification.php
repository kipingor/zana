<?php

namespace Modules\Accounting\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Auth;
use Modules\Accounting\Entities\Item;

class ItemActionNotification extends Notification
{
    private $item;

    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($item)
    {
        $this->item = $item;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail','database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $message = (new MailMessage)
            ->line(trans('items.notification.message', ['name' => $this->item->name]))
            ->action(trans('items.notification.button'), url('accounting/items', $this->item->id, true));

        return $message;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $text = __(':item was created with id :id', [
            'item' => $this->item->name,
            'id' => $this->item->id
            ]);
            return [
            'assigned_user' => Auth()->user()->id, //Assigned user ID
            'created_user' => Auth()->user()->id,
            'creator' => Auth()->user()->name,
            'title' => $this->item->name,
            'message' => $text,
            'type' => Item::class,
            'type_id' =>  $this->item->id,
            'url' => url('accounting/item/' . $this->item->id),
            'action' => $this->action
        ];
    }
}
