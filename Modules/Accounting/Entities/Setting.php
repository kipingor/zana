<?php

namespace Modules\Accounting\Entities;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
	protected $table = 'accounting_settings';

	public $timestamps = false;

  protected $fillable = ['key', 'value'];

  public static function all($code = 'general')
  {
      return static::where('key', 'like', $code . '.%')->get();
  }
}
