<?php

namespace Modules\Accounting\Entities;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class Account extends Model
{
	use Eloquence;
  
  /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['balance'];

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'number', 'currency_code', 'opening_balance', 'bank_name', 'bank_phone', 'bank_address', 'enabled'];

    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_code', 'code');
    }

    public function invoice_payments()
    {
        return $this->hasMany(InvoicePayment::Class);
    }

    public function revenues()
    {
        return $this->hasMany(Revenue::class);
    }

    public function bill_payments()
    {
        return $this->hasMany(BillPayment::class);
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    /**
     * Convert opening balance to double.
     *
     * @param  string  $value
     * @return void
     */
    public function setOpeningBalanceAttribute($value)
    {
        $this->attributes['opening_balance'] = (double) $value;
    }

    /**
     * Get the current balance.
     *
     * @return string
     */
    public function getBalanceAttribute()
    {
        // Opening Balance
        $total = $this->opening_balance;

        // Sum invoices
        foreach ($this->invoice_payments as $item) {
            $total += $item->amount;
        }

        // Sum revenues
        foreach ($this->revenues as $item) {
            $total += $item->amount;
        }

        // Subtract bills
        foreach ($this->bill_payments as $item) {
            $total -= $item->amount;
        }

        // Subtract payments
        foreach ($this->payments as $item) {
            $total -= $item->amount;
        }

        return $total;
    }
}
