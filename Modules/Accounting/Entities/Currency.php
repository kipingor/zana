<?php

namespace Modules\Accounting\Entities;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    protected $fillable = ['name', 'code', 'rate', 'enabled', 'precision', 'symbol', 'symbol_first', 'decimal_mark', 'thousands_separator'];

    /**
     * Sortable columns.
     *
     * @var array
     */
    public $sortable = ['name', 'code', 'rate', 'enabled'];

    public function accounts()
    {
        return $this->hasMany(Account::class, 'currency_code', 'code');
    }

    public function customers()
    {
        return $this->hasMany(Customer::class, 'currency_code', 'code');
    }

    public function invoices()
    {
        return $this->hasMany(Invoice::class, 'currency_code', 'code');
    }

    public function invoice_payments()
    {
        return $this->hasMany(InvoicePayment::class, 'currency_code', 'code');
    }

    public function revenues()
    {
        return $this->hasMany(Revenue::class, 'currency_code', 'code');
    }

    public function bills()
    {
        return $this->hasMany(Bill::class, 'currency_code', 'code');
    }

    public function bill_payments()
    {
        return $this->hasMany(BillPayment::class, 'currency_code', 'code');
    }

    public function payments()
    {
        return $this->hasMany(Payment::class, 'currency_code', 'code');
    }

    /**
     * Convert rate to double.
     *
     * @param  string  $value
     * @return void
     */
    public function setRateAttribute($value)
    {
        $this->attributes['rate'] = (double) $value;
    }
}
