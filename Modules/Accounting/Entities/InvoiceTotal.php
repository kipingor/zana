<?php

namespace Modules\Accounting\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Accounting\Entities\Tax;
use Modules\Accounting\Traits\DateTime;
use Modules\Accounting\Entities\Invoice;

class InvoiceTotal extends Model
{
  use DateTime;
    protected $fillable = ['invoice_id', 'code', 'name', 'amount', 'sort_order'];
    
    public function invoice()
    {
        return $this->belongsTo(Invoice::class);
    }

    /**
     * Convert amount to double.
     *
     * @param  string  $value
     * @return void
     */
    public function setAmountAttribute($value)
    {
        $this->attributes['amount'] = (double) $value;
    }

    /**
     * Get the formatted name.
     *
     * @return string
     */
    public function getTitleAttribute()
    {
        $title = $this->name;

        $percent = 0;

        switch ($this->code) {
            case 'discount':
                $title = trans($title);
                $percent = $this->invoice->discount;

                break;
            case 'tax':
                $rate = Tax::where('name', $title)->value('rate');

                if (!empty($rate)) {
                    $percent = $rate;
                }

                break;
        }

        if (!empty($percent)) {
            $title .= ' (';

            if (setting('general.percent_position', 'after') == 'after') {
                $title .= $percent . '%';
            } else {
                $title .= '%' . $percent;
            }

            $title .= ')';
        }

        return $title;
    }
}
