<?php

namespace Modules\Accounting\Entities;

use Illuminate\Database\Eloquent\Model;
use Bkwld\Cloner\Cloneable;
use Illuminate\Notifications\Notifiable;
use Sofa\Eloquence\Eloquence;
use Notifynder;
use Zana\User;
use Modules\Accounting\Entities\Invoice;
use Modules\Accounting\Entities\Revenue;
use Modules\Accounting\Entities\Currency;

class Customer extends Model
{
  use Cloneable, Eloquence, Notifiable;

    protected $table = 'clients';
    
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'company_name',
        'vat',
        'email',
        'address',
        'zipcode',
        'city',
        'primary_number',
        'secondary_number',
        'industry_id',
        'company_type',
        'user_id'];
    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }
    
    public function revenues()
    {
        return $this->hasMany(Revenue::class);
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_code', 'code');
    }    

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function onCloning($src, $child = null)
    {
        $this->user_id = null;
    }
}
