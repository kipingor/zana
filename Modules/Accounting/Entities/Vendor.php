<?php

namespace Modules\Accounting\Entities;

use Illuminate\Database\Eloquent\Model;
use Bkwld\Cloner\Cloneable;
use Sofa\Eloquence\Eloquence;
use Modules\Accounting\Traits\Media;

class Vendor extends Model
{
	use Cloneable, Eloquence, Media;
    protected $fillable = ['name', 'email', 'tax_number', 'phone', 'address', 'website', 'currency_code', 'enabled'];

    public function bills()
    {
        return $this->hasMany(Bill::class);
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_code', 'code');
    }

    /**
     * Get the current balance.
     *
     * @return string
     */
    public function getLogoAttribute($value)
    {
        if (!empty($value) && !$this->hasMedia('logo')) {
            return $value;
        } elseif (!$this->hasMedia('logo')) {
            return false;
        }

        return $this->getMedia('logo')->last();
    }
}
