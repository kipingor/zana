<?php

namespace Modules\Accounting\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Accounting\Traits\Recurring as RecurringTrait;

class Recurring extends Model
{
	use RecurringTrait;

	protected $table = 'recurring';

    protected $fillable = ['recurable_id', 'recurable_type', 'frequency', 'interval', 'started_at', 'count'];

     /**
     * Get all of the owning recurable models.
     */
    public function recurable()
    {
        return $this->morphTo();
    }
}
