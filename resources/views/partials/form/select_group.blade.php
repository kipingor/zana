<div class="{{ $col }} ">
	<div class="form-group form-group-default form-group-default-select2 input-group {{ isset($attributes['required']) ? 'required' : '' }} {{ $errors->has($name) ? 'has-error' : ''}}">
		<span class="input-group-addon primary"><i class="fa fa-{{ $icon }}"></i></span>
	    {!! Form::label($name, $text, ['class' => 'control-label']) !!}
	        
	        {!! Form::select($name, $values, $selected, array_merge(['class' => 'full-width', 'data-placeholder' => trans('general.form.select.field',  ['field' => $text]), 'data-init-plugin' => 'select2'], $attributes)) !!}
	    {!! $errors->first($name, '<p class="help-block">:message</p>') !!}
	</div>
</div>