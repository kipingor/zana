<div class="{{ $col }}">
    <div class="form-group {{ isset($attributes['required']) ? 'required' : '' }} {{ $errors->has($name) ? 'has-error' : '' }}">
        {!! Form::label($name, $text, ['class' => 'control-label']) !!}
        <div class="input-group">
            <div class="btn-group radio-inline" data-toggle="buttons">
                <label id="{{ $name }}_1" class="btn btn-default">
                    {!! Form::radio($name, '1') !!}
                    <span class="radiotext">{{ __('YES') }}</span>
                </label>
                <label id="{{ $name }}_0" class="btn btn-default">
                    {!! Form::radio($name, '0', true) !!}
                    <span class="radiotext">{{ __('NO') }}</span>
                </label>
            </div>
        </div>
        {!! $errors->first($name, '<p class="help-block">:message</p>') !!}
    </div>
</div>