<div class="{{ $col }}">
<div class="form-group form-group-default input-group {{ isset($attributes['required']) ? 'required' : '' }} {{ $errors->has($name) ? 'has-error' : '' }}">
    {!! Form::label($name, $text, ['class' => 'control-label']) !!}
        {!! Form::text($name, $value, array_merge(['class' => 'form-control', 'placeholder' => trans('general.form.enter', ['field' => $text])], $attributes)) !!}
        <span class="input-group-addon"><i class="fas fa-{{ $icon }}"></i></span>
    {!! $errors->first($name, '<p class="help-block">:message</p>') !!}
</div>
</div>
