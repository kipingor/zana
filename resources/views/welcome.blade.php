@extends('/layouts.home')
@section('content')
@include('/public.partial.explore')
@include('/public.partial.topics')

<section class="p-t-75 " data-pages-bg-image="assets/images/banner.jpeg">
    <div class="container text-center">
        <h2 class="light text-white">
        A different type of <span class="font-montserrat text-uppercase" data-pages-init="text-rotate" data-speed="1000 " data-animation="fade">Work, Era, Move</span> a Whole new page
        </h2>
        <img src="assets/images/laptop_screen.png" class="image-responsive-height m-t-40 lazy" data-papes="float">
    </div>
</section>
@endsection
@section('jumbotron')
<section class="jumbotron full-vh" data-pages="parallax">
    <div class="inner full-height">
        <div class="swiper-container" id="hero">
            <div class="swiper-wrapper">
                <div class="swiper-slide fit bg-complete">
                    <div class="slider-wrapper">
                        <div class="image" data-swiper-parallax="30%">
                            <div data-pages-bg-image="{{ asset('assets/images/mount_kenya.jpg') }}" data-bg-overlay-class="bg-white" class="full-height"></div>
                        </div>
                    </div>
                    <div class="content-layer">
                        <div class="inner full-height">
                            <div class="container-xs-height full-height">
                                <div class="col-xs-height col-middle text-left">
                                    <div class="container text-center">
                                        <h6 class="block-title m-b-0 text-white hint-text">
                                        SHAPING THE FUTURE
                                        </h6>
                                        <h1 class="m-t-5 light text-white">
                                        OF WORK
                                        </h1>
                                        <p class="text-white">Zana is the best management software to run a company. Custom Built integrated Apps.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection