@extends('/layouts.ota')
@section('jumbotron')
<section class="jumbotron last full-height xs-full-height z-index-10" data-pages="parallax" data-pages-bg-image="{{ asset('assets/images/office_table.jpg') }}">
	<div class="container-xs-height full-height inner">
		<div class="col-xs-height col-middle text-left">
			<div class="container">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<div class="text-center sm-p-t-30">
							<img src="{{ asset('assets/img/otadev/logo_icon.png') }}" width="152" height="auto" data-src-retina="{{ asset('assets/img/otadev/logo.png') }}" alt="" class="m-b-30">
							<h1 class="font-montserrat text-uppercase hidden-xs text-white">Lets Discover Together</h1>
							<h2 class="font-montserrat text-uppercase text-white visible-xs">Lets Discover Together</h2>
							<h4 class="text-white">Sign up for our spam free newsletter</h4>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<div class="row">
							<form class="m-t-40 m-b-5 clearfix">
								<div class="col-sm-5   p-r-5 sm-p-r-15">
									<div class="form-group form-group-default col-sm-12">
										<label>Your Name</label>
										<input type="email" class="form-control" placeholder="John Smith">
									</div>
								</div>
								<div class="col-sm-7  p-l-10 sm-p-l-15">
									<div class="form-group form-group-default input-group input-group-attached col-xs-12">
										<label>Email Address</label>
										<input type="email" class="form-control" placeholder="email@abc.com">
										<span class="input-group-btn">
											<button class="btn btn-success  btn-cons" type="button">Sign Up</button>
										</span>
									</div>
									<p class="fs-12 pull-right text-white text-right m-r-20">Be first to find out when we Launch our product.</p>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
@section('content')
<section class="p-b-70 p-t-55 ">
	<div class="container">
		<div class="row">
			<div class="col-md-7">
				<img alt="" src="{{ asset('assets/img/otadev/logo.png') }}">
			</div>
			{{-- <div class="col-md-5">
				<ul class="no-style fs-12 no-padding pull-right xs-pull-left xs-m-t-20">
					<li class="inline no-padding"><a href="#" class="text-black p-l-30 fs-16 xs-no-padding"><i class="fa fa-facebook"></i></a></li>
					<li class="inline no-padding"><a href="#" class="text-black p-l-30 fs-16"><i class="fa fa-twitter"></i></a></li>
					<li class="inline no-padding"><a href="#" class="text-black p-l-30 fs-16"><i class="fa fa-dribbble"></i></a></li>
					<li class="inline no-padding"><a href="#" class="text-black p-l-30 fs-16"><i class="fa fa-rss"></i></a></li>
					<li class="inline no-padding"><a href="#" class="text-black p-l-30 fs-16"><i class="fa fa-linkedin"></i></a></li>
				</ul>
			</div> --}}
		</div>
		{{-- <div class="row m-t-30">
			<div class="col-sm-3 col-xs-12 xs-m-b-20">
				<p class="fs-14">23/4 ways Street. Station 322
					<br> Ottawa Ontario K1AOB1
					<br> Canada.
				</p>
			</div>
			<div class="col-sm-3 col-xs-6 xs-m-b-20">
				<ul class="no-style no-padding">
					<li class="m-b-5 no-padding"><a class="link text-black " href="#"> Pages Blogs </a></li>
					<li class="m-b-5 no-padding"><a class="link text-black" href="#">Go Pro</a></li>
					<li class="m-b-5 no-padding"><a class="link text-black" href="#">Community Meetups</a></li>
					<li class="m-b-5 no-padding"><a class="link text-black" href="#">Become and Affiliate</a></li>
					<li class="m-b-5 no-padding"><a class="link text-black" href="#">Forums</a></li>
					<li class="m-b-5 no-padding"><a class="link text-black" href="#">Help and Support</a></li>
					<li class="m-b-5 no-padding"><a class="link text-black" href="#">News feed</a></li>
				</ul>
			</div>
			<div class="col-sm-3 col-xs-6 xs-m-b-20">
				<ul class="no-style no-padding">
					<li class="m-b-5 no-padding"><a class="link text-black " href="#"> Help Center </a></li>
					<li class="m-b-5 no-padding"><a class="link text-black" href="#">Pages License Details</a></li>
					<li class="m-b-5 no-padding"><a class="link text-black" href="#">Contact us</a></li>
					<li class="m-b-5 no-padding"><a class="link text-black" href="#">Meet the team</a></li>
					<li class="m-b-5 no-padding"><a class="link text-black" href="#">Terms & Conditions</a></li>
				</ul>
			</div>
		</div> --}}
	</div>
</section>
@endsection