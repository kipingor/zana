<section class="bg-white relative p-b-85 p-t-75 p-b-65 p-t-55">
    <div class="container">
        <div class="md-p-l-20 xs-no-padding clearfix">
            <div class="col-sm-4 no-padding">
                <div class="p-r-40 md-p-r-30 xs-no-padding text-center">
                    <img alt="" class="m-b-20" src="assets/images/Parachute.svg">
                    <h6 class="block-title p-b-5">Launch Design <i class="pg-arrow_right m-l-10"></i></h6>
                    <p class="m-b-30">Awesome notepad app. It gives you a quick and simple notepad editing experience when you write notes.</p>
                    <p class="muted font-arial small-text col-sm-9 col-sm-offset-2 no-padding">Icon Fonts, Vector SVG's, pages 14x14px vector icons.</p>
                </div>
                <div class="visible-xs b-b b-grey-light m-t-30 m-b-30"></div>
            </div>
            <div class="col-sm-4 no-padding">
                <div class="p-r-40 md-p-r-30 xs-no-padding text-center">
                    <img alt="" class="m-b-20" src="assets/images/Prizemedalion.svg">
                    <h6 class="block-title p-b-5">Award Wining <i class="pg-arrow_right m-l-10"></i></h6>
                    <p class="m-b-30">Quick chat helps you to Keep in touch with your friends and colleagues while working. It has a unique responsive design.</p>
                    <p class="muted font-arial small-text col-sm-9 col-sm-offset-2 no-padding">Limitless possibilities, Highly customizable, Great UI & UX</p>
                </div>
                <div class="visible-xs b-b b-grey-light m-t-30 m-b-30"></div>
            </div>
            <div class="col-sm-4 no-padding">
                <div class="p-r-40 md-p-r-30  xs-no-padding text-center">
                    <img alt="" class="m-b-20" src="assets/images/Umbrella.svg">
                    <h6 class="block-title p-b-5">Shop ready <i class="pg-arrow_right m-l-10"></i></h6>
                    <p class="m-b-30">This is a very young collection of charts, with the goal of keeping these components very customizable.</p>
                    <p class="muted font-arial small-text col-sm-9 col-sm-offset-2 no-padding">Highly customizable NVD3, rickshaw, Spark Lines, D3.</p>
                </div>
            </div>
        </div>
    </div>
</section>