<section class="bg-master-lightest p-b-70 p-t-80">
    <div class="container text-center">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <h5 class="block-title hint-text no-margin">Explore</h5>
                <h1 class="m-t-5 light">
                Redefine <span class="bold">Scalability</span>
                </h1>
                <p class="m-t-20">
                    Easy-to-use,CRM, HRM, MRP and accounting software designed for small businesses to manage their finances and stay on top of their cash and process flow.
                </p>
            </div>
        </div>
        <div class="content-mask-md p-t-50">
            <div class="device_morph pull-center-inner" id="iphone01">
                <img alt="" class="xs-image-responsive-height image-responsive-width" src="assets/images/b_phone.png" id="mobile_phone">
                <div class="screen">
                    <div class="iphone-border">
                        <img src="assets/images/moutain_mobs.jpg" class="lazy image-responsive-height" alt="" data-pages="float">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="bg-white relative full-width p-t-100 p-b-100 bg-position-top-center bg-size-reset xs-bg-size-cover" data-pages="parallax" data-pages-bg-image="assets/images/mount_phone.jpg">
    <div class="container m-t-50">
        <div class="md-p-l-20 md-p-r-20 m-b-20 xs-no-padding">
            <div class="row p-t-85">
                <div class="col-sm-7 ">
                </div>
                <div class="col-sm-5 col-md-4 sm-text-right">
                    <h5 class="block-title hint-text no-margin">Explore</h5>
                    <h1 class="m-t-5">CRM that helps you sell smarter, better, faster.</h1>
                </div>
            </div>
        </div>
    </div>
</section>