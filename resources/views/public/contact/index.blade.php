@extends('/layouts.public')
@section('content')
<section class="m-t-100 sm-m-t-30">
	<section class="container container-fixed-lg p-t-65 p-b-100  sm-p-b-30 sm-m-b-30">
		<div class="row">
			<div class="col-md-6">
				<h2>How can we assist you?</h2>
				<div class="p-r-40 sm-p-r-0">
					<p>Ask us about our products, support, training or consulting and we will get in touch with you within one working day.</p>
					<br>
					<br>
					<p class="semi-bold no-margin">Fill up this form to contact us if you have any futher questions</p>
					<form role="form" class="m-t-15">
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group form-group-default">
									<label>First name</label>
									<input type="text" class="form-control">
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group form-group-default">
									<label>Last name</label>
									<input type="text" class="form-control">
								</div>
							</div>
						</div>
						<div class="form-group form-group-default">
							<label>Email</label>
							<input type="email" placeholder="Email" class="form-control">
						</div>
						<div class="form-group form-group-default">
							<label>Message</label>
							<textarea placeholder="Type the message you wish to send" style="height:100px" class="form-control"></textarea>
						</div>
						<div class="sm-p-t-10 clearfix">
							<p class="pull-left small hint-text m-t-5 font-arial">I hereby certify that the information above is true and accurate. </p>
							<button class="btn btn-primary font-montserrat all-caps fs-12 pull-right xs-pull-left">Submit</button>
						</div>
						<div class="clearfix"></div>
					</form>
				</div>
			</div>
			<div class="col-md-6">
				<div class="visible-xs visible-sm b-b b-grey-light m-t-35 m-b-30"></div>
				<div class="p-l-40 sm-p-l-0 sm-p-t-10">
					<h3>Technical assistance</h3>
					<p>If you are looking for help with one of the many Zana Applications, our support team can help.</p>
					<br>
					<h3>Press enquiries</h3>
					<p>Journalists or analysts seeking press information should email <a href="mailto:pr@otadev.com">pr@otadev.com</a>. We regret we cannot respond to non-media or analyst enquiries.</p>
					<br>
					<h3>Legal enquiries</h3>
					<p>Please note that we cannot provide you with legal advice. For any other queries, please contact us on <a href="mailto:legal@otadev.com">legal@otadev.com</a>.</p>
					<br>
					<h3>Data Privacy enquiries</h3>
					<p>For information about about data privacy, please read our <a href="#">Privacy Policy</a> or <a href="#">submit an enquiry.</a> A member of our data privacy team will be in touch with you shortly.</p>
					<br>
					<h5 class="block-title hint-text m-b-0">Ota Development Solutions Public Inquiries &amp; Communications </h5>
					<br>
					<div class="row">
						<div class="col-sm-6">
							<p class="fs-12 font-montserrat bold all-caps no-margin hint-text">Hotline </p>
							<p class="hint-text no-margin fs-14">+254 (02) 736-515556</p>
						</div>
						<div class="col-sm-6">
							<p class="fs-12 font-montserrat bold all-caps no-margin hint-text">Email </p>
							<p class="hint-text no-margin fs-14"><a href="mailto:support@otadev.com" class="__cf_email__" >support@otadev.com</a></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="container-fluid  no-padding no-overflow">
		<div id="google-map" class="full-width demo-map gradient-overlay no-overflow"></div>
	</section>
</section>
@endsection
@push('scripts')
<script data-cfasync="false" src="{{ asset('assets/js/email-decode.min.js') }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
<script type="text/javascript" src="{{ asset('assets/js/google_map.js') }}"></script>
@endpush