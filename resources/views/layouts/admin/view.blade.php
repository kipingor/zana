@push('styles')
	<link rel="apple-touch-icon" href="{{ \asset('pages/ico/60.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href={{ \asset('"pages/ico/76.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ \asset('pages/ico/120.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ \asset('pages/ico/152.png') }}">
    <link rel="icon" type="image/x-icon" href="{{ \asset('favicon.ico') }}" />
    <link href="{{ \asset('assets/plugins/pace/pace-theme-flash.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ \asset('assets/plugins/bootstrapv3/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <!--<link href="{{ \asset('assets/plugins/font-awesome/css/font-awesome.css') }}" rel="stylesheet" type="text/css" />-->
    <link href="{{ \asset('assets/plugins/jquery-scrollbar/jquery.scrollbar.css') }}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{ \asset('assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{ \asset('assets/plugins/switchery/css/switchery.min.css') }}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{ \asset('pages/css/pages-icons.css" rel="stylesheet') }}" type="text/css">
    <link class="main-stylesheet" href="{{ \asset('pages/css/pages.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
@endpush

@prepend('scripts')  
<script type="text/javascript"></script>  
    <script src="{{ asset('assets/plugins/pace/pace.min.js') }}" type="text/javascript"></script>
    {{-- <script src="{{ mix('js/app.js') }}"></script> --}}
    {{-- <script src="{{ \asset('assets/plugins/jquery/jquery-1.11.1.min.js') }}" type="text/javascript"></script> --}}
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
{{-- <script src="https://code.jquery.com/jquery-migrate-git.min.js"></script> --}}
    <script src="{{ \asset('assets/plugins/modernizr.custom.js') }}" type="text/javascript"></script>
    <script src="{{ \asset('assets/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ \asset('assets/plugins/bootstrapv3/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ \asset('assets/plugins/jquery/jquery-easy.js') }}" type="text/javascript"></script>
    <script src="{{ \asset('assets/plugins/jquery-unveil/jquery.unveil.min.js') }}" type="text/javascript"></script>
    <script src="{{ \asset('assets/plugins/jquery-bez/jquery.bez.min.js') }}"></script>
    <script src="{{ \asset('assets/plugins/jquery-ios-list/jquery.ioslist.min.js') }}" type="text/javascript"></script>
    <script src="{{ \asset('assets/plugins/jquery-actual/jquery.actual.min.js') }}"></script>
    <script src="{{ \asset('assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js') }}"></script>
    <script type="text/javascript" src="{{ \asset('assets/plugins/select2/js/select2.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ \asset('assets/plugins/classie/classie.js') }}"></script>
    <script src="{{ \asset('assets/plugins/switchery/js/switchery.min.js') }}" type="text/javascript"></script>
    <!-- END VENDOR JS -->
    <!-- BEGIN CORE TEMPLATE JS -->
    <script src="{{ \asset('pages/js/pages.min.js') }}"></script>
    <!-- END CORE TEMPLATE JS -->
    <!-- BEGIN PAGE LEVEL JS -->
    <script src="{{ \asset('assets/js/scripts.js') }}" type="text/javascript"></script>
    {{-- <script src="{{ \asset('assets/js/datatables.js') }}" type="text/javascript"></script> --}}
    <!-- END PAGE LEVEL JS -->
    {{-- <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script> --}}
    {{-- <script src="{{ \asset('js/user.js') }}" type="text/javascript"></script> --}}
    
@endprepend