<ul class="notification-list no-margin hidden-sm hidden-xs b-grey b-l b-r no-style p-l-30 p-r-20">
  <li class="p-r-15 inline">
    <div class="dropdown">
      <?php $notifications = auth()->user()->unreadNotifications; ?>
      <a href="javascript:;" id="notification-center" class="icon-set globe-fill" data-toggle="dropdown">
        <span class="bubble">{{ $notifications->count() }}</span>
      </a>
      <!-- START Notification Dropdown -->
      <div class="dropdown-menu notification-toggle" role="menu" aria-labelledby="notification-center">
        <!-- START Notification -->
        <div class="notification-panel">
          <!-- START Notification Body-->
          <div class="notification-body scrollable">
            <!-- START Notification Body-->
            @foreach($notifications as $notification)
                        <!-- START Notification Item-->
            <div class="notification-item unread clearfix">

              <div class="heading">
                <a href="#" class="text-complete pull-left">
                  <i class="pg-map fs-16 m-r-10"></i>
                  <span class="bold">{{ $notification->data['action'] }}</span>
                  <span class="fs-12 m-l-10">{{ $notification->data['title'] }}</span>
                </a>
                <div class="pull-right">
                  <div class="thumbnail-wrapper d16 circular inline m-t-15 m-r-10 toggle-more-details">
                    <div><i class="fa fa-angle-left"></i>
                    </div>
                  </div>
                  <span class=" time">{{ $notification->created_at->diffForHumans() }}</span>
                </div>
                <div class="more-details" style="display: none;">
                  <div class="more-details-inner">
                    <p class="small hint-text">{{ $notification->data['message']}}</p>
                  </div>
                </div>
              </div>


              <div class="option" data-toggle="tooltip" data-placement="left" title="" data-original-title="mark as read">
                <a href="{{ route('notification.read', ['id' => $notification->id])  }}" onClick="postRead({{ $notification->id }})" class="mark"></a>
              </div>

            </div>

            <!-- END Notification Item-->
             @endforeach
          </div>
          <!-- END Notification Body-->
          <!-- START Notification Footer-->
          <div class="notification-footer text-center">
            <a href="{{ '/notifications/markall' }}" class="">Read all notifications</a>
            <a data-toggle="refresh" class="portlet-refresh text-black pull-right" href="#">
              <i class="pg-refresh_new"></i>
            </a>
          </div>
          <!-- START Notification Footer-->
        </div>
        <!-- END Notification -->
      </div>
      @push('scripts')
      <script>
          // $('#notification-clock').click(function(e) {
          //   e.stopPropagation();
          //   $(".dropdown-menu").toggleClass('unread')
          // });
          $('mark').on("click",function(e) {
            if ($('.notification-item').hasClass('unread')) {
              $(".notification-item").toggleClass('read')
            }
          })      
          id = {};
          function postRead(id) {
              $.ajax({
                  type: 'post',
                  url: '{{url('/notifications/markread')}}',
                  data: {
                      id: id,
                  },
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
              });

          }

      </script>
      @endpush
      <!-- END Notification Dropdown -->
    </div>
  </li>
  <li class="p-r-15 inline">
    <a href="#" class="icon-set clip "></a>
  </li>
  <li class="p-r-15 inline">
    <a href="#" class="icon-set grid-box"></a>
  </li>
</ul>