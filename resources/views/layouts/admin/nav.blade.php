<!-- BEGIN SIDEBPANEL-->
    <nav class="page-sidebar" data-pages="sidebar">
      <!-- BEGIN SIDEBAR MENU TOP TRAY CONTENT-->
      @include('layouts.admin.partials.sidebartop')
      <!-- START SIDEBAR MENU -->
      <div class="sidebar-menu">
        <!-- BEGIN SIDEBAR MENU ITEMS-->
        <ul class="menu-items">
          <li class="m-t-30 ">
            <a href="{{ url('/home') }}" class="detailed">
              <span class="title">Dashboard</span>
              <span class="details">12 New Updates</span>
            </a>
            <span class="icon-thumbnail"><i class="pg-home"></i></span>
          </li>
          @if(auth()->user()->level() > 1)
           <li>
            <a href="javascript:;"><span class="title">Employees</span>
            <span class="arrow"></span></a>
            <span class="bg-complete-darker icon-thumbnail"><i class="fas fa-users"></i></span>
            <ul class="sub-menu">
              <li class="">
                <a href="{{ url('/coming') }}">All Employees</a>
                <span class="icon-thumbnail" style="color: SteelBlue ">AE</span>
              </li>
              <li class="">
                <a href="{{ url('/coming') }}">Holidays</a>
                <span class="icon-thumbnail">H</span>
              </li>
              <li class="">
                <a href="{{ url('/coming') }}">Leave Requests</a>
                <span class="icon-thumbnail">LR</span>
              </li>
              <li class="">
                <a href="{{ url('/coming') }}">Attendence</a>
                <span class="icon-thumbnail">A</span>
              </li>
              <li class="">
                <a href="{{ url('/coming') }}">Department</a>
                <span class="icon-thumbnail">D</span>
              </li>
              <li class="">
                <a href="{{ url('/coming') }}">Designation</a>
                <span class="icon-thumbnail">D</span>
              </li>
              <li class="">
                <a href="{{ url('/crm/clients') }}">Clients</a>
                <span class="icon-thumbnail"><i class="fas fa-user-friends"></i></span>
              </li>
            </ul>
          </li>
          <li>
            <a href="{{ url('/crm/clients') }}" class="detailed">
              <span class="title">Clients</span>
              <span class="details">12 New Updates</span>
            </a>
            <span class="bg-warning-dark icon-thumbnail"><i class="fas fa-user-plus"></i></span>
          </li>
          <li>
            <a href="{{ url('/coming') }}" class="detailed">
              <span class="title">Projects</span>
              <span class="details">12 New Updates</span>
            </a>
            <span class="bg-complete-dark icon-thumbnail"><i class="fas fa-project-diagram"></i></span>
          </li>
          <li>
            <a href="{{ url('/crm/tasks') }}" class="detailed">
              <span class="title">Tasks</span>
              <span class="details">12 New Updates</span>
            </a>
            <span class="bg-success-darker icon-thumbnail"><i class="fas fa-tasks"></i></span>
          </li>
          <li>
            <a href="javascript:;"><span class="title">Calls</span>
            <span class=" arrow"></span></a>
            <span class="bg-danger icon-thumbnail"><i class="fas fa-phone"></i></span>
            <ul class="sub-menu">
              <li class="">
                <a href="{{ url('/settings/company') }}">Voice Call</a>
                <span class="icon-thumbnail">VC</span>
              </li>
              <li class="">
                <a href="{{ url('/settings/localization') }}">Video Calls</a>
                <span class="icon-thumbnail">VC</span>
              </li>
              <li class="">
                <a href="{{ url('/settings') }}">Incoming Calls</a>
                <span class="icon-thumbnail">IC</span>
              </li>
            </ul>
          </li>
          <li>
            <a href="{{ url('/coming') }}" class="detailed">
              <span class="title">Contacts</span>
              <span class="details">12 New Updates</span>
            </a>
            <span class="icon-thumbnail"><i class="fas fa-user-astronaut"></i></span>
          </li>
          <li>
            <a href="{{ url('/crm/leads') }}" class="detailed">
              <span class="title">Leads</span>
              <span class="details">12 New Updates</span>
            </a>
            <span class="icon-thumbnail"><i class="fas fa-headset"></i></span>
          </li>
          <li>
            <a href="javascript:;"><span class="title">Accounting</span>
            <span class=" arrow"></span></a>
            <span class="bg-success-lighter icon-thumbnail"><i class="fas fa-dollar-sign text-black"></i></span>
            <ul class="sub-menu">
              <li class="">
                <a href="{{ url('/accounting/items') }}">Items</a>
                <span class="icon-thumbnail"><i class="fas fa-money-check-alt"></i></span>
              </li>
              <li>
                <a href="javascript:;"><span class="title">Banking</span>
                <span class="arrow"></span></a>
                <span class="icon-thumbnail">L2</span>
                <ul class="sub-menu">
                  <li>
                    <a href="{{ url('/accounting/banking/accounts') }}">Accounts</a>
                    <span class="icon-thumbnail">Sm</span>
                  </li>
                  <li>
                    <a href="{{ url('/coming') }}">Transactions</a>
                    <span class="icon-thumbnail">Sm</span>
                  </li>
                  <li>
                    <a href="{{ url('/coming') }}">Tranfers</a>
                    <span class="icon-thumbnail">Sm</span>
                  </li>
                </ul>
              </li>
              <li>
                <a href="javascript:;"><span class="title">Income</span>
                <span class="arrow"></span></a>
                <span class="icon-thumbnail">L2</span>
                <ul class="sub-menu">
                  <li>
                    <a href="{{ url('/coming') }}">Estimates</a>
                    <span class="icon-thumbnail">Sm</span>
                  </li>
                  <li>
                    <a href="{{ url('/accounting/incomes/invoices') }}">Invoices</a>
                    <span class="icon-thumbnail">Sm</span>
                  </li>
                  <li>
                    <a href="{{ url('/crm/clients') }}">Customers</a>
                    <span class="icon-thumbnail">Sm</span>
                  </li>
                  <li>
                    <a href="{{ url('/accounting/incomes/revenues') }}">Revenue</a>
                    <span class="icon-thumbnail">Sm</span>
                  </li>
                </ul>
              </li>
              <li>
                <a href="javascript:;"><span class="title">Expenses</span>
                <span class="arrow"></span></a>
                <span class="icon-thumbnail">L2</span>
                <ul class="sub-menu">
                  <li>
                    <a href="{{ url('/accounting/expenses/bills') }}">Bills</a>
                    <span class="icon-thumbnail">Sm</span>
                  </li>
                  <li>
                    <a href="{{ url('/accounting/expenses/vendors') }}">Vendor</a>
                    <span class="icon-thumbnail">Sm</span>
                  </li>
                  <li>
                    <a href="{{ url('/accounting/expenses/payments') }}">Payments</a>
                    <span class="icon-thumbnail"><i class="fas fa-money-check-alt"></i></span>
                  </li>
                </ul>
              </li>
              <li class="">
                <a href="{{ url('/coming') }}">Providend Fund</a>
                <span class="icon-thumbnail">n</span>
              </li>
              <li class="">
                <a href="{{ url('/coming') }}">Taxes</a>
                <span class="icon-thumbnail">Int</span>
              </li>
            </ul>
          </li>
          <li>
            <a href="{{ url('/coming') }}" class="detailed">
              <span class="title">Payroll</span>
              <span class="details">12 New Updates</span>
            </a>
            <span class="bg-success-dark icon-thumbnail"><i class="pg-home"></i></span>
          </li>
          <li>
            <a href="{{ url('/coming') }}" class="detailed">
              <span class="title">Activities</span>
              <span class="details">12 New Updates</span>
            </a>
            <span class="bg-warning-lighter icon-thumbnail"><i class="fas fa-assistive-listening-systems text-black"></i></span>
          </li>
          <li>
            <a href="{{ url('/coming') }}" class="detailed">
              <span class="title">Reports</span>
              <span class="details">12 New Updates</span>
            </a>
            <span class="bg-warning-darker icon-thumbnail"><i class="fas fa-file-contract"></i></span>
          </li>
          <li>
            <a href="{{ url('/settings/general') }}" class="detailed">
              <span class="title">Settings</span>
              <span class="details">12 New Updates</span>
            </a>
            <span class="bg-danger-darker icon-thumbnail"><i class="fas fa-cogs"></i></span>
          </li>
          @endif
        </ul>
        <div class="clearfix"></div>
      </div>
      <!-- END SIDEBAR MENU -->
    </nav>
    <!-- END SIDEBAR -->