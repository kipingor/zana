@if(count($errors))
  <div class="pgn-wrapper" data-position="top">
    <div class="pgn push-on-sidebar-open pgn-bar">
      <div class="alert alert-danger" role="alert">
        <p class="pull-left"><strong>Errors</strong></p>
        <button class="close" data-dismiss="alert"></button>
        <div class="clearfix"></div>
        @foreach($errors->all() as $error)
        <p>{{$error}}</p>
        @endforeach
      </div>
    </div>
  </div>
@endif



@if($flash = session('flash_message'))
    <div class="pgn-wrapper" data-position="top">
      <div class="pgn push-on-sidebar-open pgn-bar">
        <div class="alert alert-info">
          <button type="button" class="close" data-dismiss="alert">
            <span aria-hidden="true">×</span>
            <span class="sr-only">Close</span>
          </button>
          <span>{{ $flash }}</span>
        </div>
      </div>
    </div>
@endif

@if($flash = session('flash_message_warning'))
    <div class="pgn-wrapper" data-position="top">
      <div class="pgn push-on-sidebar-open pgn-bar">
        <div class="alert alert-warning">
          <button type="button" class="close" data-dismiss="alert">
            <span aria-hidden="true">×</span>
            <span class="sr-only">Close</span>
          </button>
          <span>{{ $flash }}</span>
        </div>
      </div>
    </div>
@endif

@if($flash = session('flash_message_success'))
    <div class="pgn-wrapper" data-position="top">
      <div class="pgn push-on-sidebar-open pgn-bar">
        <div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert">
            <span aria-hidden="true">×</span>
            <span class="sr-only">Close</span>
          </button>
          <span>{{ $flash }}</span>
        </div>
      </div>
    </div>
@endif

@if (session('status'))
  <div class="pgn-wrapper" data-position="top"><div class="pgn push-on-sidebar-open pgn-bar"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
  <span>{{ session('status') }}</span></div></div></div>
@endif