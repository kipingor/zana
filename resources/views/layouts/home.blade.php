@include('layouts.public.frontview')
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
        <meta charset="utf-8" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name') }}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <script src="{{ asset('cdn-cgi/apps/head/SKMU_y-toaXBzIZvmUGt9sHcMjg.js') }}"></script><link rel="apple-touch-icon" href="pages/ico/60.png">
        <link rel="apple-touch-icon" sizes="76x76" href="pages/ico/76.png">
        <link rel="apple-touch-icon" sizes="120x120" href="pages/ico/120.png">
        <link rel="apple-touch-icon" sizes="152x152" href="pages/ico/152.png">
        <link rel="icon" type="image/x-icon" href="favicon.ico" />
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-touch-fullscreen" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="default">
        <meta content="" name="description" />
        <meta content="" name="author" />
        @stack('styles')
    </head>
    <body class="pace-black">
        <section class="container relative">
            <div class="top-left z-index-10">
                <div class="bg-white p-l-20 p-r-20 p-t-20 p-b-20">
                    <img src="{{ asset('assets/img/logo_z.png') }}" data-src-retina="{{ asset('assets/img/logo_2x.png') }}" class="" alt="" width="auto" height="20">
                </div>
            </div>
        </section>
        @include('layouts.public.homenav')
        @yield('jumbotron')
        @yield('content')
        @include('layouts.public.footer')
        @include('layouts.public.search')
        @stack('scripts')
    </body>
</html>