<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
	<head>
		<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
		<meta charset="utf-8" />
		<title>{{ config('app.name') }}</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<script src="/cdn-cgi/apps/head/QJpHOqznaMvNOv9CGoAdo_yvYKU.js"></script><link rel="apple-touch-icon" href="{{ asset('pages/ico/60.png') }}">
		<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('pages/ico/76.png') }}">
		<link rel="apple-touch-icon" sizes="120x120" href="{{ asset('pages/ico/120.png') }}">
		<link rel="apple-touch-icon" sizes="152x152" href="{{ asset('pages/ico/152.png') }}">
		<link rel="icon" type="image/x-icon" href="favicon.ico" />
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-touch-fullscreen" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="default">
		<meta content="" name="description" />
		<meta content="" name="author" />
		<link href="{{ asset('assets/plugins/pace/pace-theme-flash.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('assets/plugins/font-awesome/css/font-awesome.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('assets/plugins/swiper/css/swiper.css') }}" rel="stylesheet" type="text/css" media="screen" />
		<link class="main-stylesheet" href="{{ asset('pages/css/front/pages.css') }}" rel="stylesheet" type="text/css" />
		<link class="main-stylesheet" href="{{ asset('pages/css/pages-icons.css') }}" rel="stylesheet" type="text/css" />
	</head>
	<body class="pace-danger">
		@yield('jumbotron')
		<section class="z-index-1 pull-bottom-fixed full-width" data-pages="reveal-footer">
			@yield('content')
			@include('layouts.public.footer_two')
	</section>
	<script src="{{ asset('assets/plugins/pace/pace.min.js') }}" type="text/javascript"></script>
	<script type="text/javascript" src="{{ asset('assets/plugins/jquery/jquery-1.11.1.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('pages/js/pages.frontend.js') }}"></script>
</html>