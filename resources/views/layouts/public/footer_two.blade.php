<section class="p-t-10 p-b-10 bg-master-darkest">
	<div class="container">
		<div class="row">
			<div class="col-sm-7">
				<div class="m-t-10 ">
          <ul class="no-style fs-11 no-padding font-arial">
            @auth
            <li class="inline no-padding"><a href="{{ url('/home') }}" class="hint-text text-white p-l-10 p-r-10 b-r b-grey">Dashboard</a></li>
            @else
            <li class="inline no-padding"><a href="{{ route('login') }}" class="hint-text text-white p-l-10 p-r-10 {{-- b-r --}} b-grey">Login</a></li>
            <li class="inline no-padding"><a href="{{ route('register') }}" class="hint-text text-master p-l-10 p-r-10 xs-no-padding xs-m-t-10">Register</a></li>
            @endauth
            {{-- <li class="inline no-padding"><a href="{{ url('/support') }}" class="hint-text text-master p-l-10 p-r-10 b-r b-grey">Support</a></li>
            <li class="inline no-padding"><a href="{{ url('/contact') }}" class="hint-text text-master p-l-10 p-r-10 b-r b-grey">Contact</a></li> --}}
          </ul>
        </div>
				{{-- <p class="fs-11 no-margin font-arial text-white small-text"><span class="hint-text">{{ config('app.name') }}</span>
			</p> --}}

		</div>
		<div class="col-sm-5 text-right">
			<p class="fs-11 no-margin font-arial text-white small-text">Copyright © 2018 Ota Development Solutions. All Rights Reserved.
			</p>
		</div>
	</div>
</div>
</section>