@push('styles')
<link href="{{ asset('assets/plugins/pace/pace-theme-flash.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
<link href="{{ asset('assets/plugins/swiper/css/swiper.css') }}" rel="stylesheet" type="text/css" media="screen" />
<link class="main-stylesheet" href="{{ asset('pages/css/front/pages.css') }}" rel="stylesheet" type="text/css" />
<link class="main-stylesheet" href="{{ asset('pages/css/pages-icons.css') }}" rel="stylesheet" type="text/css" />
@endpush

@prepend('scripts')
<script src="{{ asset('assets/plugins/pace/pace.min.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ asset('pages/js/pages.image.loader.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/plugins/jquery/jquery-1.11.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/plugins/jquery-unveil/jquery.unveil.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('pages/js/pages.frontend.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/plugins/text-rotate/jquery.simple-text-rotator.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/custom.js') }}"></script>
@endprepend