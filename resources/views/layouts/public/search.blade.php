<div class="overlay hide" data-pages="search">
    <div class="overlay-content full-height has-results">
        <div class="container relative full-height">
            <div class="container-fluid">
                <a href="#" class="close-icon-light overlay-close text-black fs-16 top-right">
                    <i class="pg-close_line"></i>
                </a>
            </div>
            <div class="container-fluid">
                <div class="inline-block bottom-right m-b-30">
                    <div class="checkbox right">
                        <input id="checkboxn" type="checkbox" value="1" checked="checked">
                        <label for="checkboxn">Search within page</label>
                    </div>
                </div>
            </div>
            <div class="container-xs-height full-height">
                <div class="col-xs-height col-middle text-center">
                    <input id="overlay-search" class="no-border overlay-search bg-transparent col-sm-6 col-sm-offset-4" placeholder="Search..." autocomplete="off" spellcheck="false">
                    <br>
                    <div class="inline-block bottom-left m-l-10 m-b-30 hidden-xs">
                        <p class="fs-14"><i class="fa fa-search m-r-10"></i> Press enter to search</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>