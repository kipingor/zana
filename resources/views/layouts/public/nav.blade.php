<div class="demo-brush bottom-left" data-pages-bg-image="{{ asset('assets/images/brush_stroke.png') }}" >
</div>
<div class="container relative">
    <div class="pull-left">
        <div class="header-inner">
            <img src="{{ asset('assets/img/logo_z.png') }}" data-src-retina="{{ asset('assets/img/logo_2x.png') }}" class="" alt="" width="auto" height="20">
        </div>
    </div>
    <div class="pull-right">
        <div class="header-inner">
            <a href="#" data-toggle="search" class="search-toggle visible-sm-inline visible-xs-inline p-r-10"><i class="fs-14 pg-search"></i></a>
            <div class="visible-sm-inline visible-xs-inline menu-toggler pull-right p-l-10" data-pages="header-toggle" data-pages-element="#header">
                <div class="one"></div>
                <div class="two"></div>
                <div class="three"></div>
            </div>
        </div>
    </div>
    <div class="menu-content pull-right clearfix" data-pages="menu-content" data-pages-direction="slideRight" id="header">
        <div class="pull-right">
            <a href="#" class="text-black link padding-10 visible-xs-inline visible-sm-inline pull-right m-t-10 m-b-10 m-r-10" data-pages="header-toggle" data-pages-element="#header">
                <i class=" pg-close_line"></i>
            </a>
        </div>
        <div class="header-inner">
            <ul class="menu">
                <li>
                    <a class="active" href="{{ url('/') }}" data-text="Home">Home
                        <span data-text="Welcome">Welcome</span>
                    </a>
                </li>
                <!--
                <li class="classic  multiline">
                    <a href="javascript:;" data-text="Elements">Elements <i class="pg-arrow_minimize m-l-5"></i>
                        <span data-text="Shortcodes">Shortcodes</span>
                    </a>
                    <nav class="classic ">
                        <span class="arrow"></span>
                        <ul>
                            <li>
                                <a href="http://pages.revox.io/frontend/1.0/doc/#color" target="_blank">Colors</a>
                            </li>
                            <li>
                                <a href="http://pages.revox.io/frontend/1.0/doc/#icons" target="_blank">Icons</a>
                            </li>
                            <li>
                                <a href="http://pages.revox.io/frontend/1.0/doc/#buttons" target="_blank">Buttons</a>
                            </li>
                            <li>
                                <a href="http://pages.revox.io/frontend/1.0/doc/#modals" target="_blank">Modals</a>
                            </li>
                            <li>
                                <a href="http://pages.revox.io/frontend/1.0/doc/#progress_bars" target="_blank">Progress &amp; Activity</a>
                            </li>
                            <li>
                                <a href="http://pages.revox.io/frontend/1.0/doc/#accordians" target="_blank">Accordians</a>
                            </li>
                            <li>
                                <a href="http://pages.revox.io/frontend/1.0/doc/#tabs" target="_blank">Tabs</a>
                            </li>
                        </ul>
                    </nav>
                </li>
                <li>
                    <a href="pricing.html" data-text="Pricing">Pricing
                        <span data-text="Plans">Plans</span>
                    </a>
                </li>
            -->
                <li>
                    <a href="{{ url('/portfolio') }}" data-text="Portofolio">Portofolio
                        <span data-text="Clients">Clients</span>
                    </a>
                </li>
                <li>
                    <a href="{{ url('/contact') }}" data-text="Contact Us">Contact Us
                        <span data-text="Get in touch">Get in touch </span>
                    </a>
                </li>
            </ul>
            <a href="#" class="search-toggle hidden-xs hidden-sm" data-toggle="search"><i class="fs-14 pg-search"></i></a>
            <div class="font-arial m-l-35 m-r-35 m-b-20 m-t-20 visible-sm visible-xs">
                <p class="fs-11 no-margin small-text p-b-20">See Standard licenses & Extended licenses
                </p>
                <p class="fs-11 small-text muted">Copyright &copy; 2018 Ota Development Solutions</p>
            </div>
        </div>
    </div>
</div>