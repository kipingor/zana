<section class="p-b-30 p-t-40">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <img src="{{ asset('assets/img/logo.png') }}" width="auto" height="21" data-src-retina="{{ asset('assets/img/logo_2x.png') }}" class="logo inline m-r-50" alt="">
                <div class="m-t-10 ">
                    <ul class="no-style fs-11 no-padding font-arial">
                        @auth
                        <li class="inline no-padding"><a href="{{ url('/home') }}" class="hint-text text-master p-l-10 p-r-10 b-r b-grey">Dashboard</a></li>
                        @else
                        <li class="inline no-padding"><a href="{{ route('login') }}" class="hint-text text-master p-l-10 p-r-10 b-r b-grey">Login</a></li>
                        {{-- <li class="inline no-padding"><a href="{{ route('register') }}" class="hint-text text-master p-l-10 p-r-10 xs-no-padding xs-m-t-10">Register</a></li> --}}
                        @endauth
                        <li class="inline no-padding"><a href="{{ url('/support') }}" class="hint-text text-master p-l-10 p-r-10 b-r b-grey">Support</a></li>
                        <li class="inline no-padding"><a href="{{ url('/contact') }}" class="hint-text text-master p-l-10 p-r-10 b-r b-grey">Contact</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-6 text-right font-arial sm-text-left">
                <p class="fs-11 no-margin small-text"><span class="hint-text">Cloud applications availale at</span> ZANA <span class="hint-text">See</span> Standard licenses &amp; Extended licenses</p>
                <p class="fs-11 muted">Copyright &copy; 2018 OtDevelopment Solutions. All Rights Reserved.</p>
            </div>
        </div>
    </div>
</section>