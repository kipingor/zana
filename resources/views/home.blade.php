@extends('/layouts.master')

@section('jumbotron')

<div class="jumbotron page-cover" data-pages="parallax">
    <div class="container-fluid container-fixed-lg">
      <div class="inner" style="transform: translateY(27.03px); opacity: 1;">
        <!-- START BREADCRUMB -->
        <ul class="breadcrumb">
          <li>
            <a class="active" href="#">Dashboard</a>
          </li>          
        </ul>
        <!-- END BREADCRUMB -->
      </div>
    </div>
  </div>

@endsection

@section('content')
<div class="col-sm-4">
    <div class="panel panel-white">
      <div class="panel-heading">
        <div class="panel-title">
          CRM
        </div>
      </div>
      <div class="panel-body">
        <h3 class="text-primary no-margin"> manage and analyze customer interactions and data</h3>
        <br>
        <p>improving customer service relationships and assisting in customer retention and driving sales growth. 
        </p>        
        <br>
        <a href="/crm" class="btn btn-primary">Go to CRM</a>
        <p class="hint-text m-t-10 small">Learn more at CRM Pages</p>
      </div>
    </div>
</div>

<div class="col-sm-4">
    <div class="panel panel-white">
      <div class="panel-heading">
        <div class="panel-title">
          Accounting
        </div>
      </div>
      <div class="panel-body">
        <h3 class="text-primary no-margin"> manage and analyze customer interactions and data</h3>
        <br>
        <p>improving customer service relationships and assisting in customer retention and driving sales growth. 
        </p>        
        <br>
        <a href="/accounting" class="btn btn-primary">Go to Accounting</a>
        <p class="hint-text m-t-10 small">Learn more at Accounting Pages</p>
      </div>
    </div>
</div>
@endsection
