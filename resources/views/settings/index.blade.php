@extends('layouts.master')
@section('heading')
    <h1>{{ __('Settings') }}</h1>
@stop
@section('content')

<div class="row">
  <div class="col-lg-3">
    <div class="panel panel-transparent">
      <div class="panel-heading">
        <div class="panel-title">Roles and Permissions</div>
        @role('admin')
        <a href="{{ url('/roles/create')}}" class="btn btn-primary btn-block"><i class="fas fa-plus"></i> Add Role</a>
        @endrole
      </div>
      <div class="panel-body">
        <ul class="nav nav-tabs nav-tabs-simple nav-tabs-left bg-white" id="tab-3">
         @foreach($roles as $role)
          <li @if($loop->first) class="active" @endif>
            <a data-toggle="tab" href="#tab-role-{{$role->id}}">{{$role->name}}</a>
          </li>
          @endforeach
        </ul>
      </div>
    </div>
    
  </div>
    <div class="col-lg-9">
      <div class="tab-content bg-white">
        @foreach($roles as $role)
        <div class="tab-pane @if($loop->first) active @endif" id="tab-role-{{$role->id}}">
          {!! Form::model($permission, [
              'method' => 'PATCH',
              'url'    => 'settings/permissionsUpdate',
          ]) !!}
          <table class="table table-responsive table-hover table_wrapper" id="permissions-table">
            <thead>
              <tr>
                <th style="width:30%">Module Name</th>
                <th colspan="4">{{__('Permissions')}}</th>          
              </tr>
            </thead>
            <tbody>
              <input type="hidden" name="role_id" value="{{ $role->id }}"/>
              @foreach($model_value as $modperm)
              <tr>
                <td>{{$modperm->model}}</td>
                @foreach($permission as $perm)
                <?php
                $isEnabled = !current(
                        array_filter(
                          $role->permissions->toArray(), function ($element) use ($perm) {
                            return $element['id'] === $perm->id;
                          }
                        )
                );
                ?>
                
                @if($perm->model === $modperm->model)
                                
                <td>
                  <input type="checkbox" <?php if (!$isEnabled) echo 'checked' ?> name="permissions[ {{ $perm->id }} ]"
                           value="1" data-role="{{ $role->id }}">
                  
                  <span class="hint-text">{{$perm->name}}</span><br/>
                </td>

                @endif
                @endforeach 
              </tr>              
              @endforeach
              <tr>
                @role('admin')
                {!! Form::submit( __('Save Role ') , ['class' => 'btn btn-primary btn-block']) !!}
                @endrole
              </tr>
            </tbody>
          </table>
           {!! Form::close() !!}
        </div>
        @endforeach
      </div>
    </div>
    
</div>



    <div class="row">
        <div class="col-lg-12">
            <div class="sidebarheader movedown"><p>{{ __('Overall Settings') }}</p></div>


            {!! Form::model($settings, [
               'method' => 'PATCH',
               'url' => 'settings/overall'
               ]) !!}

                    <!-- *********************************************************************
     *                     Task complete       
     *********************************************************************-->
            <div class="panel panel-default movedown">
                <div class="panel-heading">{{ __('Task completion') }}</div>
                <div class="panel-body">

                    {{ __('If Allowed only user who are assigned the task & the admin can complete the task.') }} <br/>
                    {{ __('If Not allowed anyone, can complete all tasks.')}}
                </div>
            </div>
            {!! Form::select('task_complete_allowed', 
            [
                1 => __('Allowed'), 
                2 => __('Not allowed')
            ], 
            $settings->task_complete_allowed, ['class' => 'form-control']) !!}
                    <!-- *********************************************************************
     *                     Task assign       
     *********************************************************************-->
            <div class="panel panel-default movedown">
                <div class="panel-heading">{{ __('Task assigning') }}</div>
                <div class="panel-body">

                   {{ __('If Allowed only user who are assigned the task &amp; the admin can assign another user.') }} <br/>
                    {{ __('If Not allowed anyone, can assign another user.') }}
                </div>
            </div>
            {!! Form::select('task_assign_allowed', 
            [
                1 => __('Allowed'), 
                2 => __('Not allowed')
            ],
            $settings->task_assign_allowed, ['class' => 'form-control']) !!}
                    <!-- *********************************************************************
     *                     Lead complete       
     *********************************************************************-->

            <div class="panel panel-default movedown">
                <div class="panel-heading">{{ __('Lead completion') }}</div>
                <div class="panel-body">

                    {{ __('If Allowed only user who are assigned the lead & the admin can complete the lead.') }} <br/>
                    {{ __('If Not allowed anyone, can complete all leads.')}}
                </div>
            </div>
            {!! Form::select('lead_complete_allowed', [
                1 => __('Allowed'), 
                2 => __('Not allowed')
            ], 
            $settings->lead_complete_allowed, ['class' => 'form-control']) !!}
                    <!-- *********************************************************************
     *                     Lead assign       
     *********************************************************************-->
            <div class="panel panel-default movedown">
                <div class="panel-heading">{{ __('Lead assigning') }}</div>
                <div class="panel-body">

                    {{ __('If Allowed only user who are assigned the lead & the admin can complete the lead.') }} <br/>
                    {{ __('If Not allowed anyone, can complete all leads.')}}
                </div>
            </div>
            {!! Form::select('lead_assign_allowed', 
            [
                1 => __('Allowed'), 
                2 => __('Not allowed')
            ], 
            $settings->lead_assign_allowed, ['class' => 'form-control']) !!}
            <br/>
            {!! Form::submit( __('Save overall settings'), ['class' => 'btn btn-primary']) !!}
            {!! Form::close() !!}
        </div>
    </div>

@endsection