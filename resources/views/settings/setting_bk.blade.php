

    <div class="row">
    <div class="col-lg-12">
        
    </div>
     
    <div class="col-lg-12">
    {!! Form::model($permission, [
        'method' => 'PATCH',
        'url'    => 'settings/permissionsUpdate',
    ]) !!}

        <table class="table table-responsive table-hover table_wrapper" id="permissions-table">
            <thead>
            <tr>
            <th></th>
                @foreach($permission as $perm)
             <th>{{$perm->name}}</th>

                @endforeach
                <th></th>
            </tr>

            </thead>
            <tbody>
                @foreach($roles as $role)
        <input type="hidden" name="role_id" value="{{ $role->id }}"/>
                <tr>

                        <th>{{$role->name}}</th>
                        @foreach($permission as $perm)
                            <?php $isEnabled = !current(
                                    array_filter(
                                            $role->permissions->toArray(),
                                            function ($element) use ($perm) {
                                                return $element['id'] === $perm->id;
                                            }
                                    )
                            );  ?>

                            <td><input type="checkbox"
                                       <?php if (!$isEnabled) echo 'checked' ?> name="permissions[ {{ $perm->id }} ]"
                                       value="1" data-role="{{ $role->id }}">
                                <span class="perm-name"></span><br/></td>

                
                    @endforeach        
    <td>{!! Form::submit( __('Save Role') , ['class' => 'btn btn-primary']) !!}</td>
   
            </tr>
            @endforeach
      </tbody>
    </table>
     {!! Form::close() !!}
     </div>
     
</div>



    <div class="row">
        <div class="col-lg-12">
            <div class="sidebarheader movedown"><p>{{ __('Overall Settings') }}</p></div>


            {!! Form::model($settings, [
               'method' => 'PATCH',
               'url' => 'settings/overall'
               ]) !!}

                    <!-- *********************************************************************
     *                     Task complete       
     *********************************************************************-->
     <div class="col-sm-3">
            <div class="panel panel-default movedown">
                <div class="panel-heading">{{ __('Task completion') }}</div>
                <div class="panel-body">

                    {{ __('If Allowed only user who are assigned the task & the admin can complete the task.') }} <br/>
                    {{ __('If Not allowed anyone, can complete all tasks.')}}
                
                    {!! Form::select('task_complete_allowed', 
                    [
                        1 => __('Allowed'), 
                        2 => __('Not allowed')
                    ], 
                    $settings->task_complete_allowed, ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
                    <!-- *********************************************************************
     *                     Task assign       
     *********************************************************************-->
     <div class="col-sm-3">
            <div class="panel panel-default movedown">
                <div class="panel-heading">{{ __('Task assigning') }}</div>
                <div class="panel-body">

                   {{ __('If Allowed only user who are assigned the task &amp; the admin can assign another user.') }} <br/>
                    {{ __('If Not allowed anyone, can assign another user.') }}
                
                    {!! Form::select('task_assign_allowed', 
                    [
                        1 => __('Allowed'), 
                        2 => __('Not allowed')
                    ],
                    $settings->task_assign_allowed, ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
                    <!-- *********************************************************************
     *                     Lead complete       
     *********************************************************************-->
<div class="col-sm-3">
            <div class="panel panel-default movedown">
                <div class="panel-heading">{{ __('Lead completion') }}</div>
                <div class="panel-body">

                    {{ __('If Allowed only user who are assigned the lead & the admin can complete the lead.') }} <br/>
                    {{ __('If Not allowed anyone, can complete all leads.')}}
                
                    {!! Form::select('lead_complete_allowed', [
                        1 => __('Allowed'), 
                        2 => __('Not allowed')
                    ], 
                    $settings->lead_complete_allowed, ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
                    <!-- *********************************************************************
     *                     Lead assign       
     *********************************************************************-->
     <div class="col-sm-3">
            <div class="panel panel-default movedown">
                <div class="panel-heading">{{ __('Lead assigning') }}</div>
                <div class="panel-body">

                    {{ __('If Allowed only user who are assigned the lead & the admin can complete the lead.') }} <br/>
                    {{ __('If Not allowed anyone, can complete all leads.')}}
                
            
                    {!! Form::select('lead_assign_allowed', 
                    [
                        1 => __('Allowed'), 
                        2 => __('Not allowed')
                    ], 
                    $settings->lead_assign_allowed, ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
            <br/>
            {!! Form::submit( __('Save overall settings'), ['class' => 'btn btn-primary']) !!}
            {!! Form::close() !!}
        </div>
    </div>