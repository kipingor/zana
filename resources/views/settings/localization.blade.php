@extends('layouts.master')

@section('content')

<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<form>
			<h3 class="page-title">Basic Settings</h3>
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group form-group-default form-group-default-select2">
						<label>Default Country</label>
						<select class="full-width select2-hidden-accessible" data-placeholder="Select Country" data-init-plugin="select2" tabindex="-1" aria-hidden="true">
							<option selected="selected">USA</option>
							<option>United Kingdom</option>
						</select>
						<span class="select2 select2-container select2-container--default" dir="ltr" style="width: 100%;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-csvo-container"><span class="select2-selection__rendered" id="select2-csvo-container" title="USA">USA</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group form-group-default form-group-default-select2">
						<label>Date Format</label>
						<select class="full-width select2-hidden-accessible" data-init-plugin="select2" tabindex="-1" aria-hidden="true">
							<option value="d/m/Y">15/05/2016</option>
							<option value="d.m.Y">15.05.2016</option>
							<option value="d-m-Y">15-05-2016</option>
							<option value="m/d/Y">05/15/2016</option>
							<option value="Y/m/d">2016/05/15</option>
							<option value="Y-m-d">2016-05-15</option>
							<option value="M d Y">May 15 2016</option>
							<option selected="selected" value="d M Y">15 May 2016</option>
						</select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 100%;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-tl70-container"><span class="select2-selection__rendered" id="select2-tl70-container" title="15 May 2016">15 May 2016</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group form-group-default form-group-default-select2">
						<label>Timezone</label>
						<select class="full-width select2-hidden-accessible" data-init-plugin="select2" tabindex="-1" aria-hidden="true">
							<option>(UTC +5:30) Antarctica/Palmer</option>
						</select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 100%;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-sfuj-container"><span class="select2-selection__rendered" id="select2-sfuj-container" title="(UTC +5:30) Antarctica/Palmer">(UTC +5:30) Antarctica/Palmer</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group form-group-default form-group-default-select2">
						<label>Default Language</label>
						<select class="full-width select2-hidden-accessible" data-init-plugin="select2" tabindex="-1" aria-hidden="true">
							<option selected="selected">English</option>
							<option>French</option>
						</select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 100%;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-sus6-container"><span class="select2-selection__rendered" id="select2-sus6-container" title="English">English</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group form-group-default form-group-default-select2">
						<label>Currency Code</label>
						<select class="full-width select2-hidden-accessible" data-init-plugin="select2" tabindex="-1" aria-hidden="true">
							<option selected="selected">USD</option>
							<option>Pound</option>
							<option>EURO</option>
							<option>Ringgit</option>
						</select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 100%;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-0lux-container"><span class="select2-selection__rendered" id="select2-0lux-container" title="USD">USD</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group form-group-default">
						<label>Currency Symbol</label>
						<input class="form-control" readonly="" value="$" type="text">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 text-center m-t-20">
					<button type="button" class="btn btn-primary">Save &amp; update</button>
				</div>
			</div>
        </form>
	</div>
</div>

@endsection