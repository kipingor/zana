@extends('layouts.extra')

@section('content')
<div class="register-container full-height sm-p-t-30">
    <div class="container-sm-height full-height">
        <div class="row row-sm-height">
            <div class="col-sm-12 col-sm-height col-middle">
                <img src="assets/img/logo.png" alt="logo" data-src="assets/img/logo.png" data-src-retina="assets/img/logo_2x.png" width="78" height="22">
                <h3>{{ __('Register') }}</h3>
                <p>
                    <small>
                    Create a pages account. If you have a facebook account, log into it for this process. Sign in with <a href="#" class="text-info">Facebook</a> or <a href="#" class="text-info">Google</a>
                    </small>
                </p>
                <form method="POST" action="{{ route('register') }}">
                    @csrf
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group form-group-default required">
                                <label>Name</label>
                                <input id="email" type="text" name="name" placeholder="John" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" required>
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group form-group-default required">
                                <label>Email</label>
                                <input id="email" type="email" name="email" placeholder="We will send loging details to you" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}" required>
                                 @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group form-group-default required">
                                <label>Password</label>
                                <input id="password" type="password" name="password" placeholder="Minimum of 4 Charactors" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" required>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group form-group-default required">
                                <label> Confirm Password</label>
                                <input type="password" id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row m-t-10">
                        <div class="col-md-6">
                            <p>I agree to the <a href="#" class="text-info small">Pages Terms</a> and <a href="#" class="text-info small">Privacy</a>.</p>
                        </div>
                        <div class="col-md-6 text-right">
                            <a href="#" class="text-info small">Help? Contact Support</a>
                        </div>
                    </div>
                    <button class="btn btn-primary btn-cons m-t-10" type="submit">Create a new account</button>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

