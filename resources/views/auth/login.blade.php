@extends('layouts.admin.login')

@section('content')
<div class="login-container bg-white">
        <div class="p-l-50 m-l-20 p-r-50 m-r-20 p-t-50 m-t-30 sm-p-l-15 sm-p-r-15 sm-p-t-40">
          <a href="{{ url('/') }}">
            <img src="{{ asset('assets/img/logo.png') }}" alt="logo" data-src="{{ asset('assets/img/logo.png') }}" data-src-retina="{{ asset('assets/img/logo_2x.png') }}" width="78" height="22">
          </a>
          <p class="p-t-35">{{ __('Sign into your pages account') }}</p>
          <!-- START Login Form -->
          <form method="POST" action="{{ route('login') }}">
            @csrf
            <!-- START Form Control-->
            <div class="form-group form-group-default">
              <label>{{ __('E-Mail Address') }}</label>
              <div class="controls">
                <input id="email" type="email" name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                @if ($errors->has('email'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
              </div>
            </div>
            <!-- END Form Control-->
            <!-- START Form Control-->
            <div class="form-group form-group-default">
              <label>{{ __('Password') }}</label>
              <div class="controls">
                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                @if ($errors->has('password'))
                  <span class="invalid-feedback">
                    <strong>{{ $errors->first('password') }}</strong>
                  </span>
                @endif
              </div>
            </div>
            <!-- START Form Control-->
            <div class="row">
              <div class="col-md-6 no-padding">
                <div class="checkbox ">
                  <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                  <label for="checkbox1">{{ __('Keep Me Signed in') }}</label>
                </div>
              </div>
              <div class="col-md-6 text-right">
                <a href="#" class="text-info small">Help? Contact Support</a>
              </div>
            </div>
            <!-- END Form Control-->
            <button class="btn btn-primary btn-cons m-t-10" type="submit">{{ __('Sign in') }}</button>
            <a href="{{ route('password.request') }}" class="text-info small">{{ __('Forgot Your Password?') }}</a>
          </form>
          <!--END Login Form-->
          <div class="pull-bottom sm-pull-bottom">
            <div class="m-b-30 p-r-80 sm-m-t-20 sm-p-r-15 sm-p-b-20 clearfix">
              <div class="col-sm-3 col-md-2 no-padding">
                <img alt="" class="m-t-5" data-src="{{ asset('assets/img/demo/pages_icon.png') }}" data-src-retina="{{ asset('assets/img/demo/pages_icon_2x.png') }}" height="60" src="{{ asset('assets/img/demo/pages_icon.png') }}" width="60">
              </div>
              <div class="col-sm-9 no-padding m-t-10">
                <p>
                  <small>
                  Create a pages account. If you have a facebook account, log into it for this
                  process. Sign in with <a href="#" class="text-info">Facebook</a> or <a href="#"
                class="text-info">Google</a>
                </small>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection
