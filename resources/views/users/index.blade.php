@extends('layouts.master')

@section('content')
<div class="container-fluid container-fixed-lg bg-white">
<!-- START PANEL -->
<div class="panel panel-transparent">
  <div class="row">
    <div class="col-md-12">
      <a href="{{'/users/create'}}" class="btn btn-primary btn-cons"><i class="fas fa-plus"></i> Add User</a>
    </div>
  </div>
  <div class="panel-heading">
    <div class="panel-title">{{ __('All users') }}
    </div>
    <div class="pull-right">
      <div class="col-xs-12">
        <input type="text" id="search-table" class="form-control pull-right" placeholder="Search">
      </div>
    </div>
    <div class="clearfix"></div>
  </div>
  <div class="panel-body">
    <table class="table table-hover demo-table-search table-responsive-block" id="tableWithSearch">
      <thead>
        <tr>
            <th style="width:30%">{{ __('Name') }}</th>
            <th style="width:30%">{{ __('Mail') }}</th>
            <th style="width:20%">{{ __('Work number') }}</th>
            <th style="width:10%"></th>
            <th style="width:10%"></th>
        </tr>
      </thead>
      <tbody>
        @foreach($users as $user)
        <tr>
          <td class="v-align-middle semi-bold">
            <p><a href="/users/{{$user->id}}">{{$user->name}}</a></p>
          </td>
          <td class="v-align-middle">
            <p>{{$user->email}}</p>
          </td>
          <td class="v-align-middle">
            <p>{{$user->work_number}}</p>
          </td>
          @if(auth()->user()->hasPermission('edit.users'))
            <td class="v-align-middle">
                <a href=" {{route('users.edit', $user->id)}} " class="btn btn-success">Edit</a>
            </td>
            @endif
            @if(auth()->user()->hasPermission('delete.users'))
            <td class="v-align-middle">
                <button type="button" class="btn btn-danger delete_client" data-client_id=" {{$user->id}} " onClick="openModal( {{$user->id}} )" id="myBtn">Delete</button>
            </td>
            @endif          
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
<!-- END PANEL -->
</div>


<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="padding:35px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4><span class="glyphicon glyphicon-lock"></span> Handle deletion of user</h4>
        </div>
        <div class="modal-body" style="padding:40px 50px;">
          <form role="form">
           <!--HANDLE TASKS-->
            <div class="form-group">
                <label for="tasks"><span class=""></span> {{ __('How to handle the user tasks?') }}</label>
                <select name="handle_tasks" id="handle_tasks" class="form-control">
                    <option value="delete_all_tasks">{{ __('Delete all tasks') }}</option>
                    <option value="move_all_tasks"> {{ __('Move all tasks') }}</option>
                </select>   
            </div>
            <div class="form-group" id="assign_tasks" style="display:none">
                <label for="user_tasks"><span class="glyphicon glyphicon-user"></span> {{ __('Choose a new user to assign the tasks') }}</label>
                <select name="user_tasks" id="user_tasks" class="form-control">
                    <option value="null" disabled selected> {{ __('Select a user') }} </option>
                    @foreach ($users as $user)
                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                    @endforeach
                </select>   
            </div>

            <!--HANDLE LEADS-->
            <div class="form-group">
                <label for="handle_leads"><span class=""></span> {{ __('How to handle the user leads?') }}</label>
                <select name="leads" id="handle_leads" class="form-control">
                    <option value="delete_all_leads">{{ __('Delete all leads') }}</option>
                    <option value="move_all_leads"> {{ __('Move all leads') }}</option>
                </select>   
            </div>
            <div class="form-group" id="assign_leads" style="display:none">
                <label for="user_leads"><span class="glyphicon glyphicon-user"></span> {{ __('Choose a new user to assign the leads') }}</label>
                <select name="user_leads" id="user_leads" class="form-control">
                    <option value="null" disabled selected> {{ __('Select a user') }} </option>
                    @foreach ($users as $user)
                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                    @endforeach
                </select>   
            </div>

            <!--HANDLE CLIENTS-->
            <div class="form-group">
                <label for="handle_clients"><span class=""></span> {{ __('How to handle the user clients?') }}</label>
                <select name="clients" id="handle_clients" class="form-control">
                    <option value="delete_all_clients">{{ __('Delete all clients') }}</option>
                    <option value="move_all_clients"> {{ __('Move all clients') }}</option>
                </select>   
            </div>
            <div class="form-group" id="assign_clients" style="display:none">
                <label for="user_clients"><span class="glyphicon glyphicon-user"></span> {{ __('Choose a new user to assign the clients') }}</label>
                <select name="user_clients" id="user_clients" class="form-control">
                    <option value="null" disabled selected> {{ __('Select a user') }} </option>
                    @foreach ($users as $user)
                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                    @endforeach
                </select>   
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-danger" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
          <button type="submit" id="confirm_delete" class="btn btn-success"><span class="glyphicon glyphicon-trash"></span> Delete</button>
        </div>
      </div>
      
    </div>
  </div> 

@endsection

@push('styles')
    <link href="{{asset('assets/plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/datatables-responsive/css/datatables.responsive.cs')}}s" rel="stylesheet" type="text/css" media="screen" />
@endpush

@push('scripts')
    <script src="{{asset('assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/datatables-responsive/js/datatables.responsive.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/datatables-responsive/js/lodash.min.js')}}"></script>
    <script src="{{asset('assets/js/datatables.js')}}" type="text/javascript"></script>
<script>

     function openModal(client_id) {
        $("#confirm_delete").attr('delete-id', client_id);
        $("#myModal").modal();
    }
    
    $("#handle_tasks").click(function () {
    
    if($("#handle_tasks").val() == "move_all_tasks") {
        $("#assign_tasks").css('display', 'block');
    } else 
    {
        $("#assign_tasks").css('display', 'none');
    }

    });


    $("#handle_clients").click(function () {

   if($("#handle_clients").val() == "move_all_clients") {
            $("#assign_clients").css('display', 'block');
    } else {
        $("#assign_clients").css('display', 'none');
    }
    });
    
    $("#handle_leads").click(function () {

   if($("#handle_leads").val() == "move_all_leads") {
            $("#assign_leads").css('display', 'block');
    } else {
        $("#assign_leads").css('display', 'none');
    }
    });

    $("#confirm_delete").click(function () {
        id = $(this).attr("delete-id"); 
       handle_leads = $("#handle_leads").val();
       handle_tasks =  $("#handle_tasks").val();
       handle_clients =  $("#handle_clients").val()
       leads_user = $("#user_leads").val();
       tasks_user = $("#user_tasks").val();
       clients_user = $("#user_clients").val();
        $.ajax({
            url: "/users/" + id,
            type: 'DELETE',
                headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
        data: {
        tasks: handle_tasks,
        leads: handle_leads,
        clients: handle_clients,
        task_user: tasks_user,
        lead_user: leads_user,
        client_user: clients_user,
       },   
        complete: function (jqXHR, textStatus) {
                // callback
            },
            success: function (data, textStatus, jqXHR) {
                // success callback
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // error callback
            }
        });
    });
</script>
@endpush

@section('jumbotron')
<!-- START JUMBOTRON -->
<div class="jumbotron" data-pages="parallax">
    <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
      <div class="inner">
        <!-- START BREADCRUMB -->
        <ul class="breadcrumb">
          <li>
            <a href="/">Home</a>
          </li>
          <li><a href="/users" class="active">Users</a>
          </li>
        </ul>
        <!-- END BREADCRUMB -->        
      </div>
    </div>
</div>
<!-- END JUMBOTRON -->
@endsection