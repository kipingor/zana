<div class="tab-pane" id="leads">
<div class="row">
    <div class="panel panel-transparent">
        <div class="panel-heading">
            <div class="panel-title">{{ __('Leads assigned') }}</div>
                <div class="pull-right">
                    <div class="col-xs-12">
                        <input type="text" id="search-lead-table" class="form-control pull-right" placeholder="Search">
                    </div>
                </div>
            <div class="clearfix"></div>
        </div>
        <div class="panel-body">
            <table class="table table-hover demo-table-search table-responsive-block" id="tableWithLeads">
                <thead>
                    <tr>
                        <th style="width:20%">{{ __('Title') }}</th>
                        <th style="width:30%">{{ __('Client') }}</th>
                        <th style="width:20%">{{ __('Created at') }}</th>
                        <th style="width:20%">{{ __('Deadline') }}</th>
                        <th style="width:10%">
                            <select name="status" id="status-lead" class="cs-select cs-skin-slide" data-init-plugin="cs-select">
                            <option value="" disabled selected>{{ __('Status') }}</option>
                                <option value="open">Open</option>
                                <option value="closed">Closed</option>
                                <option value=" ">All</option>
                            </select>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($leads as $lead)
                    <tr>
                        <td class="v-align-middle semi-bold"> {{$lead->title}} </td>
                        <td class="v-align-middle"> {{$lead->client->name}} </td>
                        <td class="v-align-middle"> {{$lead->created_at->diffForHumans()}} </td>
                        <td class="v-align-middle">{{$lead->contact_date->toFormattedDateString()}}  </td>
                        <td class="v-align-middle">
                            @if($lead->status == 1)
                            <span class="label label-success">Open</span>
                            @else
                            <span class="label label-danger">Closed</span>
                            @endif 
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>