@extends('layouts.master')
@section('content')
@include('crm::partials.userheader')
<div class="container-fluid container-fixed-lg bg-white">
<div class="col-sm-8">
    <div class="row">
        <div class="panel">
            <ul class="nav nav-tabs nav-tabs-simple" role="tablist" data-init-reponsive-tabs="collapse">
                <li class="active"><a href="#tasks" data-toggle="tab" role="tab">Tasks</a>
                </li>
                <li><a href="#leads" data-toggle="tab" role="tab">Leads</a>
                </li>
                <li><a href="#clients" data-toggle="tab" role="tab">Clients</a>
                </li>
            </ul>
            <div class="tab-content">
                @include('users.tasks')
                @include('users.leads')
                @include('users.clients')
            </div>
        </div>
    </div>
</div>
  <div class="col-sm-4">
  <h4>{{ __('Tasks') }}</h4>
{{-- <doughnut :statistics="{{$task_statistics}}"></doughnut> --}}
<h4>{{ __('Leads') }}</h4>
{{-- <doughnut :statistics="{{$lead_statistics}}"></doughnut> --}}
  </div>
</div>



@endsection

@push('styles')
    <link href="{{asset('assets/plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/datatables-responsive/css/datatables.responsive.cs')}}s" rel="stylesheet" type="text/css" media="screen" />
@endpush

@push('scripts')
    <script src="{{asset('assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/datatables-responsive/js/datatables.responsive.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/datatables-responsive/js/lodash.min.js')}}"></script>
    <script src="{{asset('assets/js/datatables.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/bootstrap-collapse/bootstrap-tabcollapse.js')}}" type="text/javascript"></script>
@endpush


