@extends('layouts.master')

@section('content')
<div class="register-container full-height sm-p-t-30">
<div class="container-sm-height full-height">
<div class="row row-sm-height">
  <div class="col-sm-12 col-sm-height col-middle">
    {!! Form::model($user, [
            'method' => 'PATCH',
            'route' => ['users.update', $user->id],
            'class' => 'p-t-15',
            'files'=>true,
            'enctype' => 'multipart/form-data'
            ]) !!}

    @include('users.form', ['submitButtonText' =>  __('Update user')])

    {!! Form::close() !!}
  </div>
</div>
</div>
</div>
@endsection