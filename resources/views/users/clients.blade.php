<div class="tab-pane" id="clients">
<div class="row">
    <div class="panel panel-transparent">
        <div class="panel-heading">
            <div class="panel-title">{{ __('Clients assigned') }}</div>
                <div class="pull-right">
                    <div class="col-xs-12">
                        <input type="text" id="search-table" class="form-control pull-right" placeholder="Search">
                    </div>
                </div>
            <div class="clearfix"></div>
        </div>
        <div class="panel-body">
            <table class="table table-hover demo-table-search table-responsive-block" id="tableWithClients">
                
                <thead>
                    <tr>
                        <th style="width:35%">{{ __('Name') }}</th>
                        <th style="width:50%">{{ __('Company') }}</th>
                        <th style="width:15%">{{ __('Primary number') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($clients as $client)
                    <tr>
                        <td class="v-align-middle semi-bold"> {{$client->name}} </td>
                        <td class="v-align-middle"> {{$client->company_name}} </td>
                        <td class="v-align-middle"> {{$client->primary_number}} </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>
