<div class="tab-pane active" id="tasks">
    <div class="row">
        <div class="panel panel-transparent">
            <div class="panel-heading">
                <div class="panel-title">{{ __('Tasks assigned') }}</div>
                    <div class="pull-right">
                        <div class="col-xs-12">
                            <input type="text" id="search-task-table" class="form-control pull-right" placeholder="Search">
                        </div>
                    </div>
                <div class="clearfix"></div>
            </div>
            <div class="panel-body">
                <table class="table table-hover demo-table-search table-responsive-block" id="tableWithTasks">
                    <thead>
                        <tr>
                            <th style="width:20%">{{ __('Title') }}</th>
                            <th style="width:30%">{{ __('Client') }}</th>
                            <th style="width:20%">{{ __('Created at') }}</th>
                            <th style="width:20%">{{ __('Deadline') }}</th>
                            <th style="width:10%">
                                <select name="status" id="status-task" class="cs-select cs-skin-slide" data-init-plugin="cs-select">
                                <option value="" disabled selected>{{ __('Status') }}</option>
                                    <option value="open">Open</option>
                                    <option value="closed">Closed</option>
                                    <option value=" ">All</option>
                                </select>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($tasks as $task)
                        <tr>
                            <td class="v-align-middle semi-bold"> {{$task->title}} </td>
                            <td class="v-align-middle"> {{$task->client->name}} </td>
                            <td class="v-align-middle"> {{$task->created_at->toFormattedDateString()}} </td>
                            <td class="v-align-middle"> {{$task->deadline->toFormattedDateString()}} </td>
                            <td class="v-align-middle"> 
                                @if($task->status == 1)
                                <span class="label label-success">Open</span>
                                @else
                                <span class="label label-danger">Closed</span>
                                @endif 
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>    
            </div>
        </div>
    </div>
</div>