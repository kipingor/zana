<div class="row">
<div class="col-sm-12">
<div class="form-group form-group-default">
    {{ Form::label('image_path', __('Image'), ['class' => 'control-label']) }}
    {!! Form::file('image_path',  null, ['class' => 'form-control']) !!}
</div>
</div>
</div>

<div class="row">
<div class="col-sm-12">
<div class="form-group form-group-default">
    {!! Form::label('name', __('Name'), ['class' => 'control-label']) !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>
</div>
</div>

<div class="row">
<div class="col-sm-12">
<div class="form-group form-group-default">
    {!! Form::label('email', __('Mail'), ['class' => 'control-label']) !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>
</div>
</div>

<div class="row">
<div class="col-sm-12">
<div class="form-group form-group-default">
    {!! Form::label('address', __('Address'), ['class' => 'control-label']) !!}
    {!! Form::text('address', null, ['class' => 'form-control']) !!}
</div>
</div>
</div>

<div class="row">
<div class="col-sm-6">
<div class="form-group form-group-default">
    {!! Form::label('work_number', __('Work number'), ['class' => 'control-label']) !!}
    {!! Form::text('work_number',  null, ['class' => 'form-control']) !!}
</div>
</div>

<div class="col-sm-6">
<div class="form-group form-group-default">
    {!! Form::label('personal_number', __('Personal number'), ['class' => 'control-label']) !!}
    {!! Form::text('personal_number',  null, ['class' => 'form-control']) !!}
</div>
</div>
</div>

<div class="row">
<div class="col-sm-12">
<div class="form-group form-group-default">
    {!! Form::label('password', __('Password'), ['class' => 'control-label']) !!}
    {!! Form::password('password', ['class' => 'form-control']) !!}
</div>
</div>
</div>

<div class="row">
<div class="col-sm-12">
<div class="form-group form-group-default">
    {!! Form::label('password_confirmation', __('Confirm password'), ['class' => 'control-label']) !!}
    {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
</div>
</div>
</div>

<div class="row">
<div class="col-sm-6">
<div class="form-group form-group-default form-group-default-select2">
    {!! Form::label('roles', __('Assign role'), ['class' => 'control-label']) !!}
    {!!
        Form::select('roles',
        $roles,
        isset($user->role->role_id) ? $user->role->role_id : null,
        ['class' => 'full-width', 'data-init-plugin' => 'select2']) !!}
</div>
</div>

<div class="col-sm-6">
<div class="form-group form-group-default form-group-default-select2">
    {!! Form::label('departments', __('Assign department'), ['class' => 'control-label']) !!}

    {!!
        Form::select('departments',
        $departments,
        isset($user)
        ? $user->department->first()->id : null,
        ['class' => 'full-width', 'data-init-plugin' => 'select2']) !!}
</div>
</div>
</div>

{!! Form::submit($submitButtonText, ['class' => 'btn btn-primary']) !!}
