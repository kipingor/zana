
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
// import './lib/theme-default/index.css';
// import Vue from 'vue'
import vSelect from 'vue-select'

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('utility-bill', require('./components/UtilityBill.vue'));
Vue.component('v-select', vSelect);

const app = new Vue({
    el: '#app'    
});
