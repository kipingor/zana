/* ============================================================
 * Demo
 * Try various layout options available in Pages
 * For DEMO purposes only.
 * ============================================================ */
(function($) {

    'use strict';
// Initialize Modal for adding Payment
    var initAddPayment = function() {
    	$('#button-payment').click(function() {
    		$('#payment-modal').modal('show');
    	});
    	$('#add-app').click(function() {            
        $('#addNewAppModal').modal('hide');
      });
      const payment = {
			  heading: '{{ trans("invoices.add_payment") }}',
			  guardian: 'Mr. Kalehoff'
			};

      //Create Modal
      let html = '';
      html = `<div class="modal fade slide-up disable-scroll" id="payment-modal" tabindex="-1" role="dialog" aria-hidden="false" aria-labelledby="paymentModalLabel">
      <div class="modal-dialog ">
              <div class="modal-content-wrapper">
                <div class="modal-content">
                  <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>`;
      html += '<h5>{{ trans('invoices.add_payment') }}</h5>';
      html = `
                    
                    <p class="p-b-10">We need payment information inorder to process your order</p>
                  </div>
                  <div class="modal-body">
                    <form role="form">
                      <div class="form-group-attached">
                        <div class="row">
                          <div class="col-sm-12">
                            <div class="form-group form-group-default">
                              <label>Company Name</label>
                              <input type="email" class="form-control">
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-sm-8">
                            <div class="form-group form-group-default">
                              <label>Card Number</label>
                              <input type="text" class="form-control">
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <div class="form-group form-group-default">
                              <label>Card Holder</label>
                              <input type="text" class="form-control">
                            </div>
                          </div>
                        </div>
                      </div>
                    </form>
                    <div class="row">
                      <div class="col-sm-8">
                        <div class="p-t-20 clearfix p-l-10 p-r-10">
                          <div class="pull-left">
                            <p class="bold font-montserrat text-uppercase">TOTAL</p>
                          </div>
                          <div class="pull-right">
                            <p class="bold font-montserrat text-uppercase">$20.00</p>
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-4 m-t-10 sm-m-t-10">
                        <button type="button" class="btn btn-primary btn-block m-t-5">Pay Now</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
          </div>`;
          $('body').append(html);
    }
    // END BUILDER
initAddPayment();
})(window.jQuery);