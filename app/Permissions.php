<?php

namespace Zana;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Permissions extends Model
{
    public function roles()
    {
        return $this->belongsToMany(Role::class, 'permission_role', 'permission_id', 'role_id');
    }
}
