<?php

namespace Zana;

use Illuminate\Database\Eloquent\Model;

use Zana\Permissions;

class Role extends Model
{
    protected $fillable = [
        'name',
        'slug',
        'description',
        'level'
    ];

    public function userRole()
    {
        return $this->hasMany(Role::class, 'user_id', 'id');
    }

    public function permissions()
    {
        return $this->belongsToMany(permissions::class, 'permission_role', 'role_id', 'permission_id');
    }
}
