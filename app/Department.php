<?php

namespace Zana;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $fillable =
        [
            'name',
            'description'
        ];
}
