<?php

namespace Zana;

use Modules\CRM\Entities\Task;
use Illuminate\Database\Eloquent\Model;
use Modules\Accounting\Traits\Media;
use Facades\Modules\CRM\Repositories\Setting\SettingRepository;


class Setting extends Model
{
    use Media;
    protected $fillable = [
        'task_complete_allowed',
        'task_assign_allowed',
        'lead_complete_allowed',
        'lead_assign_allowed'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function tasks()
    {
        return $this->belongsTo(Task::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * @return mixed
     */
    public static function getCompanyName()
    {
        return Setting::findOrFail(1)->company;
    }

    /**
     * @param $requestData
     */
    public function updateOverall($requestData)
    {
        $setting = Setting::findOrFail(1);

        $setting->fill($requestData->all())->save();
    }

    /**
     * @return mixed
     */
    public function getSetting()
    {
        return Setting::findOrFail(1);
    }
}
