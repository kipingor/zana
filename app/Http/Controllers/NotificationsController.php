<?php

namespace Zana\Http\Controllers;

use Notifynder;
use Zana\User;
use Modules\CRM\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Log;
use Carbon\Carbon;
use Auth;

class NotificationsController extends Controller
{
     /**
     * Mark a notification read
     * @param Request $request
     * @return mixed
     */
    public function markRead(Request $request)
    {
        $user = auth()->user();
    $user->unreadNotifications()->where('id', $request->id)->first()->markAsRead();

        return redirect($user->notifications->where('id', $request->id)->first()->data['url']);
    }

    /**
     * Mark all notifications as read
     * @return mixed
     */
    public function markAll()
    {
        $user = User::find(Auth::id());
    
        foreach ($user->unreadNotifications as $notification) {
            $notification->markAsRead();
        }
        return redirect()->back();
    }
}
