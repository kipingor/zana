<?php
namespace Zana\Http\Controllers;

// use Auth;
use Session;
use Illuminate\Http\Request;
use Modules\CRM\Repositories\Setting\SettingRepositoryContract;
use Modules\CRM\Repositories\Role\RoleRepositoryContract;
use Modules\CRM\Http\Requests;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use jeremykenedy\LaravelRoles\Traits\HasRoleAndPermission;
use jeremykenedy\LaravelRoles\Models\Role;
use jeremykenedy\LaravelRoles\Models\Permission;
use Zana\Http\Requests\Setting\UpdateSettingOverallRequest;
use Zana\Http\Requests\Setting\UpdateSettingGeneralRequest;
use Modules\Accounting\Entities\Account;
use Zana\Company;
use Modules\Accounting\Entities\Currency;
use Modules\Accounting\Entities\Media;
use Modules\Accounting\Entities\Tax;
use Modules\Accounting\Traits\DateTime;
use Modules\Accounting\Traits\Uploads;
use Modules\Accounting\Entities\Setting as AccountSetting;
use MediaUploader;

class SettingsController extends Controller
{
    use DateTime, Uploads;

    protected $settings;
    protected $roles;
    protected $permissions;
    /**
    * SettingsController constructor.
    * @param SettingRepositoryContract $settings
    * @param RoleRepositoryContract $roles
    */
    public function __construct(
        SettingRepositoryContract $settings,
        RoleRepositoryContract $roles
    )
    {
        $this->middleware('auth');
        $this->settings = $settings;
        $this->roles = $roles;
        $this->middleware('user.is.admin', ['only' => ['index'.'general']]);
    }
    /**
    * @return mixed
    */
    public function index()
    {
      $permissions = Permission::all();
      $unique_perm = $permissions->unique('name');
      $perm_value = $unique_perm->values()->all();
      $unique_mod = $permissions->unique('model');
      $model_value = $unique_mod->values()->all();
        return view('settings.index', compact('perm_value', 'model_value'))
        ->withSettings($this->settings->getSetting())
        ->withPermission($this->roles->allPermissions())
        ->withRoles($this->roles->allRoles());
    }

    /**
     * [general description]
     * @return [type] [description]
     */
    public function general()
    {
        /*$setting = Setting::all()->pluck('value', 'key');*/
        $setting = AccountSetting::all()->map(function ($s) {
            $s->key = str_replace('general.', '', $s->key);
            return $s;
        })->pluck('value', 'key');
        $company_logo = $setting->pull('company_logo');        
        $setting['company_logo'] = Media::find($company_logo);
        $logo_icon = $setting->pull('logo_icon');
        $setting['logo_icon'] = Media::find($logo_icon);
        $invoice_logo = $setting->pull('invoice_logo');
        $setting['invoice_logo'] = Media::find($invoice_logo);
        $timezones = $this->getTimezones();
        $accounts = Account::all()->pluck('name', 'id');
        $currencies = Currency::all()->pluck('name', 'code');
        $taxes = Tax::all()->pluck('title', 'id');
        $payment_methods = ['1'=>'Mpesa','2'=>'Cash', '3'=>'Other'];
        $date_formats = [
            'd M Y' => '31 Dec 2017',
            'd F Y' => '31 December 2017',
            'd m Y' => '31 12 2017',
            'm d Y' => '12 31 2017',
            'Y m d' => '2017 12 31',
        ];
        $date_separators = [
            'dash' => trans('settings.localisation.date.dash'),
            'slash' => trans('settings.localisation.date.slash'),
            'dot' => trans('settings.localisation.date.dot'),
            'comma' => trans('settings.localisation.date.comma'),
            'space' => trans('settings.localisation.date.space'),
        ];
        $email_protocols = [
            'mail' => trans('settings.email.php'),
            'smtp' => trans('settings.email.smtp.name'),
            'sendmail' => trans('settings.email.sendmail'),
            'log' => trans('settings.email.log'),
        ];
        $percent_positions = [
            'before' => trans('settings.localisation.percent.before'),
            'after' => trans('settings.localisation.percent.after'),
        ];
        return view('settings.general', compact(
        'setting',
        'timezones',
        'accounts',
        'currencies',
        'taxes',
        'payment_methods',
        'date_formats',
        'date_separators',
        'email_protocols',
        'percent_positions'
        ));
    }

    /**
    * @param UpdateSettingOverallRequest $request
    * @return mixed
    */
    public function updateOverall(UpdateSettingOverallRequest $request)
    {
        $this->settings->updateOverall($request);
        Session::flash('flash_message', 'Overall settings successfully updated');
        return redirect()->back();
    }

    public function updateGeneral(UpdateSettingGeneralRequest $request)
    {
        $fields = $request->all();
        $user = auth()->user();
        

        $file_keys = ['company_logo', 'invoice_logo', 'logo_icon'];
        
        foreach ($fields as $key => $value) {

            // // Process file uploads
            if (in_array($key, $file_keys)) {
                $invoice_logo = $request->file('invoice_logo');
                // Upload attachment
                if ($request->file($key)) {
                    Session::flash('flash_message_warning', 'this file will be added '.$key);
                    $media = $this->getMedia($request->file($key), 'settings');

                    $user->attachMedia($media, $key);

                    $value = $media->id;
                }

                // Prevent reset
                if (empty($value)) {
                    continue;
                }
            }

            setting()->set('general.' . $key, $value);
        }

        // Save all settings
        setting()->save();

        $message = trans('messages.success.updated', ['type' => trans_choice('general.settings', 2)]);

        Session::flash('flash_message_success', $message);

        return redirect('settings/general');
    }
    /**
    * @param Request $request
    * @return mixed
    */
    public function permissionsUpdate(Request $request)
    {
        $this->roles->permissionsUpdate($request);
        Session::flash('flash_message_success', 'Role is updated');
        return redirect()->back();
    }
    
}