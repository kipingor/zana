<?php

namespace Zana\Http\Controllers;

use Gate;
use Carbon;
use DataTables;
use Zana\User;
use Modules\CRM\Entities\Task;
use Modules\CRM\Entities\Requests;
use Modules\CRM\Entities\Client;
use Modules\CRM\Entities\Lead;
use Illuminate\Http\Request;
use Zana\Http\Requests\User\UpdateUserRequest;
use Zana\Http\Requests\User\StoreUserRequest;
use Modules\CRM\Repositories\User\UserRepositoryContract;
use Modules\CRM\Repositories\Role\RoleRepositoryContract;
use Modules\CRM\Repositories\Department\DepartmentRepositoryContract;
use Modules\CRM\Repositories\Setting\SettingRepositoryContract;
use Modules\CRM\Repositories\Task\TaskRepositoryContract;
use Modules\CRM\Repositories\Lead\LeadRepositoryContract;
use Illuminate\Http\Response;
// use Illuminate\Routing\Controller;

class UsersController extends Controller
{
    protected $users;
    protected $roles;
    protected $departments;
    protected $settings;

    public function __construct(
        UserRepositoryContract $users,
        RoleRepositoryContract $roles,
        DepartmentRepositoryContract $departments,
        SettingRepositoryContract $settings,
        TaskRepositoryContract $tasks,
        LeadRepositoryContract $leads
    )
    {

        $this->users = $users;
        $this->roles = $roles;
        $this->departments = $departments;
        $this->settings = $settings;
        $this->tasks = $tasks;
        $this->leads = $leads;
        $this->middleware('user.create', ['only' => ['create']]);
        $this->middleware('user.update');
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $users = User::all();
        return view('users.index', compact('users'));
    }

    public function users()
    {
        return User::all();
    }

    public function anyData()
    {
        $canUpdateUser = auth()->user()->hasPermission('edit.user');
        $users = User::select(['id', 'name', 'email', 'work_number']);
        return datatables()->of($users)
            // ->addColumn('namelink', function ($users) {
            //     return '<a href="users/' . $users->id . '">'. $users->name .'</a>';
            // })
            ->addColumn('edit', function ($user) {
                return '<a href="' . route("users.edit", $user->id) . '" class="btn btn-success"> Edit</a>';
            })
            ->addcolumn('delete', function ($user) { 
                return '<button type="button" class="btn btn-danger delete_client" data-client_id="' . $user->id . '" onClick="openModal(' . $user->id. ')" id="myBtn">Delete</button>';
            })->make(true);
    }


    /**
     * @return mixed
     */
    public function create()
    {
        return view('users.create')
            ->withRoles($this->roles->listAllRoles())
            ->withDepartments($this->departments->listAllDepartments());
    }

    /**
     * @param StoreUserRequest $userRequest
     * @return mixed
     */
    public function store(StoreUserRequest $userRequest)
    {
        $getInsertedId = $this->users->create($userRequest);
        return redirect()->route('users.index');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        $tasks = Task::all()->where('user_assigned_id', $id);
        $leads = Lead::all()->where('user_assigned_id', $id);
        $clients = Client::all()->where('user_id', $id);
        $contact = $this->users->find($id);
        return view('users.show', compact('contact','tasks','leads','clients'))
            ->withUser($this->users->find($id))
            ->withCompanyname($this->settings->getCompanyName())
            ->withTaskStatistics($this->tasks->totalOpenAndClosedTasks($id))
            ->withLeadStatistics($this->leads->totalOpenAndClosedLeads($id));
    }

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        return view('users.edit')
            ->withUser($this->users->find($id))
            ->withRoles($this->roles->listAllRoles())
            ->withDepartments($this->departments->listAllDepartments());
    }

    /**
     * @param $id
     * @param UpdateUserRequest $request
     * @return mixed
     */
    public function update($id, UpdateUserRequest $request)
    {
        $this->users->update($id, $request);
        Session()->flash('flash_message', 'User successfully updated');
        return redirect()->back();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy(Request $request, $id)
    {
        $this->users->destroy($request, $id);

        return redirect()->route('users.index');
    }
}
