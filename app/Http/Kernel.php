<?php

namespace Zana\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \Zana\Http\Middleware\TrimStrings::class,
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,
        \Zana\Http\Middleware\TrustProxies::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \Zana\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            // \Illuminate\Session\Middleware\AuthenticateSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \Zana\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
            \Zana\Http\Middleware\LogLastUserActivity::class,
        ],
        'client.create' => [ \Modules\CRM\Http\Middleware\CanClientCreate::class ],
        'client.update' => [ \Modules\CRM\Http\Middleware\CanClientUpdate::class ],
        'user.create' => [ \Zana\Http\Middleware\CanUserCreate::class ],
        'user.update' => [ \Zana\Http\Middleware\CanUserUpdate::class ],
        'task.create' => [ \Modules\CRM\Http\Middleware\CanTaskCreate::class ],
        'task.update.status' => [ \Modules\CRM\Http\Middleware\CanTaskUpdateStatus::class ],
        'task.assigned' => [ \Modules\CRM\Http\Middleware\IsTaskAssigned::class ],
        'lead.create' => [ \Modules\CRM\Http\Middleware\CanLeadCreate::class ],
        'lead.assigned' => [ \Modules\CRM\Http\Middleware\IsLeadAssigned::class ],
        'lead.update.status' => [ \Modules\CRM\Http\Middleware\CanLeadUpdateStatus::class ],
        'user.is.admin' => [ \Zana\Http\Middleware\RedirectIfNotAdmin::class ],
        'create.item' => [ \Modules\Accounting\Http\Middleware\CanCreateItem::class ],
        'delete.item' => [ \Modules\Accounting\Http\Middleware\CanDeleteItem::class ],

        'api' => [
            'throttle:60,1',
            'bindings',
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \Illuminate\Auth\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'cache.headers' => \Illuminate\Http\Middleware\SetCacheHeaders::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \Zana\Http\Middleware\RedirectIfAuthenticated::class,
        'signed' => \Illuminate\Routing\Middleware\ValidateSignature::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'role' => \Zizaco\Entrust\Middleware\EntrustRole::class,
        'permission' => \Zizaco\Entrust\Middleware\EntrustPermission::class,
        'ability' => \Zizaco\Entrust\Middleware\EntrustAbility::class,
        'role' => \jeremykenedy\LaravelRoles\Middleware\VerifyRole::class,
        // 'permission' => \jeremykenedy\LaravelRoles\Middleware\VerifyPermission::class,
        'level' => \jeremykenedy\LaravelRoles\Middleware\VerifyLevel::class,
    ];
}
