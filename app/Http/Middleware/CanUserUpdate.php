<?php

namespace Zana\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CanUserUpdate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!auth()->user()->hasPermission('edit.users')) {
            Session()->flash('flash_message_warning', 'Not allowed to update user');
            return redirect()->route('users.index');
        }
        return $next($request);
    }
}
