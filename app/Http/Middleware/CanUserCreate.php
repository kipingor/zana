<?php

namespace Zana\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CanUserCreate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (!auth()->user()->hasPermission('create.users')) {
            Session()->flash('flash_message_warning', 'Not allowed to create user');
            return redirect()->route('users.index');
        }
        return $next($request);
    }
}
