<?php

namespace Zana\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'Modules\CRM\Events\ClientAction' => [
            'Modules\CRM\Listeners\ClientActionNotify',
            'Modules\CRM\Listeners\ClientActionLog',
        ],
         'Modules\CRM\Events\TaskAction' => [
            'Modules\CRM\Listeners\TaskActionNotify',
            'Modules\CRM\Listeners\TaskActionLog',
         ],
        'Modules\CRM\Events\LeadAction' => [
            'Modules\CRM\Listeners\LeadActionNotify',
            'Modules\CRM\Listeners\LeadActionLog',
        ],  
        'Modules\CRM\Events\NewComment' => [
            'Modules\CRM\Listeners\NotiftyMentionedUsers'
        ],      
        'Zana\Events\Event' => [
            'Zana\Listeners\EventListener',
        ],
         'Modules\Accounting\Events\InvoiceAction' => [
            'Modules\Accounting\Listeners\InvoiceActionNotify',
            'Modules\Accounting\Listeners\InvoiceActionLog',
         ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
