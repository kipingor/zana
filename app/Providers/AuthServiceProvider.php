<?php

namespace Zana\Providers;

use Zana\User;
use Modules\CRM\Entities\Task;
use Modules\CRM\Policies\allowTaskComplete;
use Illuminate\Support\Facades\Gate;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'Zana\Model' => 'Zana\Policies\ModelPolicy',
        Task::class => allowTaskComplete::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);

        //
    }
}
