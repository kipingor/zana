<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// use Modules\CRM\Http\Controllers;
// use Modules\Accounting\Http\Controllers;

Route::get('/', function () {
    return view('public.otadev');
});
Route::get('/portfolio', function () {
    return view('public.portfolio.index');
});
Route::get('/contact', function () {
    return view('public.contact.index');
});
Auth::routes();

// Route::get('/logout', 'Auth\LoginController@logout');
Route::group(['middleware' => ['auth']], function () {
    
    Route::get('/home', 'HomeController@index')->name('home');
    
    /**
     * Users
     */
    Route::group(['prefix' => 'users'], function () {
        Route::get('/data', 'UsersController@anyData')->name('users.data');
        Route::get('/taskdata/{id}', 'UsersController@taskData')->name('users.taskdata');
        Route::get('/leaddata/{id}', 'UsersController@leadData')->name('users.leaddata');
        Route::get('/clientdata/{id}', 'UsersController@clientData')->name('users.clientdata');
        Route::get('/users', 'UsersController@users')->name('users.users');
    });
        Route::resource('users', 'UsersController');

/**
     * Settings
     */
    Route::group(['prefix' => 'settings'], function () {
        Route::get('/', 'SettingsController@index')->name('settings.index');
        Route::patch('/permissionsUpdate', 'SettingsController@permissionsUpdate');
        Route::patch('/overall', 'SettingsController@updateOverall');
        Route::patch('/updateGeneral', 'SettingsController@updateGeneral');

        Route::get('/company', function () {
            return view('/settings/company');
        });
        Route::get('/localization', function () {
            return view('/settings/localization');
        });
        Route::get('/general', 'SettingsController@general');
    });

    /**
     * Departments
     */
        Route::resource('departments', 'DepartmentsController');
     /**
     * Roles
     */
        Route::resource('roles', 'RolesController');

    
        
    /**
     * Notifications
     */
    Route::group(['prefix' => 'notifications'], function () {
        Route::post('/markread', 'NotificationsController@markRead')->name('notification.read');
        Route::get('/markall', 'NotificationsController@markAll');
        Route::get('/{id}', 'NotificationsController@markRead');

    });
    Route::resource('notifications', 'NotificationsController');
    Route::get('/coming', function () {
        return view('coming');
    });
});